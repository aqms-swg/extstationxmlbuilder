from functions import openpickle, picklewriter, check_none
from processing.processor import input_processor
import datetime
import copy

dataloggerPrimaryMapping = {'CUSP-Tustin-2': 'JMPRY',
                            'National-Black-Mountain': 'PBMRY',
                            'National-Bloomer': 'OBHRY',
                            'National-Bullion': 'MBURY',
                            'National-Cahto': 'KCPRY',
                            'National-Carr-Hill': 'PCHRY',
                            'National-Fremont-Pk': 'HSPRY',
                            'National-Geyser-Peak': 'GGPRY',
                            'National-Hollister': 'HBLRY',
                            'National-LLNL': 'CLLRY',
                            'National-Mammoth': 'MWDRY',
                            'National-MP': 'JMPRY',
                            'National-Mt-Tamalpais': 'NTARY',
                            'National-Pierce': 'KMPRY',
                            'National-Sonoma-Mountain': 'NSMRY',
                            'National-South-Fork': 'LSFRY',
                            'National-Vollmer-2': 'CVPRY',
                            'National-Vollmer-Peak': 'CVPRY',
                            'National-Williams-Hill': 'PWHRY'
                            }


multiDatalogSerialMapping = {'402': 'KMPRY',
                             '403': 'KMPRY',
                             '414': 'KMPRY',
                             '417': 'LSFRY',
                             '418': 'LSFRY',
                             '419': 'LSFRY',
                             '420': 'LSFRY',
                             '109': 'LACRY',
                             '111': 'KHMRY',
                             '112': ['LACRY', 'KMPRY'],  # LBC appears to be incorrect
                             '113': 'KHMRY',
                             '115': ['KMPRY', 'LACRY'],
                             '131': 'KMPRY',
                             '129': 'PBMRY',
                             '157': 'PWHRY',
                             '165': 'PWHRY',  # MRD appears to be incorrect
                             '177': 'PWHRY',
                             '180': 'PWHRY',
                             '185': 'PBMRY',
                             '186': 'PBMRY',
                             '193': 'PBMRY',
                             '195': 'PWHRY',
                             '197': 'PWHRY',
                             '198': 'PBMRY',
                             '200': 'PBMRY',
                             '201': 'PBMRY',
                             '202': 'PBMRY',
                             '204': 'PWHRY',
                             '205': 'KCPRY',
                             '206': 'KCPRY',
                             '207': 'PWHRY',
                             '347': 'PBMRY',
                             '168m': 'MDP1',  # Not a relay site
                             }

multiSNs = [
    "T2",
    "0X160C8EF",
    "0X0167BF64",
    "0X160C8F1",
    "0X0167BF6E",
    "0X160C8F4",
    "0X019E7ECF",
    "0X160C8F5",
    "0X019E7F03",
    "0X160C8F6",
    "0X01A1EF05",
    "0X16282E8",
    "0X01A1EF09",
    "0X19BCEC7",
    "0X1001934",
    "0X19E7ED5",
    "0X1011D54",
    "0X19E7EDC",
    "0X1011D62",
    "0X19E7EF3",
    "0X1011D86",
    "0X1A1EEEC",
    "0X1011DD0",
    "0X1A1EF16",
    "0X1011DEF",
    "0XA5DF8C",
    "0X1210BD0",
    "0XA6D3BA",
    "0X13F76CC",
    "0XA80BB4",
    "0X1437215",
    "0XA84F4F",
    "0X14B9EFE",
    "GEYSERPK-UNKN",
    "0X1583E50",
    "N13",
    "0X1583E6E",
    "0X160C8ED"
]

def multi_loggers():
    keys = ["CUSP", "NATIONAL"]
    nets = ["NC", "NP"]
    fs = openpickle("objs/FS_dic.obj")
    multis = {}
    for dl in fs['recorders']:
        ns = dl['netsta'].split("-")
        for k in keys:
            if k in dl['rectype'].upper() and ns[0] in nets:
                # print(dl['rectype'])
                if dl['rectype'] in dataloggerPrimaryMapping:
                    # ms = f"{dataloggerPrimaryMapping[dl['rectype']]}_{dl['rectype']}"
                    ms = dl['recsn'].upper()
                    if ms not in multis:
                        multis[ms] = {"dls": [dl], "upchans": [], "upsens": []}
                    else:
                        multis[ms]['dls'].append(dl)

    for mdl in multis:
        added = {}
        for dl in multis[mdl]['dls']:
            # print(dl['ln'])
            for chan in fs['channels']:
                if chan['ln'] == dl['ln']:
                    if chan['chadate_inst'] != chan['chadate_rem']:

                        if chan['pinnumber'] not in added:
                            added[chan['pinnumber']] = [chan['chadate_inst']]
                            multis[mdl]['upchans'].append(chan)
                        elif chan['chadate_inst'] not in added[chan['pinnumber']]:
                            added[chan['pinnumber']].append(chan['chadate_inst'])
                            multis[mdl]['upchans'].append(chan)
                            # TODO this should go with parse_FredSheet.py
                            if len(multis[mdl]['upchans']) % 100 == 0:
                                print(f"I've got {len(multis[mdl]['upchans'])} "
                                      f"channels for {mdl}!")
            for sen in fs['sensors']:
                if sen['ln'] == dl['ln']:
                    multis[mdl]['upsens'].append(sen)

        pins = []
        for chan in multis[mdl]['upchans']:
            if check_none(chan['pinnumber']):
                ip = int(chan['pinnumber'])
                pins.append(ip)
            # if check_none(chan['dlchan']):
            #     ip2 = int(chan['dlchan'])
            #     pins.append(ip2)
        # if 0 in pins:
        #     for chan in multis[mdl]['upchans']:
        #         idx = multis[mdl]['upchans'].index(chan)
        #         if idx % 100 == 0:
        #             print("increasing pinnumber/dlchan for index", idx)
        #         if check_none(chan['pinnumber']):
        #             multis[mdl]['upchans'][idx]['pinnumber'] = str(int(chan['pinnumber']))
        #         if check_none(chan['dlchan']):
        #             multis[mdl]['upchans'][idx]['dlchan'] = str(int(chan['dlchan']))

        pchans, psens = input_processor(
            multis[mdl]['upchans'], multis[mdl]['upsens'], multis[mdl]['dls'])
        multis[mdl]['chans'] = pchans
        multis[mdl]['sens'] = psens
    picklewriter("objs/msdls.obj", multis)


# TODO: only got GGPRY portion of 0x1011D86 - why not NSMRY (sonoma)?



"""
1. arrange sens/chans/amps into dict by line, 
so line 1423 just gives me what I need without looking through everything?
2. Add amps
"""
