from processing.nrl_linker import Equipment
from obspy.clients.nrl import NRL
import sisxmlparser3_0 as sisxmlparser
from logger_setup import my_logger as myl
import shelve
from functions import check_none
my_logger = myl()


def gr2(chan, sensor=None, dl=None, eqt=None):
    rate = chan['samprate']
    if eqt == "SENSOR":
        etype = sensor['sentype']
        rangeg = sensor['rangeg']
        sens = sensor['sens']
        gcorrect = 1
        if check_none(sensor):
            if 'sens_units' in sensor and check_none(sensor['sens_units']):
                if 'g' not in sensor['sens_units']:
                    gcorrect = 9.8
        eq = Equipment(etype, rate, sens, eqt="SENSOR", rangeg=rangeg, gcorrect=gcorrect) # rate, sensitivity
        # print(etype, eq.keys)
    elif eqt == "LOGGER":
        if dl['rectype'] in [None, 'None', 'none', '', '-']:
            return None
        elif "NETQUAKE" in dl['rectype'].upper():
            return None
        sens = chan['sensitivity']
        etype = dl['rectype']
        if 'causal' in chan:
            filtertype = chan['causal']
        else:
            filtertype = "ACAUSAL"

        eq = Equipment(etype, rate, sens, eqt="LOGGER", filtertype=filtertype,
                       fir=chan['firfilter'])
        # print(eq.keys) # LML, LMM, LOM, LSS
        # GOOD: MCUB, MDH, NIMB
    link = nrlink(eq.keys, eqt=eqt)
    if type(link) == str:
        return link
    else:
        if "L4C" not in etype:
            my_logger.info(f"gr2 couldn't find a response for {etype}")
        return None


def get_netquakes_return():
    base = sisxmlparser.parse("templates/netquakes_response.xml")
    return base.HardwareResponse.ResponseDictGroup


"""
unity?
Sensors:
sensors/generic/RESP.XX.NS352..BNZ.UNITY_ACCEL.DC.1 (ACCELERATION)
sensors/generic/RESP.XX.NS000..BHZ.UNITY.DC.1 (VELOCITY)

Loggers:
NRL/dataloggers/generic/RESP.XX.NS000..BHZ.UNITY.1.40
"""


def nrlink(keys, eqt="SENSOR", reset=False):
    if keys == [None] or keys == []:
        return None
    else:
        sh = shelve.open("objs/shelves/nrl_links")
        if ".".join(keys) not in sh or reset:
            nrl = NRL()
            my_nrlink = None
            if eqt == "SENSOR":
                my_nrlink = nrl.sensors
            elif eqt == "LOGGER":
                my_nrlink = nrl.dataloggers

            if my_nrlink:
                while len(keys) > 0:
                    key = keys[0]
                    if key.strip() in my_nrlink:
                        my_nrlink = my_nrlink[key.strip()]
                    elif key.strip().capitalize() in my_nrlink:
                        my_nrlink = my_nrlink[key.strip().capitalize()]
                    else:
                        my_logger.info(f"couldn't get the nrl link properly for {my_nrlink}, {key}")
                    keys.remove(key)
                if type(my_nrlink) == tuple and len(my_nrlink) == 2:
                    sh[".".join(keys)] = my_nrlink[1]
                    sh.close()
                    return my_nrlink[1]
                else:
                    sh[".".join(keys)] = None
                    sh.close()
                    pass

                if keys:
                    my_logger.info(f"couldn't get a whole link from {my_nrlink}")
                return None
        else:
            sh_nrlink = sh[".".join(keys)]
            sh.close()
            return sh_nrlink







