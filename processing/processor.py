import copy
import datetime
from functions import check_none, sensitivity, convert_types, \
    component_label, overlap_serial
from processing.NRL_urls import gr2
from logger_setup import my_logger as myl
import requests
import shelve
my_logger = myl()


basalt_4x_component_rename_sites = [
    "NP.ENM",
    "NP.1576",
    "NP.1662",
    "NP.1745",
    "NP.1761",
    "NP.1767",
    "NP.1768",
    "NP.1769",
    "NP.1770",
    "NP.1771",
    "NP.2172",
    "NP.2213",
    "NP.2222",
    "NP.2272",
    "NP.2282",
    "NP.2283",
    "NP.2285",
    "NP.2286",
    "NP.2287",
    "NP.2426",
    "NP.2477",
    "NP.2838",
    "NP.5230",
    "NP.5231",
    "NP.7017",
    "NP.7023",
    "NP.7038",
    "NP.7046",
    "NP.7202",
    "NP.7203",
    "NP.7212",
    "NP.7213",
    "NP.7224",
    "NP.7226",
    "NP.7227",
    "NP.7228",
    "NP.7231",
    "NP.8049",
    "NP.8050",
]

l4c_component_rename_sites = [
    "NP.1041",
    "NP.1042",
    "NP.1462",
    "NP.2173",
    "NP.5036",
    "NP.5037",
    "NP.5053",
    "NP.5054",
    "NP.5055",
    "NP.5080",
    "NP.5081",
    "NP.5241",
    "NP.5242",
    "NP.5288",
    "NP.5289",
    "NP.5337",
    "NP.5371",
    "NP.5372",
    "NP.5375",
    "NP.5409",
    "NP.5410",
    "NP.5422",
    "NP.5423",
    "NP.5425",
    "NP.5426",
    "NP.5430",
    "NP.5431",
    "NP.5432",
    "NP.5434",
    "NP.5435",
    "NP.5491",
    "NP.5492",
    "NP.5493",
    "NP.5494",
    "NP.5495",
    "NP.7209",
    "NP.7210",
]

other_rename_sites = [
    "NP.2561",
    "NP.DIX",
    "NP.ELK",
    "NP.EMR",
    "NP.KIR",
    "NP.MCD",
    "NP.MOD",
    "NP.PLA",
    "NP.SIA",
    "NC.JST",
    "NC.PCB",
    "NC.PHC",
    "NC.PHSB",
    "NC.PJC",
    "NC.PMPB",
    "NC.PSM",
    "NC.SN01",
    "NC.SN02",
    "NC.SN05",
    "NC.SN07",
    "NC.SN09",
    "NC.SN11",
    "NC.LRD",
    "NC.LEL",
    "NC.KCR",
    "NC.KFP",
    "NC.KOM",
    "NC.KBN",
    "NC.LMI",
    "NC.HGS",
]

skipsites = [
    "NC.MDH",
    "NC.MDY",
    "NC.MINS",
    "NC.MLI",
    "NC.PBIB",
    "NP.286",
    "NP.5030",
    "NP.5062",
    "NP.5081",
    "NP.5271",
    "NP.5399",
    "NP.5402",
    "NP.5403",
    "NP.5442",
    "NP.5444",
    "NP.5480",
    "NP.5492",
    "NP.5499",
    "NP.655",
    "NP.5481",
    "NP.ENB01"
]

def date_match(d1, d2, first='neither'):
    if d1 == d2:
        return True
    else:
        if type(d1) == datetime.datetime and type(d2) == datetime.datetime:
            if first == 'neither':
                pass
            elif first == "d1":
                if d1 <= d2:
                    return True
                else:
                    return False
            elif first == "d2":
                if d1 >= d2:
                    return True
                else:
                    return False



def check_ori2(channels, sensors):
    directions = {}

    for chan in channels:
        for sen in sensors:
            if sen['chaid_fk'] == chan['chaid'] and \
                    date_match(
                        sen['sendate_inst'],
                        chan['chadate_inst'],
                        first="d1") and \
                    date_match(
                        sen['sendate_rem'],
                        chan['chadate_rem'],
                        first="d2"):
                sid = sen['senid']
                if sid not in directions:
                    directions[sid] = []
                # if 'seedchan' in chan and check_none(chan['seedchan']):
                if 'seedchan' in chan and check_none(chan['seedchan']):
                    scd = chan['seedchan'][-1] # should give orientation
                    if scd not in directions[sid]:
                        directions[sid].append(scd)
    oris = {}
    for sen in directions:
        if len(directions[sen]) == 3:
            oris[sen] = "3D"
        elif "Z" in directions[sen] or "1" in directions[sen]:
            oris[sen] = "VERTICAL"
        elif "N" in directions[sen] or "E" in directions[sen] or \
                "2" in directions[sen] or "3" in directions[sen]:
            oris[sen] = "HORIZONTAL"
        elif "O" in directions[sen]:
            oris[sen] = None
        else:
            oris[sen] = None
    return oris


# def check_ori(channels):
#     linked = {}
#     epochs = {}
#     scs = []
#     for chan in channels:
#         if 'seedchan' in chan and check_none(chan['seedchan']):
#             chid = str(chan['chaid'])
#             scd = chan['seedchan'][-1]
#             if scd not in scs:
#                 scs.append(scd)
#         # elif 'dip' in chan and check_none(chan['dip']) \
#         #         and type(chan['dip']) in [float, int]:
#         #     if 80 < (float(chan['dip']) ** 2) ** 0.5 < 100:
#         #         scs.append("Z")
#         #     else:
#         #         scs.append("3")
#             # my_logger.warning(f"no seedchan in this channel: {chan}")
#     oris = []
#
#
#
#     if len(scs) == 3:
#         return "3D"
#     # assuming that EP1 is vertical, and EP2 & EP3 are horizontal, and similar
#     # is this ever not true? Wouldn't it be better to look at dip?
#
#     if ("N" in scs or "E" in scs or "2" in scs or "3" in scs) and "Z" not in scs:
#         return "HORIZONTAL"
#     else:
#         return "VERTICAL"
#     # for e in epochs:
#     #     for d in epochs[e]:
#     #         if len(epochs[e]) == 1:
#     #
#     #             if d == "Z":
#     #                 if "VERTICAL" not in oris:
#     #                     oris.append("VERTICAL")
#     #             else:
#     #                 if "HORIZONTAL" not in oris:
#     #                     oris.append("HORIZONTAL")
#     #         else:
#     #             return "3D"
#
#     # if len(oris) == 1:
#     #     return oris[0]
#     # elif len(oris) == 2:
#     #     scs = []
#     #     for chan in channels:
#     #         if 'seedchan' in chan and check_none(chan['seedchan']):
#     #             sc = str(chan['seedchan'])
#     #             if sc not in scs:
#     #                 scs.append(sc)
#     #     my_logger.warning(f"THIS SENSOR HAS CHANNELS WITH DIFFERENT ORIENTATIONS: "
#     #                       f"{scs}, {oris}! Returning {oris[0]}")
#     #     return oris[0]
#     # elif len(oris) == 0:
#     #     my_logger.warning(f"Couldn't get oris for {channels}")
#     #     return None


def input_processor(channels, sensors, loggers, correctionlimit=0.2, amplifiers=[]):
    my_logger.info(f"going to process {len(channels)} channels, "
                   f"{len(sensors)} sensors "
                   f"and {len(loggers)} loggers")
    # orientation = check_ori(channels)
    oris = check_ori2(channels, sensors)
    cnames = []
    processed_sensors = []
    processed_channels = []
    scs = []
    # counting number of channels with each logger
    recids = {}
    for chan in channels:
        recid = chan['recid_fk']
        dt = chan['chadate_inst'].strftime('%Y/%m/%d %H:%M:%S')

        if recid not in recids:
            recids[recid] = {dt: 1}
        elif datetime not in recids[recid]:
            recids[recid][dt] = 1
        else:
            recids[recid][dt] += 1

    lrids = {}
    for rid in recids:
        nchans = 1
        for dt in recids[rid]:
            if recids[rid][dt] > nchans:
                nchans = recids[rid][dt]
        lrids[rid] = nchans

    n = 1
    for chan in channels:
        skip = False
        cnetsta = chan['netstarec'].split("-")[:2]

        if ".".join(cnetsta) in skipsites:
            skip = True
            my_logger.info(f"skipping, {cnetsta} is in skipsites!")
        rsn = chan['netstarec'].split("-")[-1]
        n += 1
        if 'pinnumber' in chan:
            cnp = check_none(chan['pinnumber'])
        else:
            cnp = None
        if cnp:
            cnames.append(chan['pinnumber'])
            chan['logpin'] = chan['pinnumber']
        elif check_none(chan['dlchan']):
            cnames.append(chan['dlchan'])
            chan['logpin'] = chan['dlchan']
        else:
        # if 'pinnumber' in chan and check_none(chan['pinnumber']):
        #     cnames.append(chan['pinnumber'])
        #     chan['logpin'] = chan['pinnumber']
        # elif 'pinnumber' not in chan and check_none(chan['dlchan']):
        #     chan['logpin'] = chan['dlchan']
        # else:
            chan['logpin'] = None

            if 'pinnumber' in chan:
                pnic = check_none(chan['pinnumber'])
            else:
                pnic = "NOT IN CHAN"
            my_logger.warning(f"Will skip chan {chan['netstarec']} {chan['seedchan']} "
                              f"because pinnumber = {pnic} "
                              f"and dlchan = {check_none(chan['dlchan'])}")
        ampgain = 1
        if 'ampgain' in chan:
            try:
                fg = float(chan['ampgain'])
                if fg > 0:
                    ampgain = fg
            except Exception as e:
                my_logger.warning(f"Looks like I couldn't convert {chan['ampgain']} to a float, {e}")
        for amp in amplifiers:
            if chan['seedchan'] == amp['seedchan'] and \
                    amp['ondate'] == chan['chadate_inst']:
                if check_none(amp['model']) and check_none(amp['serial']):
                    chan['ampmod'] = amp['model']
                    chan['ampsn'] = amp['serial']
                    if 'pag' in amp and check_none(amp['pag']) and float(amp['pag']) > 0:
                        ampgain = float(amp['pag'])


        chid = str(chan['chaid'])
        # chid = f"{chan['chaid']}_{check_none(chan['lcode'])}"

        recid = chan['recid_fk']
        relsen = None
        reldl = None
        mininsts = {}
        maxrems = {}
        for sen in sensors:
            if 'sendate_rem' not in sen or not sen['sendate_rem']:
                sdr = datetime.datetime(3000, 1, 1, 0, 0, 0)
            else:
                sdr = sen['sendate_rem']
            if not check_none(sen['sendate_inst']):
                sen['sendate_inst'] = chan['chadate_inst']
                sen['sendate_rem'] = chan['chadate_rem']
                my_logger.info(f"Sensor {sen['sentype']} {sen['unitsn']} didn't have an inst date"
                               f"so we set it to {sen['sendate_inst']}, and the rem date to "
                               f"{sen['sendate_rem']}, from the channel")
            key = f"{sen['sentype']}_{sen['unitsn']}"
            if key not in mininsts:
                mininsts[key] = [sen['sendate_inst']]
                maxrems[key] = [sdr]
            else:
                mininsts[key].append(sen['sendate_inst'])
                maxrems[key].append(sdr)

        for sen in sensors:

            snetsta = sen['netstareccha'].split("-")[:2]
            key = f"{sen['sentype']}_{sen['unitsn']}"
            if 'chadate_rem' not in chan or not chan['chadate_rem']:
                cdr = datetime.datetime(3000, 1, 1, 0, 0, 0)
            else:
                cdr = chan['chadate_rem']

            if str(chid).strip() == str(sen['chaid_fk']).strip():
                if snetsta == cnetsta:
                    if sen['sendate_inst'] == chan['chadate_inst'] and \
                            sen['sendate_rem'] == chan['chadate_rem']:
                        relsen = sen
                        break

                    if min(mininsts[key]) <= chan['chadate_inst'] \
                            and max(maxrems[key]) >= cdr:
                        relsen = sen
                        # break

# TODO in FS they should be the same.
#  In NOD are the chadates every within the sendates?
        for dl in loggers:
            if 'ln' in dl and 'ln' in chan:
                if dl['ln'] == chan['ln']:
                    reldl = dl
            else:
                if recid == dl['recid'] and rsn == dl['recsn']:
                    reldl = dl

        if reldl:
            ov_recid = overlap_serial(recid, "LOGGERS", reldl['rectype'],
                                      reldl['netsta'].split("-")[0])
            ov_recid = str(ov_recid).strip().strip("_")

            ov_recid = str(ov_recid).replace("__", "_")

            if recid != ov_recid:
                my_logger.info(f"MODIFIED DL SERIAL!, {recid}, {ov_recid}, "
                               f"{type(recid)}, {type(ov_recid)}")
                reldl['recid'] = ov_recid
                chan['recid_fk'] = ov_recid

        if relsen:
            if (check_none(relsen['sens_units'])
                    or relsen['sentype'] in ['EXTEN', 'PIEZO', 'DM']) \
                    and check_none(relsen['sentype']):
                senrespf = gr2(chan, sensor=relsen, eqt="SENSOR")
                if check_none(reldl) and check_none(reldl['rectype']):
                    logrespf = gr2(chan, dl=reldl, sensor=relsen, eqt="LOGGER")
                else:
                    logrespf = None

                nrl_sgain = None
                nrl_lgain = None
                if senrespf:
                    nrl_sgain = get_nrl_gain(senrespf)
                if logrespf:
                    nrl_lgain = get_nrl_gain(logrespf)
                db_sgain = relsen['sens']
                sunits, sdesc = sensitivity(relsen['sens_units'])
                if 'sorange' not in chan and check_none(relsen['rangeg']):
                    chan['sirange'] = relsen['rangeg']
                elif 'sirange' not in chan: # I did have this without the check for sirange
                    # why would that have been?
                    chan['sirange'] = None

                # sunits, sdesc, ssval = g_convert(sunits, sdesc, db_sgain)
                ssval = db_sgain
                # chan['sounit'] = sunits[0]

                if nrl_sgain:
                    ssval = check_similar(nrl_sgain, ssval, correctionlimit)

                if check_none(chan['sensitivity']):
                    db_lgain = chan['sensitivity']
                    chan['sensunits'] = "cpv"
                    # lunits, ldesc = sensitivity(
                    #     chan['sensunits'], inputunits=sunits[0])
                    # lunits, ldesc, lsval = g_convert(lunits, ldesc, db_lgain)
                    lsval = db_lgain
                    lsval = check_similar(nrl_lgain, lsval, correctionlimit)
                else:
                    lunits, ldesc, lsval = [sunits[0], sunits[0]], \
                                           [sdesc[0], sdesc[0]], 1
                if ssval and lsval:
                    totalsens = ssval * lsval * ampgain

                    chan['totalsens'] = round(totalsens, 1)
                elif ssval and not lsval:
                    chan['totalsens'] = ssval
                else:
                    chan['totalsens'] = 1
                chan['senrespf'] = senrespf
                if 'sorange' not in chan:
                    if relsen['rangeg']:
                        chan['sorange'] = relsen['rangeg'] #* ssval
                    else:
                        chan['sorange'] = ssval



                # if 'siunit' not in chan:
                chan['siunit'] = sunits[1]
                # if 'sounit' not in chan:
                chan['sounit'] = sunits[0]

                chan['ssval'] = ssval
                chan['sidesc'] = sdesc[1]
                chan['sodesc'] = sdesc[0]

                chan['sid'] = relsen['senid']
                chan['logcomponent'] = f"DATA{chan['logpin']}"

                chan['logrespf'] = logrespf
                chan['lsval'] = lsval
                # chan['liunit'] = lunits[1]
                # chan['lounit'] = lunits[0]
                # chan['lidesc'] = ldesc[1]
                # chan['lodesc'] = ldesc[0]
                chan['liunit'] = chan['sounit']
                chan['lidesc'] = chan['sodesc']
                chan['lounit'] = "count"
                chan['lodesc'] = "Digital counts"
                if not check_none(chan['seedchan']):
                    chan['seedchan'] = relsen['netstareccha'].split("-")[-1]
                chan['sencomponent'] = component_label(chan['seedchan'])
                scs.append(chan['sencomponent'])

                if check_none(reldl):
                    if check_none(reldl['rectype']):
                        if reldl['recid'] in lrids:
                            lchans = lrids[reldl['recid']]
                        else:
                            lchans = 1
                        lmodel = convert_types(reldl['rectype'],
                                               eqtype="dl", lchans=lchans)
                        chan['lmodel'] = lmodel
                        reldl['rectype'] = lmodel
                else:
                    chan['lmodel'] = None
                usn = unitsn(chan, relsen, reldl)
                chan['unitsn'] = usn
                relsen['unitsn'] = usn

                orientation = None
                if usn in oris:
                    orientation = oris[usn]
                if sen['senid'] in oris:
                    orientation = oris[sen['senid']]

                if 'chadep' in chan:
                    dep = chan['chadep']
                else:
                    dep = 0 # elevation is included in NOD,
                    # but not depth
                smodel = convert_types(relsen['sentype'],
                                       eqtype="sen",
                                       orientation=orientation,
                                       # orientation=oris[sen['senid']],
                                       chaname=chan['seedchan'],
                                       depth=dep)
                relsen['sentype'] = smodel
                usn2 = overlap_serial(usn, "SENSORS", smodel,
                                      relsen['netstareccha'].split("-")[0])
                usn2 = str(usn2).strip().strip("_")
                if usn2 != usn:
                    my_logger.info(f"changed usn from {usn} to {usn2}")
                    chan['unitsn'] = usn2
                    relsen['unitsn'] = usn2

                netsta = f"{chan['netstarec'].split('-')[0]}." \
                         f"{chan['netstarec'].split('-')[1]}"
                if smodel == "L-4C VERTICAL" and netsta in \
                        l4c_component_rename_sites:
                    if chan['sencomponent'] == "Z":
                        chan['sencomponent'] = "VERTICAL"

                if not smodel:
                    my_logger.warning(f"why didn't smodel work? {relsen['sentype']}")
                chan['smodel'] = smodel
                relsen['sentype'] = smodel

                az = None
                if 'orientation' in chan and 'azim' not in chan \
                        and check_none(chan['orientation']):
                    chan['azim'] = chan['orientation']
                    az = chan['orientation']
                elif 'azim' in chan:
                    az = chan['azim']
                    try:
                        az = float(az)
                        while az >= 360:
                            az -= 360
                        while az < 0:
                            az += 360
                        chan['azim'] = az
                    except ValueError:
                        my_logger.info(f"Looks like the azimuth {az} can't "
                                       f"be converted to a float")
                else:
                    chan['azim'] = None

                if 'dip' not in chan and az:
                    if az not in ["UP", "DOWN"]:
                        dipv = 0
                    else:
                        dipv = 90 * int(az == "UP") - 90 * int(az == "DOWN")
                    chan['dip'] = dipv
                elif 'dip' in chan:
                    if type(chan['dip']) == str:
                        try:
                            chan['dip'] = float(chan['dip'])
                        except Exception as e:
                            my_logger("couldn't convert", chan['dip'], "to float", e)
                if 'dip' not in chan:
                    chan['dip'] = 0.0

                rsc = copy.deepcopy(relsen)
                cc = copy.deepcopy(chan)
                if check_none(chan['siunit']):
                    if "m/s" in cc['siunit'] and "m/s**2" not in cc['siunit']:
                        rsc['rangeg'] = None
                        cc['sorange'] = None
                        cc['sirange'] = None
                else:
                    chan['siunit'] = None
                # is this the problem?
                if 'sentype' in rsc and 'sencomponent' in cc \
                        and 'seedchan' in cc and 'azim' in cc \
                        and check_none(cc['unitsn']) and check_none(rsc['sentype']) \
                        and not skip:
                    if check_none(chan['siunit']):
                        processed_sensors.append(rsc)
                        processed_channels.append(cc)
                else:
                    my_logger.warning(f"SKIPPING CHANNEL: {chan['netstarec']} "
                                      f"{chan['seedchan']} {chan['chadate_inst']} "
                                      f"{chan['chaid']} sensor :{relsen}, {skip} ")

        else:
            my_logger.warning(f"NO RELEVANT SENSOR! {chid}")

    for chan in processed_channels:
        netsta = f"{chan['netstarec'].split('-')[0]}.{chan['netstarec'].split('-')[1]}"
        # units are proper here
        idx = processed_channels.index(chan)
        pidx = processed_channels[idx]
        if 'pinnumber' in chan:
            if pidx['pinnumber'] not in cnames:
                pidx['logpin'] = pidx['pinnumber']

        pidx['logcomponent'] = f"DATA{pidx['logpin']}"

        # a bit worried that this will change things for other models
        if netsta in basalt_4x_component_rename_sites:
            for n in [5, 6, 7]:
                processed_channels[idx]['logcomponent'] = \
                    processed_channels[idx]['logcomponent'].replace(
                        f"DATA{n}", f"MASS-POSITION{n}")
        if ('lmodel' in chan and chan['lmodel'] in ["CENTAUR", "130-01", "Q330S+"]) or \
                (chan['recid_fk'] in ['AB4F', 'AB8B', '7186', '6892']):
            if netsta in other_rename_sites:
                for n in [4, 5, 6, 10, 11, 12]:
                    processed_channels[idx]['logcomponent'] = \
                        processed_channels[idx]['logcomponent'].replace(
                            f"DATA{n}", f"MASS-POSITION{n}")

        dupenames = {"VERTICAL": "Z",
                     "COMPONENT2": "X",
                     "COMPONENT3": "Y"}
        for n in dupenames:
            if n in scs and chan['sencomponent'] == dupenames[n]:
                chan['sencomponent'] = n

    return processed_channels, processed_sensors


# def g_convert(units, descriptions, value):
#     # converts gs to m/s**2 in units (and related values)
#     for unit in units:
#         idx = units.index(unit)
#         if unit == "G-mks":
#             units[idx] = "m/s**2"
#             descriptions[idx] = "Acceleration in Meters per Second per Second"
#             value *= 9.80665**(-1.0**int(idx == 0)) # converts to 1/g
#             # if the unit is on the denominator
#         if unit == "g":
#             units[idx] = "m/s**2"
#             descriptions[idx] = "Acceleration in Meters per Second per Second"
#             value *= 9.80665 ** (-1.0 ** int(idx == 0))
#     return units, descriptions, value


def get_nrl_gain(nrlurl):
    already_processed_gains = shelve.open("objs/shelves/nrl_gains")
    if nrlurl not in already_processed_gains:
        r = requests.get(nrlurl)
        content = r.content
        decoded = bytes.decode(content).split("\n")
        for line in decoded:
            if "Sensitivity" in line:
                sens = line.split()[-1]
        try:
            fsens = float(sens)
            already_processed_gains[nrlurl] = fsens
            already_processed_gains.close()
            # fh = open("objs/nrl_gains.obj", "wb")
            # pickle.dump(already_processed_gains, fh)
            # fh.close()

            return fsens
        except Exception as e:
            my_logger.warning(f"couldn't convert {sens} to a float. {e}")
            already_processed_gains[nrlurl] = None
            already_processed_gains.close()
            # fh = open("objs/nrl_gains.obj", "wb")
            # pickle.dump(already_processed_gains, fh)
            # fh.close()
            return None
    else:
        gain = already_processed_gains[nrlurl]
        already_processed_gains.close()
        return gain


def unitsn(channel, sensor, dl):
    sensn = str(sensor['sensn']).strip()
    senid = str(sensor['senid']).strip()
    if channel:
        recnum = channel['netstarec'].split("-")[2]
    elif sensor:
        recnum = sensor['netstareccha'].split("-")[2]
    elif dl:
        recnum = dl['recsn']

    if check_none(sensor['sentype']) and "DECK" in sensor['sentype']:
        if dl:
            # return f"{dl['rectype']}-{recnum}"
            # return f"NOVALIDLOGGER-{recnum}"
            return f"DECK-{recnum}"
        else:
            return f"DECK-{recnum}" #
    elif check_none(sensor['sentype']) and "TITAN" in sensor['sentype']:
        if "-" in sensn:
            sensn = sensn.split("-")[0]
        if " " in sensn:
            sensn = sensn.split(" ")[0]
        return f"TITAN_SMA {sensn}"
    elif check_none(str(sensor['unitsn'])):
        return str(sensor['unitsn'])
    else:
        if "-" in sensn:
            sensn = sensn.split("-")[0]
        if " " in sensn:
            sensn = sensn.split(" ")[0]
        if not check_none(senid): ############ added this so I get more sensors & channels...?
            sensn += f"_{senid}"
            sensn = sensn.strip("_")

        if not check_none(sensn):
            if dl and check_none(dl['recsn']):
                nsrc = sensor['netstareccha']
                r = nsrc.split("-")[2]
                sensn = f"{r}_SENSOR"
            else:
                return "UNKN"

        return sensn


def check_similar(original, replacement, acceptablediff):
    # if replacement is more than aceptablediff off from original, returns replacement
    if replacement and original:
        fracoff = float(original)/float(replacement)
        if fracoff > 1 + acceptablediff or fracoff < 1 - acceptablediff:
            return float(replacement)
        else:
            return float(original)
    elif original:
        return float(original)
    elif replacement:
        return float(replacement)

