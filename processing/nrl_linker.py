# from obspy.clients.nrl import NRL
from functions import check_none
from logger_setup import my_logger
# nrl = NRL()
logger = my_logger()


def zstrip(v):
    sv = str(v)
    return sv.replace(".0", "").strip()


class Equipment:
    def __init__(self, mytype, rate, sensitivity,
                 eqt="SENSOR", filtertype=None, pag="1", rangeg=None, fir=None,
                 gcorrect=1):
        self.type = mytype
        self.rate = zstrip(rate)
        self.sens = zstrip(sensitivity)
        self.eqt = eqt
        self.causal = filtertype
        self.pag = zstrip(pag)
        self.rangeg = zstrip(rangeg)
        self.fir = fir
        self.gcorrect = gcorrect
        if not self.type:
            self.keys = [None]
            brand = None
        else:
            # brand = self.brand()
            # self.keys = self.brand()
            self.keys = [self.brand()[0]]
            brand = self.keys[0]
            # self.keys = [brand]
        if brand == "Kinemetrics":
            self.Kinemetrics()
        elif brand == "REF TEK":
            self.Reftek()
        elif brand == "Nanometrics":
            self.Nanometrics()
        elif brand == "Guralp":
            self.Guralp()
        elif brand in ["GEOsig", "GeoSIG"]:
            self.Geosig()
        elif brand == "Sprengnether (now Eentec)":
            self.Sprengnether()
        elif brand == "Streckeisen":
            self.Streckeisen()
        elif brand == "Geo Space/OYO":
            self.Geospace()
        elif brand == 'Sercel/Mark Products':
            self.Sercel()
        elif brand == "EQMet":
            self.EQMet()
        elif brand == "Quanterra":
            self.Quanterra()
        elif brand == 'Metrozet':
            self.Metrozet()
        else:
            if not brand:
                brand = "NO BRAND"
            logger.info(f"Can't find NRL link for "
                        f"brand = {brand} and type = {self.type}")

    def brand(self):
        # we need to accept some sensor-logger overlap - for internal sensors

        sensor_brands = {
            "Nanometrics": ["TITAN", "TRILLIUM"],
            "EQMet": ["TSA"],
            'Sercel/Mark Products': ["L22", "L-4C", "L4"],
            # "Geo Space/OYO": ["GEOSPACE", "DM"],
            "Guralp": ["CMG"],
            "Streckeisen": ["STS"],
            "Sprengnether (now Eentec)": ["S6000"],
            # "REF TEK": ["RT-", "130", "131", "147"],
            "GEOsig": ["GEOSIG"], # these are loggers
            "Kinemetrics": ["K2", "WHITNEY", "BASALT",
                            "GRANITE", "ETNA", "FBA",
                            "EPISENSOR", "ES-",
                            "EPI", "SSA"],
            "Metrozet": ["MBB-2"]
        }

        logger_brands = {
            # "Geo Space/OYO": ["DM"],
            # "REF TEK": ["REFTEK", "130"],
            "Nanometrics": ["NANOMETRICS", "TITAN", "CENTAUR", "TAURUS",
                            "HRD-24", "TRIDENT"],
            "Quanterra": ["Q330", "330S+"],
            "GeoSIG": ["IA18"],
            "EQMet": ["TSA-SMA"],  # watch for kinemetrics sensor taking SMA first
            "Kinemetrics": ["DSA", "SMA", "K2", "BASALT", "ETNA", "WHITNEY",
                            "GRANITE", "OBSIDIAN", "SSA", "SSA-1", "SSA-2"],
        }
        if self.eqt == "SENSOR":
            for brand in sensor_brands:
                for model in sensor_brands[brand]:
                    if model in self.type.upper():
                        return [brand, model]
            # internal sensors...
            for brand in logger_brands:
                if self.type.upper() == "TITAN":
                    print("okay, so it's a nanometrics titan. What gives?")
                for model in logger_brands[brand]:
                    if model in self.type.upper():
                        return [brand, model]
        elif self.eqt == "LOGGER":
            for brand in logger_brands:
                for model in logger_brands[brand]:
                    if model in self.type.upper():
                        return [brand, model]
        return [None]

    def fsv(self):
        val = float(self.sens) * float(self.rangeg) * self.gcorrect
        rval = round(val/2.5)*2.5  # this rounds to the nearest 2.5
        roundage = 0
        if rval != 0:
            roundage = 1 - (val/rval)
        if roundage > 0.2:
            logger.warning(f"Had to round the kinemetrics filter by more than 20% "
                           f"{self.type}")
            print(f"val = {val}, rval = {rval}")

        if 0 < rval < 3.5:
            return '+/- 2.5V Single-ended'
        elif 3.5 < rval <= 7.5:
            return '+/- 5V Differential'
        elif 7.5 < rval <= 15.0:
            return '+/- 10V Single-ended'
        elif 15.0 < rval:
            return '+/- 20V Differential'
        else:
            if rval < 2.5:
                return '+/- 2.5V Single-ended'

    def Kinemetrics(self):
        if self.eqt == "SENSOR":
            rangegs = ['0.25', '0.5', '1', '2', '4']
            if "ES-" in self.type.upper() or "EPISENSOR" in self.type.upper():
                self.keys.append('Episensor (ES-T, ES-U, ES-U2, DS-DH, SBEPI)')
                self.keys.append(self.fsv())
                if self.rangeg in rangegs:
                    self.keys.append(f"{self.rangeg}g")
                else:
                    rg2 = round(float(self.rangeg)/0.25)*0.25
                    if rg2 < 1:
                        rg3 = str(rg2)
                    else:
                        rg3 = str(int(rg2))
                    if rg3 in rangegs:
                        self.keys.append(f"{rg3}g")
            elif "EPI" in self.type.upper():
                self.keys.append('Episensor (ES-T, ES-U, ES-U2, DS-DH, SBEPI)')
                if "2G-int" in self.type:
                    self.keys.extend(('+/- 2.5V Single-ended', '2g'))
                elif "2G-ext" in self.type:
                    self.keys.extend(('+/- 10V Single-ended', '2g'))
                elif "4G-int" in self.type:
                    self.keys.extend(('+/- 2.5V Single-ended', '4g'))
                elif "4G-ext" in self.type:
                    self.keys.extend(('+/- 2.5V Single-ended', '4g'))
                else:
                    self.keys.append(self.fsv())
                if self.rangeg in rangegs:
                    self.keys.append(f"{self.rangeg}g")
                    # self.keys.extend(('+/- 2.5V Single-ended', '2g')) #########
                    ########### don't know this, just filling with something
            elif "FBA" in self.type.upper():
                if "11" in self.type:
                    self.keys.extend(('FBA-11', '1g'))
                elif "13" in self.type:
                    self.keys.extend(('FBA-13', '1g'))
                elif "23" in self.type:
                    self.keys.append('FBA-23')
                    end = self.type.split("-")[-1]
                    if end[-1].upper() == "G":
                        self.keys.append(f"{end[0]}g")
                    else:
                        if self.rangeg in ['0.25', '0.5', '1', '2', '4']:
                            rf = float(self.rangeg)
                            self.keys.append(f"{self.rangeg}g")
                            if check_none(self.rate) and float(self.rate) > 50.0:
                                if rf < 1.5:
                                    self.keys.append('100 Hz')
                                else:
                                    self.keys.append('90 Hz')
                            else:
                                self.keys.append('50 Hz')
                else:
                    if check_none(self.rate) and float(self.rate) > 50:
                        self.keys.append('FBA-23')
                        sf = None
                        if self.rangeg in \
                                ['0.25', '0.5', '1', '2', '4']:
                            self.keys.append(f"{sf}g")
                        if sf and sf in ['2', '4']:
                            self.keys.append('90 Hz')
                        else:
                            self.keys.append('100 Hz')
                    # print(self.type, self.rate, self.rangeg, self.sens)
            elif "SS-1" in self.type:
                self.keys.extend(('SS-1 Ranger', '345 V/m/s'))
            else:
                logger.info(f"Is this a sensor in NRL? {self.type}?")
        elif self.eqt == "LOGGER":
            if "K2" in self.type:
                self.keys.extend(("K2", self.pag, self.causal, self.rate))
                # pre-amp gain
            elif "ETNA" in self.type:
                if "2" not in self.type:
                    self.keys.append("Etna")
                else:
                    self.keys.append("Etna 2")
                self.keys.append(self.rate)
                if "2" in self.type:
                    self.keys.append(self.causal)
            elif "BASALT" in self.type.upper() or \
                    "GRANITE" in self.type.upper() or \
                    "DOLOMITE" in self.type.upper() or \
                    "OBSIDIAN" in self.type.upper():
                self.keys.extend((
                    'Rock Family (Basalt, Granite, Dolomite, Obsidian)',
                    self.pag))
                self.keys.append(str(int(round(2**24 / float(self.sens)))))
                self.keys.extend((str(self.causal), self.rate))
            elif "WHITNEY" in self.type:
                self.keys.append("Mt. Whitney")
                if int(self.pag) in [1, 3, 10, 30, 100]:
                    self.keys.append(str(self.pag))
                self.keys.append(self.causal)
                if self.rate in \
                        ['20', '40', '50', '100', '200', '250']:
                    self.keys.append(self.rate)

            else:
                logger.info(f"Couldn't get a NRL link for {self.type}")
                # print(f"Couldn't get a NRL link for {self.type}")

                # TODO: issue with SMA-1?
    def Reftek(self):
        self.keys = []
        # Jacob said to skip all Reftek NRL links

        # if self.eqt == "SENSOR":
        #     if "131" in self.type or "130" in self.type:
        #         if "SMHR" not in self.type.upper():
        #             self.keys.extend(('RT 131 (also 130-SMA)', '131A-02 (also 130-SMA)'))
        #             if round(self.rangeg) == 3:
        #                 self.keys.append('SF1500S')
        #             elif round(self.rangeg) == 4.0:
        #                 pass
        #         # self.keys.extend((str(self.pag), str(self.rate)))
        #         # print("keys = ", self.keys, self.type)
        #         # '131A-02 (also 130-SMA)', '131B-01', '131B-02/3'
        #         # 'SF1500S', 'SF1600', 'SF3000L'
        #     elif "147" in self.type or "SMHR" in self.type.upper():
        #         self.keys.extend(('RT 147 & 147A (also 130-SMHR)', '147'))
        #     elif "151" in self.type:
        #         self.keys.append("RT 151")
        # elif self.eqt == "LOGGER":
        #     if "130" in self.type:
        #         if "SMHR" in self.type:
        #             self.keys.append("RT 130S & 130-SMHR")
        #         else:
        #             self.keys.append("RT 130 & 130-SMA")
        #         self.keys.extend((self.pag, self.rate))

    def Nanometrics(self):
        if self.eqt == "SENSOR":
            if "TITAN" in self.type.upper():
                self.keys.extend(('Titan Accelerometer', '4g'))
                # all FS are 4g clip level, what about NOD?

            elif "TRILLIUM" in self.type.upper(): # \
                   # and "COMPACT" in self.type.upper():
                self.keys.extend((
                    'Trillium Compact 120 (Vault, Posthole, OBS)',
                    '754 V/m/s'))
            else:
                # TODO: 120P? Do I have a response for that?
                print("what do I do this type of nanometrics?", self.type)
        elif self.eqt == "LOGGER":
            if "TITAN" in self.type.upper():
                logger.info(f"making assumptions about {self.type}")
                self.keys.extend(("Titan SMA", "Off", "Linear phase"))
                self.keys.append(self.rate)
                ########### don't know this, just guessing

            elif "HRD" in self.type.upper():
                logger.info(f"making assumptions about {self.type}")
                self.keys.extend(("HRD-24", "1"))
                if self.rate in ['10', '20', '40', '50', '80',
                                 '100', '120', '125', '200',
                                 '250', '500', '1000']:
                    self.keys.extend((self.rate, "Off"))
            elif "TAURUS" in self.type.upper():
                self.keys.append("Taurus")
                if self.rangeg in ['40', '20', '4', '2', '1']:
                    rg = self.rangeg
                else:
                    rg = '40'
                d1 = 16/float(rg)
                if d1 >= 1:
                    d1 = int(d1)
                self.keys.append(f"{rg} Vpp ({d1})")
                self.keys.extend(("Low (default)", 'Off'))
                if self.rate in ['10', '100', '1000', '120',
                                 '20', '200', '250', '40', '50', '500','80']:
                    self.keys.append(self.rate)
                else:
                    self.keys.append('200')
            elif "TRIDENT" in self.type.upper():
                self.keys.append("Trident")
                rg = self.rangeg
                ssrg = str(rg).strip()
                if ssrg in ['40', '20', '4', '2', '1']:
                    pass
                else:
                    ssrg = '40'
                d1 = 16 / float(ssrg)
                if d1 >= 1:
                    d1 = int(d1)
                self.keys.append(f"{ssrg} Vpp ({d1})")
                self.keys.append("Off")
                ssr = str(self.rate).strip()
                if ssr in ['10', '100', '1000', '120',
                                 '20', '200', '250', '40', '50', '500','80']:
                    pass
                else:
                    ssr = '200'
                self.keys.append(ssr)

            # if "LYNX" in self.type.upper():
            #     self.keys.extend(('HRD-24', '1'))
            #     self.keys.extend((str(self.rate), 'Off'))



    def Guralp(self):
        if "CMG-40T" in self.type.upper() or "CMG40T" in self.type.upper():
            self.keys.extend(("CMG-40T", '60s - 50Hz'))
            # need to get this:
            # looks like everything is 100 hz
            #   '100s - 50Hz', '10s - 100Hz', '1s - 100Hz', '20s - 50Hz',
            #   '2s - 100Hz', '30s - 100Hz', '30s - 50 Hz', '40s - 100Hz',
            #   '5s - 100Hz', '60s - 100Hz', '60s - 50Hz'
            if self.sens in ['1600', '2000', '20000', '800']:
                self.keys.append(self.sens)
            else:
                print("Had a problem with this sensitivity: "
                      "sens = ", self.sens)
        elif "ESP" in self.type.upper():
            self.keys.append("CMG-3ESP")
            self.keys.append("60 s - 50 Hz") # just sort of guessing here
            if self.sens in ['1500', '2000', '20000']:
                self.keys.append(self.sens)
            elif float(self.sens) < 1750:
                self.keys.append('1500')
            elif float(self.sens) < 3000:
                self.keys.append('2000')
            else:
                self.keys.append('20000')

    def Geosig(self):
        if self.eqt == "SENSOR":
            self.keys.append("AC-73")
            if self.rangeg in ['0.5', '1', '2', '3', '4']:
                self.keys.append(f"{self.rangeg}g")
        elif self.eqt == "LOGGER":
            self.keys.append("GMSplus")
            logger.info(f"I don't know the full scale input range. "
                        f"I have rangeg = {self.rangeg} for {self.type}. "
                        f"I'm GUESSING that it's 10V")
            self.keys.append("+/-10 V")
            if self.rate in ['40', '50', '100', '200', '250', '500', '1000']:
                self.keys.append(self.rate)


    def Sprengnether(self):
        self.keys.append("S6000")
        print("got a sprengnether", self.type)
        # coil and shunt resistances?

    def Streckeisen(self):
        if 'STS-2.5' in self.type.upper():
            self.keys.append("STS-3")
        elif 'STS-2' in self.type.upper():
            self.keys.extend(("STS-2", "1500", "3 - installed 04/97 to present"))
        # sensitivities are often between 1500 and 1600 - but
        # definitely closer to 1500 than 2000
        # also all of them are newer than '97 in FS, but not sure about NOD

    def Metrozet(self):
        self.keys.append("MBB-2")

    # def Geospace(self):
    #     # custom response!!!
    #     # print(self.type, self.rate)
    #     self.keys.append('HS-1-LT')
    #     print("got a geospace", self.type)
    #     # coil resistance
    #     # '2500 Ohms', '3810 Ohms'
    #     # 2500 is 2 Hz, 3810 has shunt resistance, or not

    def Sercel(self):
        rounded_sens = round(float(self.sens)/10.0)*10.0
        if rounded_sens != 0.0:
            if abs(1 - float(self.sens)/rounded_sens) > 0.2:
                logger.warning(f"Had to round the sercel filter "
                               f"by more than 20% {self.type}")
        if "22" in self.type:
            self.keys.append("L-22D")
            if rounded_sens == 20.0:
                self.keys.extend(("210 Ohms", "857 Ohms"))
            elif rounded_sens == 30.0:
                self.keys.extend(("325 Ohms", "1327 Ohms"))
            elif rounded_sens == 60.0:
                self.keys.extend(("510 Ohms", "2516 Ohms"))
            elif rounded_sens == 90.0:
                self.keys.extend(("5470 Ohms", "20000 Ohms"))
            elif rounded_sens == 120.0:
                self.keys.extend(("8540 Ohms", "42133 Ohms"))
            elif rounded_sens == 500.0:
                # this makes no sense, but the sensor is called L22 - 5500
                self.keys.extend(("5470 Ohms", "20000 Ohms"))
            else:
                print("wasn't sure what coil resistance corresponded "
                      "with sens = ", self.sens, rounded_sens, self.type)
        elif "4C" in self.type or "4" in self.type:
            pass
            # rounded_sens = round(float(self.sens) / 25.0) * 25.0
            # if rounded_sens != 0.0:
            #     if abs(1 - float(self.sens) / rounded_sens) > 0.2:
            #         logger.warning(f"Had to round the sercel filter "
            #                        f"by more than 20% {self.type}")
            #         print(f"Had to round the sercel filter"
            #               f" by more than 20% "
            #               f"{self.type}, {self.sens}, {rounded_sens}")
            #
            # self.keys.append("L-4C")
            # if rounded_sens <= 50.0:
            #     # CALLING THIS <= BECAUSE 38 IS APPARENTLY CLOSE ENOUGH?
            #     self.keys.extend(("500 Ohms", "810 Ohms"))
            # elif 50.0 < rounded_sens <= 125.0:
            #     self.keys.extend(("2000 Ohms", "3238 Ohms"))
            # elif 125.0 < rounded_sens:
            #     self.keys.extend(("5500 Ohms", "8905 Ohms"))
            # else:
            #     print("wasn't sure what coil resistance corresponded "
            #           "with sens = ", self.sens, rounded_sens)
        # print("got a sercel", self.type)
        # need coil resistance
        #  '210 Ohms', '2200 Ohms', '325 Ohms', '510 Ohms', '5470 Ohms',
        #   '8540 Ohms'

    def EQMet(self):
        if self.eqt == "SENSOR":
            self.keys.append('TSA-100S & TSA-SMA')
            vrange = 2**24/float(self.sens)
            if vrange <= 5:
                self.keys.append('+/- 5V Single-ended (TSA-SMA)')
            elif vrange <= 10:
                self.keys.append('+/- 10V Single-ended')
            elif vrange <= 20:
                self.keys.append('+/- 20V Differential')
        elif self.eqt == "LOGGER":
            self.keys.append("SMA")
            options = ['50', '100', '200', '250', '500']
            if self.rate in options:
                self.keys.append(self.rate)

    def Quanterra(self):
        logger.info(f"ASSUMING THAT {self.type} IS ALWAYS LINEAR")
        if "+" in self.type:
            self.keys.append("Q330S+")
        else:
            self.keys.append("Q330SR")
        self.keys.append(str(int(float(self.pag))))
        if check_none(self.rate):
            self.keys.append(self.rate)
            fs = self.fir.split(".")[-1]
            if fs == "all":
                self.keys.append('LINEAR AT ALL SPS')
            elif fs == "below100":
                self.keys.append('LINEAR BELOW 100 SPS')

"""
  'Episensor (ES-T, ES-U, ES-U2, DS-DH, SBEPI)', 'Episensor 2',
  'FBA-11', 'FBA-13', 'FBA-23', 'FBA-3', 'SS-1 Ranger'
"""


"""
REFTEK 130-01 ch1-3 (external sensor)  1.589 uV/count
REFTEK 130-01 ch4-6 (external sensor)  1.589 uV/count
REFTEK 131A-02/3 (external)  2.4 V/g


not sure how to finish refteks. look it up, but move on
"""

"""
  'Meridian Compact Posthole', 'Meridian Posthole',
  'Titan Accelerometer', 'Trillium 120 Horizon', 'Trillium 120P/PA',
  'Trillium 120Q/QA,PH,BH (Vault, Posthole or Borehole)',
  'Trillium 240', 'Trillium 360', 'Trillium 40',
  'Trillium All-Terrain', 'Trillium Cascadia Accelerometer',
  'Trillium Cascadia seismometer', 'Trillium Compact 120 (Vault,
  Posthole, OBS)', 'Trillium Compact 20 (Vault, Posthole, OBS)'

"""



"""
Not in NRL:
RFT-250, RFT-350 (TELEDYNE not in sensors/loggers)
DCA-333, DCA-300 (TERRATECH not in sensors/loggers)
DILATOMETER (SACKS-EVERTSON not in sensors/loggers)
WILCOXON-731 (WILCOXON not in NRL)
Kinemetrics SH-1, SMA*
SETRA204-barometer (SETRA not in NRL)
PRESSURE, VELOCITY (what are these??)
SS-202, EXTEN, PIEZO, DSA102, SA-102, SA-101, SA-102, STD, 
L4, L4C, DIGIQUARTZ 8DP, DIGIQUARTZ, R2 (not in SIS)
ARS-14, MO-2 (who is manufacturer?)

"""

"""
Loggers not in NRL:
National* 
CUSP (what is brand?)
AR-240, RFT-250, RFT-350 (TELEDYNE not in sensors/loggers)
DCA-333, DCA-300 (TERRATECH not in sensors/loggers)
SCOPE, STD, Q4128, ALARM, CRA-1, DIALER, MALAKU (not in SIS)
MO-2 (who is manufacturer?) NEW ZEALAND? not in NRL
GEOS (USGS manufactured? not in NRL)
NCSN-DST (unknown manufacturer)
"""