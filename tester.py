from functions import openpickle, check_none, picklewriter
from sis_api.sis_api import query
from processing.processor import input_processor
import sisxmlparser3_0 as sisxmlparser
from datetime import datetime


class DB_contents:
    def __init__(self, obj, stacode=None):
        self.db = openpickle(obj)
        self.stacode = str(stacode)


    def keys(self):
        # prints keys...

        for key in self.db:
            print(key, type(self.db[key]))

    def sens(self):
        n = 0
        for sen in self.db['sensors']:
            if self.stacode in sen['netstareccha']:
                print(sen)

    def dls(self):
        n = 0
        for dl in self.db['recorders']:
            if dl['rectype'] == "CRA-1" and dl['recsn'] == '103':
                print(dl)
            # if not check_none(dl['recdate_inst']):
            #     n += 1
        # print(n)
            # if self.stacode in dl['netsta']:
            #     print(dl)

    def stas(self):
        n = 0
        # print(len(self.db['sites']))
        # print(len(self.db['stations']))
        for sta in self.db['stations']:
            if str(sta['stacode']) == self.stacode:
                print(sta)
            # if 'netcode' not in sta:
            #     print(f"WHY ISN'T IT HERE? IT WAS HERE FOR THE LAST {n}")
            #     break
            # else:
            #     n += 1

    def chans(self):
        valid_hcs = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12, 13, 14, 15, 20, 50
        ]
        n = 0
        for chan in self.db['channels']:
            if self.stacode in chan['netstarec']:
                print(chan)
            # if 'T2' in chan['netstarec']:
                # if chan['chadate_inst'] == datetime(1984, 1, 1, 0, 0):
            # if chan['pinnumber'] == '147':
            #     print(chan['chadate_inst'], chan['chadate_rem'], chan['recid'], chan['ln'])
                # if chan['dlchan'] == '147':
                #     print(chan['chadate_inst'], chan['chadate_rem'])
            # if chan['chadate_inst'] == datetime
            # if "103" == chan['netstarec'].split("-")[-1]:
            #     print(chan)
        # print("hi!", len(self.db['channels']))
        # nodupes = []
        # print(len(self.db['channels']))
        # for chan in self.db['channels']:
        #     print(chan)
            # if "NP-1484" in chan['netstarec']:
            #     if chan['seedchan'] == "HN3":
            #         if chan['lcode'] == "RA":
            #         # print(chan)
            #             print(chan['seedchan'], chan['lcode'], chan['chadate_inst'], chan['chadate_rem'])
                    # print(chan['chaid'], chan['chadate_inst'], chan['chadate_rem'])
                    # print(chan)
            # if chan not in nodupes:
            #     nodupes.append(chan)
        # print(len(nodupes))
            # nsr = chan['netstarec'].split("-")
            # if nsr[1] == "1098" and chan['dlchan'] == 1 and nsr[2] == "6183":
            #     print(chan)
        #     if "NP-1065" in chan['netstarec']:
        #         print(chan[''])
            # if not check_none(chan['chadate_inst']):
            #     print("no inst date?")
            #     n += 1
        # print(n)
            # if self.stacode in chan['netstarec']:
            #     print(chan)
            # if str(chan['chaid']) == "1116": # or 0087 or 1116
            #     print(chan['seedchan'], chan['netstarec'], chan['src'])
            # hc = chan['cosmoscode']
            # if int(hc) not in valid_hcs:
            #     print(f"Looks like channel {chan['chaid']} at {chan['netstarec']} has "
            #           f"an invalid housing code: {hc}")


def fix_amp(mod="DEVELCO"):
    amp = "xmls/amplifiers/"
    pass


def check_nod():
    nod = DB_contents("objs/NOD_dic.obj", "-2819-")
    # nod.keys()
    # print("dls:")
    # nod.dls()
    print("sens:")
    nod.sens()
    print("Stations:")
    nod.stas()
    print("Channels:")
    nod.chans()


def check_fs():
    fs = DB_contents("objs/FS_dic.obj", "")
    fs.chans()
    # print("~~~~~~~~~~~~")
    # fs.stas()
    # print("~~~~~~~~~~~~")
    # fs.sens()
    # fs.dls()

def check_processed():
    # pd = DB_contents("objs/processed.obj")
    pd = openpickle("objs/processed.obj")
    # for key in pd['sites']['NC.MHI']:
    #     print(key, len(pd['sites']['NC.MHI'][key]))
    #
    # for key in pd['sens']:
    #     if "L-4C VERTICAL_4376" in key:
    #         asdf = pd['sens'][key]
    #         for sen in asdf['psens']:
    #             print(sen['netstareccha'])
            # for k2 in asdf:
            #     print(k2, len(asdf[k2]))
            # for sen in pd['sens'][key]:
            #     print(sen['netstarec'])

    mods = []
    for key in pd['sites']:
        if key in ['NP.1721', 'NP.2477']:
            print(pd['sites'][key]['dls'])


    # print("hi!")
    # pd.keys()
    # pd.stas()

def check_grouped():
    # pd = openpickle("objs/grouped_eq.obj")
    pd = openpickle("objs/NOD_dic.obj")
    for dl in pd['recorders']:
        if "1462" in str(dl['recsn']):
            print(dl)
    print("~~~~~~~~~~~~~~~~~~~~~~")
    for chan in pd['channels']:
        if "1462" in chan['netstarec']:
            print(chan)
        # print(key, len(key))
        # if "L4C_4376" in key:
        #     d = pd[key]
        #     # print(len(d['chans']), type(d['chans']))
        #     # for chan in d['chans']:
        #     #     if "MHI" in chan['netstarec']:
        #     #         print(chan)
        #     # print("~~~~~~~~~~~~~~")
        #     # for sen in d['sens']:
        #     #     if "MHI" in sen['netstareccha']:
        #     #         print(sen)
        #     # print(len(d['dls']))
        #     pchans, psens = input_processor(d['chans'], d['sens'], d['dls'])
        #     for ps in psens:
        #         if "MHI" in ps['netstareccha']:
        #             print(ps)
        #
        #     print(len(psens))

def lens():
    vanilla_grouped = openpickle("objs/fs_nod_grouped.obj")
    grouped_eq = openpickle("objs/grouped_eq.obj")
    processed = openpickle("objs/processed.obj")

    print("vanilla_grouped has this many sensors", len(vanilla_grouped['sensors']))
    print("processed has this many sensors", len(processed['sens']))

    print("vanilla_grouped has this many loggers", len(vanilla_grouped['recorders']))
    print("processed has this many loggers:", len(processed['dls']))

    print("vanilla_grouped has this many sites:", len(vanilla_grouped['stations']))
    print("processed has this many sites:", len(processed['sites']))

    for site in vanilla_grouped['stations']:
        if site not in processed['sites']:
            print(site)#, len(vanilla_grouped['stations'][site]))

    # for key in grouped_eq:
    #     print(key)

    n = 0
    # for key in grouped_eq:
    #     if key not in processed['sens']:
    #         # print(key)
    #         n += 1
    #         d = grouped_eq[key]
    #         pchans, psens = input_processor(d['chans'], d['sens'], d['dls'])
    #         if len(psens) == 0 or len(pchans) == 0:
    #             print(f"looks like {key} had {len(psens)} processed sensors "
    #                   f"and {len(pchans)} processed channels! "
    #                   f"It had {len(d['chans'])} channels and {len(d['sens'])} sensors!")
    #             print("first seedchan:", d['chans'][0]['seedchan'])
    #         else:
    #             stype = psens[0]['sentype']
    #             serial = psens[0]['unitsn']
    #             newkey = f"{stype}_{serial}"
    #             if newkey not in processed['sens']:
    #                 print(f"Looks like {key} turned into {newkey} "
    #                       f"and neither was processed!")
    #
    # print(n, len(grouped_eq) - len(processed['sens']))


def whats_missing():
    pcd = openpickle("objs/processed.obj")
    gpd = openpickle("objs/fs_nod_grouped.obj")

    # for key in gpd['stations']:
    #     if key not in pcd['sites']:
    #         print(f"Why didn't {key} get processed?")

    for key in gpd['sensors']:
        if key not in pcd['sens']:
            print(key)
            d = gpd['sensors'][key]
            for k2 in d:
                print(k2)
            break


def test_process(netsta):
    nss = netsta.split("-")
    vg = openpickle("objs/fs_nod_grouped.obj")
    gpd = openpickle("objs/grouped_eq.obj")

    allsens = []
    allchans = []
    for senkey in gpd:
        sens = gpd[senkey]['sens']
        chans = gpd[senkey]['chans']
        dls = gpd[senkey]['dls']
        for sen in sens:
            if nss == sen['netstareccha'].split("-")[:2]:
                allsens.append(sen)
        for chan in chans:
            if nss == chan['netstarec'].split("-")[:2]:
                allchans.append(chan)
    if len(allsens) > 0:
        print(len(allchans), len(allsens))
        # pchans, psens = input_processor(chans, sens, dls)
        # print(len(pchans), len(psens))
    for chan in allchans:
        print(chan)

    print("~~~~~~~~~~~~~~~~~~~~~~")
    for sen in allsens:
        print(sen)

    acs = []
    ass = []
    for chan in vg['channels']:
        if netsta in chan['netstarec']:
            acs.append(chan)
    for senkey in vg['sensors']:
        sens = vg['sensors'][senkey]
        for sen in sens:
            if nss == sen['netstareccha'].split("-")[:2]:
                ass.append(sen)

    # print(len(acs), len(ass))
    # for ch in acs:
    #     print(ch)

    # grouped = openpickle("objs/grouped_eq.obj")
    # if netsta in grouped:
    #     d = grouped[netsta]
    # else:
    #     print(f"looks like {netsta} didn't get into grouped?")

def check_type_converter(eqmod):
    from processing.processor import convert_types
    print(convert_types(eqmod))


def check_multis():
    op = openpickle("objs/msdls.obj")
    pins = []
    # for key in op:
    #     print(key)
    for chan in op['JMPRY_National-MP']['upchans']:
        if chan['chadate_inst'].year < 1999:
            print(chan['chadate_inst'])
        if chan['chadate_inst'] == datetime(1984, 1, 1, 0, 0):
            # print(chan['pinnumber'])
            pins.append(int(chan['pinnumber']))

    pins.sort()
    for p in pins:
        print(p)


def compare_preprocessed():
    fs = openpickle("objs/FS_dic.obj")
    nod = openpickle("objs/NOD_dic.obj")
    processed = openpickle("objs/processed.obj")
    # fs_stas = []
    # for sta in fs['stations']:
    #     nc = sta['netcode']
    #     sc = sta['stacode']
    #     ns = f"{nc}.{sc}"
    #     if ns not in fs_stas:
    #         fs_stas.append(ns)
    #     # print(key)
    # pstas = []
    # for key in processed['sites']:
    #     pstas.append(key)
    #
    # nod_stas = []
    # for sta in nod['stations']:
    #     nc = sta['netcode']
    #     sc = sta['stacode']
    #     ns = f"{nc}.{sc}"
    #     if ns not in nod_stas:
    #         nod_stas.append(ns)
    #
    # print("FS sites missing:")
    # for ns in fs_stas:
    #     if ns not in pstas:
    #         print(ns)
    # print("NOD sites missing:")
    # for ns in nod_stas:
    #     if ns not in pstas:
    #         print(ns)
    # for key in fs:
    #     print(key)
    pins = {}
    for chan in fs['channels']:
        if "JMPRY" in chan['netstarec']:
            if chan['chaid'] == "T2_sensor":
                if chan['pinnumber'] == "547":
                    # if chan['chadate_inst']
                    print(chan)
                # if chan['pinnumber'] not in pins:
                #     pins[chan['pinnumber']] = [chan['chadate_inst']]
                # else:
                #     pins[chan['pinnumber']].append(chan['chadate_inst'])

    # for sen in fs['sensors']:
    #     if sen['senid'] == "0964":
    #         print(sen)

    # for pin in pins:
    #     pins[pin].sort()
    #     print("~~~~~", pin)
    #     for p in pins[pin]:
    #         print(p)
    # ppins = {}
    # for key in processed['dls']:
    #     if "JMPRY" in key:
    #         # print(key)
    #         for chan in processed['dls'][key]['chans']:
    #             # print(chan)
    #             if "EARTHWORM NI" in chan['lmodel']:
    #                 # print(chan)
    #                 if chan['pinnumber'] not in ppins:
    #                     ppins[chan['pinnumber']] = [chan['chadate_inst']]
    #                 else:
    #                     ppins[chan['pinnumber']].append(chan['chadate_inst'])
    #
    # for key in pins:
    #     if key not in ppins:
    #         print(key, key in ppins)
    #     else:
    #         for k2 in pins[key]:
    #             if k2 not in ppins[key]:
    #                 print(key, k2)

    """
    need to distinguish CUSP-TUSTIN-2_JMPRY and EARTHWORM NI_JMPRY
    """


def check_processed(file="xmls/dataloggers/LOGGER_CUSP-TUSTIN-2_JMPRY_v10multi.xml", compname=None):
    p = sisxmlparser.parse(file)
    # print(p.HardwareResponse.Hardware.LoggerBoard[0].Component[0])
    cdates = []
    for comp in p.HardwareResponse.Hardware.LoggerBoard[0].Component:
        ldates = True
        if compname:
            if comp.ComponentName == compname:
                ldates = True
            else:
                ldates = False
        if ldates:
            # print("~~~", comp.ComponentName)
            for cali in comp.Calibration:
                # print(cali.CalibrationDate)
                cdate = cali.CalibrationDate.replace(tzinfo=None)

                # cdate.tzinfo = None
                cdates.append(cdate)
    return cdates


def component_list(logger="CVPRY"):
    pc = openpickle("objs/processed.obj")
    calis = {}
    for key in pc['sites']:
        for k2 in pc['sites'][key]['chans']:
            if logger in k2['netstarec']:
                if k2['pinnumber'] not in calis:
                    calis[k2['pinnumber']] = [k2['chadate_inst']]
                else:
                    calis[k2['pinnumber']].append(k2['chadate_inst'])
               # print(k2['pinnumber'], k2['chadate_inst'], k2['chadate_rem'])
        # break
    return calis

def check_multi(logger="CVPRY"):
    mdls = openpickle("objs/msdls.obj")
    for key in mdls:
        if logger in key:
            # print(key)
            k3 = mdls[key]['chans']
            for cha in k3:
                if "AOH" in cha['netstarec']:
            #     if cha['chadate_inst'].year == 2017:
                    print(cha['netstarec'], cha['chadate_inst'], cha['chadate_rem'], cha['seedchan'], cha['logcomponent'])
            # print(k3['netstarec'])
            # print(mdls[key]['upchans'])
            # for k2 in mdls[key]:
            #     print(k2)


def check_both(compname):
    print(compname)
    l1 = check_processed(compname=compname,
                         file="xmls/dataloggers/LOGGER_EARTHWORM_NI_JMPRY_v10multi.xml")
    l2 = check_processed(compname=compname,
                         file="xmls/dataloggers/LOGGER_CUSP-TUSTIN-2_JMPRY_v10multi.xml")
    return l1 + l2


def comparison():
    op = openpickle("objs/overlaps.obj")
    for key in op['SENSORS']:
        if len(op['SENSORS'][key]) > 0:
            # print(key, "SENSOR OVERLAPS")
            for k2 in op['SENSORS'][key]:
                print(f"SENSOR, {key}, {k2}, {op['SENSORS'][key][k2]}")
    for key in op['LOGGERS']:
        if len(op['LOGGERS'][key]) > 0:
            # print(key, "LOGGER OVERLAPS")
            for k2 in op['LOGGERS'][key]:
                print(f"LOGGER, {key}, {k2}, {op['LOGGERS'][key][k2]}")


def fscheck():
    fs = openpickle("objs/FS_raw_dic.obj")
    for line in fs:
        if line['Datalog serial #'].upper() == "0X1011D54":
            # if line['Datalogger (primary)'] == "National-Mt-Tamalpais":
            # if line['Datalogger (primary)'] == "National-South-Fork":
            if "3000" in line['End date']:
                print(line['Start date'], line['End date'], line['Site code'])


def mslcheck():
    import glob
    files = glob.glob("/home/ben/projects/extstationxmlbuilder/xmls/dataloggers/*")
    for f in files:
        a = sisxmlparser.parse(f)
        print("~~~", f.split("/")[-1].split("_")[-2], "~~~")
        epochstarts = []
        for e in a.HardwareResponse.Hardware.Logger[0].EquipmentEpoch:
            epochstarts.append(e.OnDate)
        # print(a.HardwareResponse.Hardware.Logger[0].EquipmentEpoch[0].OnDate)
        lod = a.HardwareResponse.HardwareInstallationGroup.HardwareInstallation[0].Location.OnDate
        for e in epochstarts:
            print(e)
            if e < lod:
                print(f"AHHH! {e} < {lod}!!")

if __name__ == "__main__":
    # comparison()
    # lens()
    # check_grouped()
    # print("~~~~~~ processed ~~~~~~~~~~~~~")
    # check_processed()
    # print("~~~~~~grouped~~~~~~~~~~~~")
    # check_grouped()
    # whats_missing()
    # print("checking nod")
    # check_nod()
    #
    # print("~~~~~~~~~~~~~~~~~~~")
    # print("checking fs")
    # check_fs()
    # print("checking processed")
    # check_processed()
    # test_process("NP-2819")
    # test_process("NP-2819")
    # check_type_converter("SCOPE")
    # print("checking multis")
    # check_multis()
    # qu("EPISENSOR ES-T", "eest", "SENSOR")
    # qu("L-4C VERTICAL", "l4cv", "SENSOR")
    # qu("L-4C HORIZONTAL", "l4ch", "SENSOR")
    # qu("GEOSIG-AC-63", "geosig", "SENSOR")
    # read_api_results("L-4C VERTICAL", "SENSOR")
    # a = sisxmlparser.parse("xmls/ciwr/full/CI_TER.xml")
    # compare_preprocessed()
    # calis = component_list()
    # for key in calis:
    #     print(key, calis[key])
    #     check_loggers = check_both(f"DATA{str(int(key))}")
    #     # print(check_loggers)
    #     print("~~~~~~~")
    #     # print(calis[key])
    #     for k2 in calis[key]:
    #         if k2 not in check_loggers:
    #             print(f"Looks like I couldn't find {key} {k2}")
            # else:
            #     print("found one!")
        # break
    # check_multi()
    fscheck()
    # mslcheck()
    # check_processed(compname="DATA143")
    # check_processed(file="xmls/dataloggers/LOGGER_EARTHWORM_NI_JMPRY_v10multi.xml",
    #                 compname="DATA205")


# VCM??
# PG1
# PG2
# PCM
# NTP
"""
okay, I want to do the following
1. Save all the existing L4C's from SIS in a dict with key = serial and value = owner
2. Make a list of all of my L4C VERTICAL/HORIZONTALs 
3. Compare the two

also need for episensor ES-T's (1511 for example) and L4C Horizontals
"""