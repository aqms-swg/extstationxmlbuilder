from sqlalchemy import create_engine
from sqlalchemy import inspect
import pickle
import copy

USER = "generic_junk_username"
PASS = "generic_junk_pw"


class Scrape:
    def __init__(self, user, password, dbname="nod_db", host="localhost"):
        self.engine = \
            create_engine(f"mysql+pymysql://{user}:{password}@{host}/{dbname}")

    def scrape_tables(self):
        # finds all of the tables in an engine
        # and all of the columns within those tables
        # useful to check what we have in the database
        inspector = inspect(self.engine)
        table_names = []
        columns = {}
        for table_name in inspector.get_table_names():
            table_names.append(table_name)
            columns[table_name] = []
            for column in inspector.get_columns(table_name):
                columns[table_name].append(column)
        return table_names, columns

    def scraper(self, table="stations"):
        # scrapes a table from NOD and puts it in a list of dicts
        table_dict = {}
        dicts = []
        with self.engine.connect() as con:
            entries = con.execute(f"SELECT * FROM {table}")
            for key in entries.keys():  # get the keys for the column
                table_dict[key] = None
            for item in entries:
                td = copy.deepcopy(table_dict)
                for key in td:
                    td[key] = item.__getattribute__(key)  # fill in the keys with the values
                dicts.append(td)  # add my table-entry-dict to my list
        return dicts

    def pickle_engine(self, outputfile="objs/NOD_dic.obj"):
        # pulls the relevant pieces from the NOD sql and puts them in a dict
        table_names, columns = self.scrape_tables()
        nod_dict = {"columns": columns}
        for table in table_names:
            nod_dict[table] = self.scraper(table=table)

        fh = open(outputfile, "wb")
        pickle.dump(nod_dict, fh)


