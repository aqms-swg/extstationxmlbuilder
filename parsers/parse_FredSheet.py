from functions import openpickle, check_none, overlap_serial, picklewriter
from processing.multi_processor import dataloggerPrimaryMapping
import csv
import pickle
from datetime import datetime


def parsedate_fs(datestring):
    # converts a date like "10/01/1993 16:35" into a datetime object
    if "3000" in datestring:
        return None
    else:
        return datetime.strptime(datestring, '%m/%d/%Y %H:%M')


def fs_to_dict(validnetworks=["NC", "NP"], outputfile="objs/FS_raw_dic.obj",
               source="objs/ncsn20211105.csv"):
    # converts the fredsheet to a list of dictionaries,
    # with the column headers as keys
    # and the column contents as values
    n = 0
    with open(source, "r") as fscsv:
        fs = csv.DictReader(fscsv)
        my_fs_dicts = []
        for line in fs:
            n += 1
            dl = dict(line)
            dl['line_number'] = n
            net = dl['Net code']
            if net in validnetworks:
                my_fs_dicts.append(dl)

    fh = open(outputfile, "wb")
    pickle.dump(my_fs_dicts, fh)



def translate_fs(coded_comment):
    # translates fredsheet comments to English
    logMap = {
        'A': 'New installation',
        'B': 'Replacement of unit',
        'C': 'Re installation of original unit after repair',
        'D': 'Change attenuation',
        'E': 'Change Battery',
        'F': 'Off frequency',
        'G': 'VCO problems',
        'H': 'Calibrator problems',
        'I': 'Deviation change',
        'J': 'Seismometer/Accelerometer problems or replacement',
        'K': 'Telephone problems',
        'L': 'Signal level problems',
        'M': 'Radio Problems MB radio replacement',
        'N': 'Antenna problems NB Antenna replacement',
        'O': 'Landline Problems OB Landline replacement',
        'P': 'Grounding problems',
        'Q': 'Flooding problems',
        'R': 'Routine maintainance,not covered by other codes',
        'S': 'Seismometer leveling problems',
        'T': 'Seismicly dead station',
        'U': 'Amplifier problems',
        'V': 'Vandalism',
        'W': 'Summing amp problems WB Summing amp replacement',
        'X': 'Noise Problems*',
        'Y': 'Stabilizer Problems',
        'Z': 'Tape channel change',
        '\\': 'Temporary removal from tape recorder',
        '/': 'Permanent removal from tape recorder (may imply site removal)',
        '1': 'Power Repair/Replacement',
        '2': 'Data Logger Exchange',
        '3': 'Firmware Changes',
        '4': 'Data Recording and Transmission Parameter Change',
        '5': 'GPS  Recording/Equipment Repair',
        '6': 'Satellite Equipment Repair/Replacement',
        '7': 'Earthworm Digitizing and Recording Repair/Replacement/Upgrade',
        '8': 'Microwave Equipment Replacement/Repair',
        '9': 'Telemetry Access Hardware Replacement',
        '#': 'Site Electronic Noise/Interference',
        '@': 'Site Move',
        '!': 'Site Removal',
        '+': 'hardware change requires new channel/location',
        '{': 'data recording begins',
        '}': 'data recording ends'
    }
    output = ""
    for letter in coded_comment:
        if letter in logMap:
            output += logMap[letter]
            output += ", "
    return output.strip(", ")


def get_logger(loggername, chans=[]):
    # for getting proper logger names from fredsheet
    ll = loggername.lower()
    lmod = None
    if "basalt" in ll:
        if len(chans) >= 4:
            lmod = "BASALT-8X"
        else:
            lmod = "BASALT-4X"
    elif "k2" in ll:
        lmod = "K2"
    elif "reftek" in ll:
        if "130-01" in ll:
            lmod = "130-01"
        elif "130-anss" in ll:
            lmod = "130-ANSS"
        elif "130-smhr" in ll:
            lmod = "130-SMHR"
    elif "tustin-2" in ll:
        lmod = "CUSP-TUSTIN-2"
    elif "tustin" in ll:
        lmod = "CUSP-TUSTIN"
    elif "etna" in ll:
        if "2" in ll:
            lmod = "ETNA-2"
        else:
            lmod = "ETNA"
    elif "gsr" and "ia18" in ll:
        lmod = "GSR-IA18-NETQUAKE"  # or with a + appended? 2 models - ask
    elif "obsidian" in ll:
        if len(chans) >= 4:
            lmod = "OBSIDIAN-8X"
        else:
            lmod = "OBSIDIAN-4X"
    elif "trident" in ll:
        lmod = "TRIDENT"  # or trident 30s?
        # we have trident and trident-multi inputs
    elif "hrd" in ll and "24" in ll:
        lmod = "HRD-24"
    elif "national" in ll:
        lmod = "EARTHWORM NI"
    elif "330s+" in ll:
        lmod = "Q330S+"
    elif "330" in ll:
        lmod = "Q330"
    elif "q980" in ll:
        lmod = "Q980"
    elif "taurus" in ll:
        lmod = "TAURUS"
    elif ll.strip() in ["none", "-"]:
        lmod = None
    elif "ssa2" in ll:
        lmod = "SSA-2"
    elif "dst" in ll:
        lmod = "NCSN-DST"
    elif "centaur" in ll:
        lmod = "CENTAUR"
    elif "lynx" in ll:
        lmod = "LYNX"
    else:
        # logger.info(f"UNKNOWN LOGGER TYPE NOT expected FS list: {ll}")
        lmod = loggername
    if type(lmod) == str:
        return lmod.upper()
    else:
        return None


def amp_model(mod):
    ml = mod.lower()
    if "j3x" in ml:
        return "J3X", "J3X.NC.ptbl"
    elif "sprengnether" in ml:
        return "PTS", None
    elif "develco" in ml:
        return "DVLC", "DVLC.NC.ptbl"
    elif "rogers" in ml:
        return "Jon Rogers", None
    elif mod in [
        "J202-1", "J202-2", "J302-1", "J302-2", "J312-1", "J312-2",
        "J402-1", "J402-2", "J412-2", "J502-1", "J502-2", "J512-1",
        "J512-2", "V512", #"V02H",
    ]:
        return mod, preamp_response_file(mod)
    elif check_none(mod) and "unknown" not in ml:
        print(f"~~~~~~~~~~~~~~~~~~~looks like {mod} isn't in my list!")
    return None, None


def vco_model(mod):
    if mod in vco_links:
        return "J512", vco_links[mod]
    else:
        return VCOs[mod][1], None

vco_links = {
    #"J512-1": "J512 (J512-1.NC.ptbl)",
    #"J512-2": "J512 (J512-2.NC.ptbl)",
}


VCOs = {
    "Unity": ["GENERIC", "UNITY"],
    "V02H": ["UNKNOWN", "V02H"],
    # "Develco": ["DEVELCO", "DEVELCO"],
    # "J512-1": ["USGS", "J512"],
    # "J512-2": ["USGS", "J512"],
    "J512": ["USGS", "J512"],
    "St. Louis": ["UNKNOWN", "STLOUIS"],
    "St._Louis": ["UNKNOWN", "STLOUIS"],
}


def preamp_response_file(mod):
    if not check_none(mod):
        return None
    elif "-" in mod:
        p1 = mod.split("-")[0]
        return f"{p1} ({mod}.NC.ptbl)"
    # elif "512" in mod:
    #     return f"512 (512.NC.ptbl)"


# def datalog_serial(dlsn, mod):
#     dpm = {k.lower(): v for k, v in dataloggerPrimaryMapping.items()}
#     if mod.lower() in dpm:
#         return dpm[mod.lower()]
#     else:
#         return dlsn


def location(dlsn, sta):
    dpm = {k.lower(): v for k, v in dataloggerPrimaryMapping.items()}
    if dlsn.lower() in dpm:
        return dpm[dlsn.lower()]
    else:
        return sta


class Line:
    def __init__(self, line):
        self.skip = False
        self.line = line
        self.net = line['Net code'].strip()
        self.sta = line['Site code'].strip()
        self.inst = parsedate_fs(line['Start date'].strip())
        self.rem = parsedate_fs(line['End date'].strip())
        # self.sens, self.sensu = self.sensorsensitivity()
        self.sens = float(self.line['Sens senstivity'])
        self.sensu = self.line['Sensor units'].strip()
        # self.recid = datalog_serial(line['Datalog serial #'].strip(),
        #                             line['Datalogger (primary)'])
        self.recid = line['Datalog serial #'].strip().strip("_").upper()
        # self.recid = overlap_serial(recid, 'LOGGERS', self.net)
        self.recloc = location(line['Datalogger (primary)'].strip(), self.sta)
        self.seedchan = line['SEED chan'].strip()
        self.lat = line['Latitude'].strip()
        self.lon = line['Longitude'].strip()
        self.depth = line['Depth m'].strip()
        self.elevation = line['Sensor elevation m'].strip()
        self.fir = line['FIR filter'].strip()
        if self.fir == "RT72A-07_50":
            self.fir += "ss"
        self.senid = self.senid()
        self.straw = line['Sensor type'].strip()
        self.amod, self.aeqt, self.arespf = self.amp_prep()
        self.rangeg, self.sirange, self.sorange, self.siunit, self.sounit = \
            self.ranges()

    def amp_prep(self):
        model = self.line['Preamp type'].strip()
        if model in VCOs:
            # mod = VCOs[model][1]
            eqtype = "VCO"
            # respfile = None
            mod, respfile = vco_model(model)
        else:
            mod, respfile = amp_model(model)
            eqtype = "AMPLIFIER"
        return mod, eqtype, respfile

    def senid(self):
        ssraw = self.line['Sensor serial #'].strip()
        while str(ssraw)[0] == "0":
            ssraw = ssraw[1:]
        if "HS1" in ssraw:
            senid = ssraw
            if self.seedchan not in ssraw:
                senid += f"_{self.seedchan}"
        elif "_" in ssraw:
            if len(ssraw.split("_")) in [4, 5]:
                ssrs = ssraw.split("_")
                senid = ssrs[1]
                if senid.upper() == "UNKN":
                    senid = f"{ssrs[0]}_{ssrs[2]}"
            else:
                senid = ssraw
        else:
            senid = ssraw.strip()
            if not check_none(senid):
                sid = f"{self.line['Datalog serial #'].strip()}"
                if check_none(sid):
                    senid = sid + "_sensor"
                else:
                    self.skip = True
                    print("Ignoring line with sensor serial:", ssraw, "and logger serial:",
                          self.line['Datalog serial #'].strip())
            # senid = overlap_serial(senid, 'SENSORS', self.net)
        return senid

    # def sensorsensitivity(self):
    #     sensitivity = float(self.line['Sens senstivity'])
    #     sensunits = self.line['Sensor units'].strip()
    #     if self.line['line_number'] in [6768, 6769]:
    #         print(sensitivity, sensunits)
    #     if "m/s/s" in sensunits:
    #         sensitivity *= 9.80665  # converting to g...
    #         sensunits = sensunits.replace("m/s/s", "g")
    #     if self.line['line_number'] in [6768, 6769]:
    #         print(sensitivity, sensunits)
    #     return sensitivity, sensunits

    def check_logger(self):
        for field in [self.straw, self.sensu]:
            if not check_none(field):
                return False
        return True

    def stachan(self):
        stachan = self.line['station channel'].strip()
        return check_none(stachan)

    def samprate(self):
        sr = str(self.line['Sample rate']).strip()
        if sr in ['', '-']:
            return 0.0
        else:
            try:
                fsr = float(sr)
            except Exception as e:
                print(f"couldn't convert sample rate {sr} to a float")
                return None
            if fsr >= 0:
                return fsr
            else:
                return None

    def causal(self):
        if check_none(self.fir):
            return "CAUSAL"
        else:
            return "ACAUSAL"

    def site(self):
        site = {
            'netcode': self.net,
            'stacode': self.sta,
            'stadate_inst': self.inst,
            'stadate_rem': self.rem,
            'stalat': self.lat,
            'stalon': self.lon,
            'elevation': self.elevation,
            'depth': self.line['Depth m'].strip(),
            'city': None,
            'region': None,
            'state': None,
            'staname': self.line['Station name'].strip(),
            'short_name': None,
            'comment': f"{translate_fs(self.line['Field remarks'])}. "
                       f"{self.line['Comment']}",
            "src": "FS",
            "ln": self.line['line_number']
        }
        return site

    def ranges(self):
        if self.line['max out units'].strip() == 'g':
            rangeg = float(self.line['sensor max out'])
            siunit = "g"
            sounit = "v"
            sirange = rangeg
            fac = 1.0
            if self.line['Sensor units'].strip("v/") == "m/s/s":
            # if self.line['max out units'].strip("v/") == "m/s/s":
                fac = 9.80665
            sorange = float(self.line['Sens senstivity'])*fac*sirange

            sorange = round(sorange, 1)

        elif self.line['max out units'].strip() == 'v':
            rangeg = float(self.line['sensor max out'])/float(self.sens)
            sounit = "v"
            siunit = self.line['Sensor units'].strip("v/")
            sorange = None #rangeg
            sirange = None #sorange/self.sens
        else:
            rangeg = float(self.line['sensor max out'])
            sirange = None
            sorange = None
            siunit = None
            sounit = None
        return rangeg, sirange, sorange, siunit, sounit

    def sensor(self):
        if not check_none(self.line['Sensor type'].strip()):
            self.skip = True
        if self.line['max out units'].strip() not in ['g', 'v', '?']:
            print(self.line['max out units'])


        # if self.line['max out units'].strip() == 'g':
        #     rangeg = float(self.line['sensor max out'])
        #     self.siunit = "g"
        #     self.sounit = "v"
        #     self.sirange = rangeg
        #     fac = 1.0
        #     if self.line['Sensor units'].strip("v/") == "m/s/s":
        #         fac = 9.80665
        #     self.sorange = float(self.line['Sens senstivity'])*fac*self.sirange
        #     # if siunit is m/s/s, we need to convert
        # elif self.line['max out units'].strip() == 'v':
        #     rangeg = float(self.line['sensor max out'])/self.sens
        #     self.sounit = "v"
        #     self.siunit = self.line['Sensor units'].strip("v/")
        #     self.sorange = rangeg
        #     self.sirange = self.sorange/self.sens
        # else:
        #     rangeg = None
        if check_none(self.sounit) and check_none(self.siunit):
            sensu = f"{self.sounit}/{self.siunit}"
        else:
            sensu = self.line['Sensor units'].strip()
        sensor = {
            'netstareccha': f"{self.net}-{self.sta}-"
                            f"{self.line['Datalog serial #'].strip()}",
            'sentype': self.line['Sensor type'].strip(),
            'senid': self.senid,
            'sensn': self.senid,
            'sendate_inst': self.inst,
            'sendate_rem': self.rem,
            'unitsn': self.senid, # need to make sure this is the same for sensors (all channels on equipment)
            'chaid_fk': self.senid,
            'sens': self.sens,
            'sens_units': self.sensu,
            # 'sens_units': sensu,
            'rangeg': self.rangeg,
            # okay, sensor rangeg from FS is MAX OUT
            "src": "FS",
            "ln": self.line['line_number']
        }
        if not check_none(sensor['rangeg']):
            print("WTF!", self.line)
        return sensor

    def logger(self):
        dl = {
            'netsta': f"{self.net}-{self.sta}",
            'recdate_inst': self.inst,
            'recdate_rem': self.rem,
            'rectype': self.line['Datalogger (primary)'].strip(),
            'recid': self.recid,
            'recsn': self.recid,
            'loc': self.recloc,
            "src": "FS",
            "ln": self.line['line_number']
        }
        return dl

    def channel(self):
        channel = {
            'netstarec': f"{self.net}-{self.sta}-{self.recid}",  # netsta,
            'seedchan': self.seedchan,
            'chadate_inst': self.inst,
            'chadate_rem': self.rem,
            'chalat': self.lat,
            'chalon': self.lon,
            'chadep': self.depth,
            'elevation': self.elevation,
            'dlchan': self.line['Sens chan #'].strip(),  # need to get channel number for each epoch
            'stachan': self.stachan(),
            'chaid': self.senid,
            'recid_fk': self.recid,
            'lcode': check_none(self.line['Location code']),
            'sensitivity': self.line['Digit sens (counts/v)'],
            'samprate': self.samprate(),  # skip if this is -1 or 0
            # 'sensunits': "cpv", # what? Do I need this?
            'siunit': self.siunit,
            'sounit': self.sounit,
            'sirange': self.sirange,
            'sorange': self.sorange,
            'senid': self.senid,
            'recid': self.recid,
            'azim': self.line['Azim'], # if this is a float, maybe I can't strip?
            'dip': self.line['Dip'],
            'cosmoscode': self.line['cosmos code'].strip(),
            'firfilter': self.fir,
            'causal': self.causal(),
            'pinnumber': self.line['Pin # (primary)'].strip(),
            'ampsn': self.line['Preamp serial #'].strip(),
            'ampmod': self.amod,
            'amptype': self.aeqt,
            'amprespf': self.arespf,
            'ampgain': self.line['Preamp gain'].strip(),
            "src": "FS",
            "ln": self.line['line_number'],
            'rangeg': float(self.line['sensor max out']),
        }
        return channel

    def amp(self):
        if self.arespf != "unchanged":
            respf = self.arespf
        else:
            respf = self.line['Preamp response']
        amp = {
            'netsta': f"{self.net}-{self.sta}",
            'model': self.amod,
            'eqtype': self.aeqt,
            'serial': self.line['Preamp serial #'].strip(),
            'pag': self.line['Preamp gain'].strip(),
            'ondate': self.inst,
            'offdate': self.rem,
            'net': self.net,
            'seedchan': self.seedchan,
            'respf': respf,
            "src": "FS",
            "ln": self.line['line_number']
        }
        return amp


def fs_nod_format(fsrawd="objs/FS_raw_dic.obj", output="objs/FS_dic.obj", nets=["NP", "NC"]):
    # outputs data from the fredsheet in the same format as NOD
    fsd = {
        "stations": [],
        "sensors": [],
        "recorders": [],
        "channels": [],
        "amplifiers": []
    }
    fs = openpickle(fsrawd)
    for line in fs:
        my_line = Line(line)
        if my_line.net in nets:
            if not my_line.skip:
                fsd["stations"].append(my_line.site())
                fsd["sensors"].append(my_line.sensor())
                fsd["recorders"].append(my_line.logger())
                fsd["channels"].append(my_line.channel())
                fsd["amplifiers"].append(my_line.amp())
        else:
            print("I'm skipping this one!")

    # fh = open(output, "wb")
    # picklewriter("objs/grouped_eq.obj", grouped_eq)
    picklewriter(output, fsd)
    # pickle.dump(fsd, fh)
