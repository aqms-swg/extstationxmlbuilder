from functions import openpickle, parsedate, picklewriter, check_none, \
    overlap_serial
from parsers.create_nod_dic import Scrape
from datetime import datetime
import copy
import csv
from logger_setup import my_logger as myl
my_logger = myl()


class Tweaks:
    def __init__(self, basedict="objs/NOD_raw_dic.obj"):
        self.nod = openpickle(basedict)
        self.sensordatechanges = []
        self.loggerdatechanges = []
        self.channeldatechanges = []
        with open("parsers/changes.csv", "r") as dccsv:
            datechanges = csv.DictReader(dccsv)
            for dc in datechanges:
                dc['stacode'] = str(dc['stacode'])
                dc['sens'] = str(dc['sensn'])
                for field in ['new_sendate_inst', 'old_sendate_inst',
                              'new_sendate_rem', 'old_sendate_rem']:
                    if check_none(dc[field]):
                        if "no change" not in dc[field]:
                            print(dc[field])
                            dc[field] = datetime.strptime(dc[field], "%m/%d/%y")
                            if dc[field].year > 2022:
                                dc[field] = dc[field].replace(dc[field].year-100)
                    else:
                        dc[field] = None
                if dc['Type'] == "sensor":
                    self.sensordatechanges.append(dc)
                elif dc['Type'] == "recorder":
                    self.loggerdatechanges.append(dc)
        with open("parsers/channel_changes.csv", "r") as cccsv:
            cdchanges = csv.DictReader(cccsv)
            for change in cdchanges:
                for field in ['old_chadate_inst', 'new_chadate_inst',
                              'old_chadate_rem', 'new_chadate_rem']:
                    if "no change" not in change[field] and check_none(change[field]):
                        change[field] = datetime.strptime(change[field], "%m/%d/%y")
                        if change[field].year > 2022:
                            change[field] = change[field].replace(change[field].year - 100)
                self.channeldatechanges.append(change)

    def trimmer(self):
        # pulls out the pieces of NOD that we care about
        print("channels was this long:", len(self.nod['channels']))
        slimdict = {
            "stations": self.nod['stations'],
            "sensors": self.nod['sensors'],
            "recorders": self.nod['recorders'],
            # "channels": self.nod['channels'],
            "channels": self.remove_dupes(self.nod['channels']),
            "amplifiers": []
        }
        print("now, it's this long:", len(slimdict['channels']))
        return slimdict

    @staticmethod
    def remove_dupes(listofdicts):
        seen = set()
        new_l = []
        for d in listofdicts:
            t = tuple(sorted(d.items()))
            if t not in seen:
                seen.add(t)
                new_l.append(d)
            else:
                print("removed", d, "because it's a duplicate?")
        return new_l

    def change_applyer(self, xtype, xd):
        xd2 = copy.deepcopy(xd)
        if xtype == "sensor":
            changes = self.sensordatechanges
            nsrc = xd['netstareccha'].split("-")
            net = nsrc[0]
            sta = nsrc[1]
            sn = str(xd['sensn'])
            mod = xd['sentype']
            ikey = 'sendate_inst'
            rkey = 'sendate_rem'
        elif xtype == "recorder":
            changes = self.loggerdatechanges
            ns = xd['netsta'].split("-")
            net = ns[0]
            sta = ns[1]
            sn = str(xd['recsn'])
            mod = xd['rectype']
            ikey = 'recdate_inst'
            rkey = 'recdate_rem'
        else:
        # elif xtype == "channel":
            changes = self.channeldatechanges
            nsr = xd['netstarec'].split("-")
            net = nsr[0]
            sta = nsr[1]
            sn = nsr[2]
            mod = None
            ikey = 'chadate_inst'
            rkey = 'chadate_rem'

        # TODO: works for sensors and recorders. What's missing for channels?
        for change in changes:

            match = False
            if change['Type'] != 'channel':
                if net == change['netcode'] and sta == change['stacode'] \
                        and sn == change['sensn']: # \
                    if mod and mod == change['sentype']:
                        match = True
                    elif xd['dlchan'] in [1, 2, 3]:
                        match = True
                    if match:
                        c = False
                        if type(change['new_sendate_inst']) == datetime:
                            xd2[ikey] = change['new_sendate_inst']
                            c = True
                        if type(change['new_sendate_rem']) == datetime:
                            xd2[rkey] = change['new_sendate_rem']
                            c = True
                        if c:
                            print(f"made a change to {xtype} {net} {sta} {sn} {mod}")
            else:
                if net == change['netcode'] and sta == change['stacode'] \
                        and sn == change['recsn']:
                    if str(int(xd['dlchan'])) == str(int(change['dlchan'])) and \
                        xd['chadate_inst'] == change['old_chadate_inst'] and \
                            xd['seedchan'] == change['old_seed']:
                        for field in ['chadate_inst', 'chadate_rem', 'seed', 'lcode']:
                            if field == 'seed':
                                change['new_seedchan'] = change[f'new_{field}']
                                field = 'seedchan'
                            # if type(change[f"new_{field}"]) == datetime:

                            if change[f"new_{field}"] != 'no change':
                                print(f"changed field from {xd[field]} "
                                      f"to {change[f'new_{field}']}")
                                xd[field] = change[f"new_{field}"]
        return xd2

    def futuredate(self, d):
        if "chadate_inst" in d:
            i = 'chadate_inst'
            r = 'chadate_rem'
        elif 'sendate_inst' in d:
            i = 'sendate_inst'
            r = 'sendate_rem'
        elif 'recdate_inst' in d:
            i = 'recdate_inst'
            r = 'recdate_rem'
        elif 'stadate_inst' in d:
            i = 'stadate_inst'
            r = 'stadate_rem'
        if d[i]:
            if d[i].year > 2021:
                d[i] = d[i].replace(d[i].year - 100)
        # else:
        #     my_logger.warning(f"WILL SKIP DUE TO NO INST {d}")
        if d[r]:
            if d[r].year > 2021:
                d[r] = d[r].replace(d[r].year - 100)
        return d

    def datefixer(self, nod, outfile):
        datefixed = {
            "stations": [],
            "sensors": [],
            "recorders": [],
            "channels": [],
            "amplifiers": []
        }
        for sta in nod['stations']:
            fixedsta = copy.deepcopy(sta)
            fixedsta['stadate_inst'] = parsedate(sta['stadate_inst'])
            fixedsta['stadate_rem'] = parsedate(sta['stadate_rem'])
            fixedsta = self.futuredate(fixedsta)
            fixedsta['src'] = "NOD"
            if sta['netcode'] in ['NC', 'NP']:
                datefixed['stations'].append(fixedsta)

        for sen in nod['sensors']:
            fixedsen = self.change_applyer("sensor", sen)
            fixedsen['sendate_inst'] = parsedate(sen['sendate_inst'])
            fixedsen['sendate_rem'] = parsedate(sen['sendate_rem'])
            fixedsen = self.futuredate(fixedsen)
            fixedsen['src'] = "NOD"

            # fixedsen['unitsn'] = overlap_serial(sen['unitsn'], 'SENSORS',
            #                                     sen['netstareccha'].split("-")[0])
            # fixedsen['senid'] = fixedsen['unitsn']
            if sen['netstareccha'].split("-")[0] in ['NC', 'NP']:
                datefixed['sensors'].append(fixedsen)

        for dl in nod['recorders']:
            # fixeddl = copy.deepcopy(dl)
            fixeddl = self.change_applyer("recorder", dl)
            fixeddl['recdate_inst'] = parsedate(dl['recdate_inst'])
            fixeddl['recdate_rem'] = parsedate(dl['recdate_rem'])
            fixeddl = self.futuredate(fixeddl)
            if dl['netsta'] == "NP.7038":
                if dl['recdate_inst'] == "2017-05-27":
                    fixeddl['recdate_inst'] = "2007-05-27"

            if dl['recsn'] == "29-D":
                fixeddl['recsn'] = "29D"

            # fixeddl['recsn'] = overlap_serial(dl['recsn'], "LOGGERS",
            #                                   dl['netsta'].split("-")[0])
            if str(dl['recsn']) == "1462" and dl['rectype'] == "K2":
                fixeddl['recsn'] = str(dl['recsn']) + "_NC"

            fixeddl['src'] = "NOD"
            if dl['netsta'].split("-")[0] in ['NC', 'NP'] \
                    and check_none(fixeddl['recdate_inst']):
                datefixed['recorders'].append(fixeddl)

        hys = {}
        for chan in nod['channels']:

            fixedchan = copy.deepcopy(chan)
            if not check_none(chan['seedchan']):
                import time
                if chan['netstarec'] not in hys or hys[chan['netstarec']] == 3:
                    hys[chan['netstarec']] = 0
                hys[chan['netstarec']] += 1

                my_logger.info(f"no seedchan for {chan}, I'm setting it to HY{hys[chan['netstarec']]}", )
            fixedchan['chadate_inst'] = parsedate(chan['chadate_inst'])
            fixedchan['chadate_rem'] = parsedate(chan['chadate_rem'])
            fixedchan1 = self.futuredate(fixedchan)
            fixedchan = self.change_applyer("channel", fixedchan1)

            # fixedchan['chaid'] = overlap_serial(chan['chaid'], 'SENSORS', net)
            # need for chaid too?

            if fixedchan['cosmoscode'] == 51:
                fixedchan['cosmoscode'] = 50

            if fixedchan['cosmoscode'] == 19:
                fixedchan['cosmoscode'] = 10

            if fixedchan['netstarec'] == "NP-1004-29-D":
                fixedchan['netstarec'] = "NP-1004-29D"

            fixedchan['src'] = "NOD"
            if chan['cosmoscode'] == 19:
                fixedchan['cosmoscode'] = 10
                my_logger.info("Changed cosmoscode")
            if chan['cosmoscode'] == 51:
                fixedchan['cosmoscode'] = 50
            if chan['cosmoscode'] == 0:
                fixedchan['cosmoscode'] = 11
            if chan['netstarec'].split("-")[0] in ['NC', 'NP']:
                datefixed['channels'].append(fixedchan)

        picklewriter(outfile, datefixed)
        # fh = open(outfile, "wb")
        # pickle.dump(datefixed, fh)


def build_nod_dic(user, password, output="objs/NOD_raw_dic.obj",
                  dbname="nod_db", host="localhost"):
                  # dbname="nod", host="localhost"):
    print("SCRAPING!!!")
    # parsers NOD sql database, writes a dictionary and saves it as a pickle file
    scraped_db = Scrape(user=user, password=password, dbname=dbname, host=host)
    scraped_db.pickle_engine(outputfile=output)


def fix_nod_dates(infile="objs/NOD_raw_dic.obj", outfile="objs/NOD_dic.obj"):
    tweaker = Tweaks(infile)
    trimmed = tweaker.trimmer()
    tweaker.datefixer(trimmed, outfile=outfile)

# TODO: sample rate changes for NOD for some loggers - email to come from Jason
#
