import copy

import sisxmlparser3_0 as sisxmlparser
from functions import parse_operator, root, first_last_date
from logger_setup import my_logger as myl
my_logger = myl()
# starting by hoping that I don't have to scrape everything from the amplifier data after downloading it

# TODO: combine components (take component out of serial)


class Amplifier:
    # def __init__(self, model, serial, ondate, offdate, operator, components, pag):
    def __init__(self, amps, version=10, folder=None):
        self.amps = amps
        self.model = amps[0]['model']
        self.serial = amps[0]['serial']
        self.net = amps[0]['net']
        self.operator = parse_operator(self.net)
        self.get_start_stop()
        self.my_components = self.components(self.calibrations())
        if amps[0]['eqtype'] == "VCO":
            hwr = self.vcohardware()
        else:
            hwr = self.amphardware()
        self.template = root(self.net, hwr=hwr)
        self.outputname = self.namer(folder, version=version)
        # self.ondate = ondate
        # self.offdate = offdate
        # if offdate:
        #     self.status="RETIRED"
        # else:
        #     self.status="FUNCTIONAL"
        # self.pag = pag
        # self.namespace = "NCSS"
        # self.type = "PolesZeros"
        # self.operator = operator
        # self.my_components = self.components(components)

    def epochs(self):
        epochs = []
        inst, rem, status = first_last_date(self.amps)
        epoch = sisxmlparser.EquipmentEpochType(
            OnDate=inst,
            OffDate=rem,
            InventoryStatus=status,
            Operator=parse_operator(self.net),
            Owner=parse_operator(self.net)
        )
        offdates = []
        if epoch not in epochs:
            offdates.append(epoch.OffDate)
            epochs.append(epoch)
        if None not in offdates:
            epochs.append(
                sisxmlparser.EquipmentEpochType(
                    OnDate=max(offdates),
                    OffDate=None,
                    InventoryStatus="RETIRED",
                    Operator=parse_operator(self.net),
                    Owner=parse_operator(self.net)
                )
            )
        return epochs

    def get_start_stop(self):
        # needs to get first start and last end
        my_startdates = []
        my_enddates = []
        for amp in self.amps:
            my_startdates.append(amp['ondate'])
            my_enddates.append(amp['offdate'])
        self.ondate = min(my_startdates)
        if None not in my_enddates:
            self.offdate = max(my_enddates)
            self.status = "FUNCTIONAL"
        else:
            self.offdate = None
            self.status = "RETIRED"

    def calibrations(self):
        calis = {}
        calidates = []
        for amp in self.amps:
            linkname = self.amps[0]['respf']
            if linkname and "(" not in linkname:
                lo = copy.deepcopy(linkname)
                linkname = f"{lo.split('.')[0]} ({lo})"
                print(f"changed linkname from {lo} to {linkname}")
            if amp['net'] == "NC":
                namespace = "NCSS"
            elif amp['net'] == "NP":
                namespace = "NSMP"
            elif amp['net'] == "CI":
                namespace = "SCSN-CA"
            elif amp['net'] == "WR":
                namespace = "CDWR"
            else:
                namespace = None
            if linkname:
                rdl = sisxmlparser.ResponseDictLinkType(
                    Name=linkname,
                    SISNamespace=namespace,
                    Type="PolesZeros"
                )
            else:
                rdl = None
            calibration = sisxmlparser.CalibrationType(
                CalibrationDate=amp['ondate'],
                Response=sisxmlparser.CalResponseType(
                    ResponseDictLink=rdl,
                    Gain=sisxmlparser.SISGainType(
                        Value=amp['pag'],
                        Frequency=5.0,  # always 5.0 for FS
                        InputUnits=sisxmlparser.UnitsType(
                            Name="V",
                            Description="Voltage in Volts"
                        ),
                        OutputUnits=sisxmlparser.UnitsType(
                            Name="V",
                            Description="Voltage in Volts"
                        )
                    ),

                )
            )
            comp = amp['seedchan']
            if comp not in calis:
                calis[comp] = [calibration]
                calidates.append(amp['ondate'])
            elif amp['ondate'] not in calidates:
                calis[comp].append(calibration)
                calidates.append(amp['ondate'])
        return calis

    def components(self, calis):
        my_comps = []
        added_chans = []
        n = 1
        for amp in self.amps:
            if amp['seedchan'] not in added_chans:
                comp = f"COMPONENT{n}" # this needs to link components if calibration dates change?
                n += 1
                my_comp = sisxmlparser.ComponentType(
                    ComponentName=comp,
                    Calibration=calis[amp['seedchan']]
                )
                my_comps.append(my_comp)
                added_chans.append(amp['seedchan'])
        return my_comps

    def amphardware(self):
        hardware = sisxmlparser.HardwareResponseType(
            Hardware=sisxmlparser.HardwareType(
                Amplifier=[sisxmlparser.EquipType(
                    SerialNumber=self.serial,
                    ModelName=self.model,
                    Category="AMPLIFIER",
                    IsActualSerialNumber=False,
                    EquipmentEpoch=[sisxmlparser.EquipmentEpochType(
                        OnDate=self.ondate,
                        OffDate=self.offdate,
                        InventoryStatus=self.status,
                        Operator=self.operator,
                    )],
                    # TODO: should have distinct epochs. Make them in get_start_stop
                    Component=self.my_components
                )]
            )
        )
        self.eqt = "AMPLIFIER"
        return hardware

    def vcohardware(self):
        hardware = sisxmlparser.HardwareResponseType(
            Hardware=sisxmlparser.HardwareType(
                VCO=[sisxmlparser.EquipType(
                    SerialNumber=self.serial,
                    ModelName=self.model,
                    Category="VCO",
                    IsActualSerialNumber=False,
                    EquipmentEpoch=[sisxmlparser.EquipmentEpochType(
                        OnDate=self.ondate,
                        OffDate=self.offdate,
                        InventoryStatus=self.status,
                        Operator=self.operator,
                    )],
                    # TODO: should have distinct epochs. Make them in get_start_stop
                    Component=self.my_components
                )]
            )
        )
        self.eqt = "VCO"
        return hardware

    def namer(self, folder, version=0):
        if not folder:
            my_folder = "xmls/amplifiers"
        else:
            my_folder = folder
        fname = f"{self.eqt}_{self.model}_{self.serial}_v{version}.xml"
        fname = fname.replace(" ", "_")
        return f"{my_folder}/{fname}"