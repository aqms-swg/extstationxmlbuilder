import sisxmlparser3_0 as sisxmlparser
from functions import root, parse_operator, convert2serialize, first_last_date
from logger_setup import my_logger as myl
from processing.NRL_urls import get_netquakes_return
import datetime
logger = myl()


startdates = {
    "JMPRY": datetime.datetime(1970, 1, 1, 0, 0, 0),
    "PBMRY": datetime.datetime(1987, 10, 7, 0, 0, 0),
    "OBHRY": datetime.datetime(2006, 12, 7, 0, 0, 0),
    "MBURY": datetime.datetime(2019, 4, 22, 0, 0, 0),
    "KCPRY": datetime.datetime(1979, 10, 31, 0, 0, 0),
    "PCHRY": datetime.datetime(1970, 1, 1, 0, 0, 0),
    "HSPRY": datetime.datetime(1973, 1, 1, 0, 0, 0),
    "GGPRY": datetime.datetime(1975, 4, 1, 0, 0, 0),
    "HBLRY": datetime.datetime(2006, 8, 23, 0, 0, 0),
    "CLLRY": datetime.datetime(2004, 10, 18, 0, 0, 0),
    "MWDRY": datetime.datetime(1970, 1, 1, 0, 0, 0),
    "NTARY": datetime.datetime(1970, 1, 1, 0, 0, 0),
    "KMPRY": datetime.datetime(1979, 6, 27, 0, 0, 0),
    "NSMRY": datetime.datetime(1970, 1, 1, 0, 0, 0),
    "LSFRY": datetime.datetime(1983, 4, 12, 0, 0, 0),
    "CVPRY": datetime.datetime(1991, 4, 1, 0, 0, 0),
    "PWHRY": datetime.datetime(1970, 8, 1, 0, 0, 0),

}

enddates = {
    "JMPRY": None,
    "PBMRY": None,
    "OBHRY": None,
    "MBURY": None,
    "KCPRY": None,
    "PCHRY": None,
    "HSPRY": None,
    "GGPRY": None,
    "HBLRY": datetime.datetime(2020, 8, 12, 0, 0, 0),
    "CLLRY": datetime.datetime(2010, 11, 15, 0, 0, 0),
    "MWDRY": None,
    "NTARY": None,
    "KMPRY": None,
    "NSMRY": None,
    "LSFRY": None,
    "CVPRY": None,
    "PWHRY": None,
}


def copy_response(eqtype, nchans=1):
    # produces response information for listed sensors that don't have NRL entries
    # but do have analgous entries in SIS
    if eqtype in ["Q330"]:
        p = sisxmlparser.parse("templates/Q330.xml")
    elif eqtype in ["K2", "WHITNEY", "WHITNEY-MULTI", "MT. WHITNEY"]:
        p = sisxmlparser.parse("templates/K2.xml")
    elif eqtype in ["GRANITE", "GRANITE-MULTI", "BASALT",
                    "BASALT-MULTI", "BASALT-4X", "BASALT-8X"]:
        if nchans > 4:
            p = sisxmlparser.parse("templates/Basalt-8x.xml")
        else:
            p = sisxmlparser.parse("templates/Basalt-4x.xml")
    elif "HS-1" in eqtype:
        p = sisxmlparser.parse("templates/hs1.xml")
    else:
        p = None
    if p:
        return p.HardwareResponse.Hardware.LoggerBoard[0].\
            Component[0].Calibration[0].Response
    else:
        return None

# for L4s, any # in last digit means horizontal
# NC should always be 1-component - horizontal or vertical, not 3d


class Logger:
    def __init__(self, serial, ml, channels, version=1, folder=None,
                 epochstart=None, epochend=None, hwis=[]):
        self.hwis = hwis
        self.netquake = False
        # print("starting off, serial = ", serial)
        self.serial = str(serial)
        self.ml = ml
        self.version = version
        self.net = ml[0]['netsta'].split("-")[0]
        self.my_chans = channels
        self.my_epochs, self.my_hwg = self.epochs()
        # self.my_epochs = self.epochs()
        self.my_responses = [copy_response(ml[0]['rectype'], nchans=len(channels))]
        self.calis = {}
        self.mycalis = {}
        self.mycalidates = {}
        self.my_components = self.components3()
        self.my_boards = self.boards()
        self.my_package = self.package()
        self.my_hardware = self.hardware()
        self.template = root(self.net, hwr=self.my_hardware)
        self.outputname = self.namer(folder)

    def epochs(self):
        # creates the epochtype object for the logger
        # returns in a list because there can be more than one,
        # but loggers come in groups of 1? No? Maybe not?
        epochs = []
        mininst, maxrem, status = first_last_date(self.my_chans)
        epochs = [sisxmlparser.EquipmentEpochType(
            OnDate=mininst,
            OffDate=maxrem,
            InventoryStatus=status,
            Operator=parse_operator(self.net),
            Owner=parse_operator(self.net)
        )]
        lepochs = {}
        offdates = []
        hwis = []
        for ml in self.ml:
            net = ml['netsta'].split("-")[0]
            offdates.append(ml['recdate_rem'])
            if 'loc' not in ml:
                ml['loc'] = ml['netsta'].split("-")[1]
            # if 'loc' in ml:
            if ml['loc'] not in lepochs:
                lepochs[ml['loc']] = {'inst': ml['recdate_inst'],
                                      'rem': ml['recdate_rem'], 'net': net}

            if ml['recdate_inst'] < lepochs[ml['loc']]['inst']:
                lepochs[ml['loc']]['inst'] = ml['recdate_inst']
            if not ml['recdate_rem'] or not lepochs[ml['loc']]['rem']:
                lepochs[ml['loc']]['rem'] = None
            elif ml['recdate_rem'] > lepochs[ml['loc']]['rem']:
                lepochs[ml['loc']]['rem'] = ml['recdate_rem']

        added = []
        for le in lepochs:
            # oo = parse_operator(lepochs[le]['net'])
            # # if not lepochs[le]['rem']:
            # status = "FUNCTIONAL"
            # # else:
            # #     status = "RETIRED"
            # if self.hwis:
            #     description = le
            # else:
            #     description = None
            # epoch = sisxmlparser.EquipmentEpochType(
            #     OnDate=lepochs[le]['inst'],
            #     OffDate=lepochs[le]['rem'],
            #     InventoryStatus=status,
            #     Operator=oo,
            #     Owner=oo,
            #     # Description=description
            # )
            # if lepochs[le] not in added:
            #     added.append(lepochs[le])
            #     epochs.append(epoch)
            if self.hwis and le in startdates:
                hwi = sisxmlparser.HardwareInstallationType(
                    Equipment=sisxmlparser.EquipIDType(
                        SerialNumber=self.serial,
                        ModelName=self.ml[0]['rectype'],
                        Category="LOGGER",
                    ),
                    Location=sisxmlparser.SiteIDType(
                        SiteNetCode="NC",
                        SiteLookupCode=le,
                        OnDate=startdates[le],
                        OffDate=enddates[le]
                    ),
                    IsMultiSite=True,
                    InstallDate=lepochs[le]['inst'],
                    RemovalDate=lepochs[le]['rem']
                )
                hwis.append(hwi)

        if hwis:
            hwg = sisxmlparser.HardwareInstallationGroupType(
                HardwareInstallation=hwis
            )
        else:
            hwg = None
        if None not in offdates:
            epochs.append(
                sisxmlparser.EquipmentEpochType(
                    OnDate=max(offdates),
                    OffDate=None,
                    InventoryStatus="RETIRED",
                    Operator=parse_operator(self.net),
                    Owner=parse_operator(self.net)
                )
            )
        return epochs, hwg

    def components3(self):
        comps = {}
        addedcalis = {}
        for chan in self.my_chans:
            if chan['logcomponent'] not in comps:
                addedcalis[chan['logcomponent']] = []
                comps[chan['logcomponent']] = sisxmlparser.ComponentType(
                    ComponentName=chan['logcomponent'],
                    Calibration=[]
                )
            # if 'recrate' in self.ml:
            #     freq = self.ml['recrate']
            # else:
            #     freq = chan['samprate']
            # if not freq: # setting a default so I can build without frequency defined
            #     freq = 1.0
            if not chan['sensitivity']:
                chan['sensitivity'] = 1.0
            if not chan['liunit']:
                chan['liunit'] = "v"
                chan['lidesc'] = "Voltage in Volts"
            if not chan['lounit']:
                chan['lounit'] = "counts"
                chan['lodesc'] = "Digital counts"
            response = sisxmlparser.CalResponseType(
                Gain=sisxmlparser.SISGainType(
                    # Value=chan['sensitivity'],
                    Value=chan['sensitivity'],
                    InputUnits=sisxmlparser.UnitsType(
                        Name=chan['liunit'],
                        Description=chan['lidesc'],
                    ),
                    OutputUnits=sisxmlparser.UnitsType(
                        Name=chan['lounit'],
                        Description=chan['lodesc'],
                    ),
                    Frequency=1.0,
                )
            )
            irange = chan['lsval']
            idesc = sisxmlparser.UnitsType(
                Name=chan['liunit'],
                Description=chan['lidesc']
            )
            odesc = sisxmlparser.UnitsType(
                Name=chan['lounit'],
                Description=chan['lodesc']
            )
            cali = self.get_calib(chan['chadate_inst'], response,
                                  irange, idesc, None, odesc)

            self.get_calis(chan['logcomponent'], cali)

        for comp in self.calis:
            comps[comp].Calibration = self.calis[comp]

        complist = []
        for name in comps:
            complist.append(comps[name])
        return complist

    @staticmethod
    def get_calib(date, response, irange, irdes, orange, ordes, comment=None):
        return sisxmlparser.CalibrationType(
            CalibrationDate=date,
            Response=response,
            # InputRange=irange,
            InputRange=None,
            InputRangeUnit=irdes,
            # OutputRange=orange,
            OutputRange=None,
            OutputRangeUnit=ordes,
            Comments=comment
        )

    def boards(self):
        board = sisxmlparser.EquipType(
            SerialNumber=f"{self.serial}-B1",
            IsActualSerialNumber=False,
            ModelName=self.ml[0]['rectype'],
            Category="LOGGERBOARD",
            EquipmentEpoch=self.my_epochs,
            Component=self.my_components,
        )
        return [board]

    def package(self):
        details = []
        usedpins = []
        for component in self.my_components:
            pin = component.ComponentName.strip("DATA")
            for chan in self.my_chans:
                if str(chan['logcomponent']) == component.ComponentName.strip("DATA"):
                    if 'logpin' in chan:
                        pin = chan['logpin']
            detail = sisxmlparser.ComponentDetailType(
                ComponentName=component.ComponentName,
                PinNumber=pin,
            )
            usedpins.append(pin)
            details.append(detail)

        package = [sisxmlparser.LoggerPackageType(
            Logger=sisxmlparser.EquipIDType(
                SerialNumber=self.serial,
                ModelName=self.ml[0]['rectype'],
                Category="LOGGER",
            ),
            LoggerContent=[sisxmlparser.LoggerPackageContentType(
                LoggerBoard=sisxmlparser.EquipIDType(
                    SerialNumber=f"{self.serial}-B1",
                    ModelName=self.ml[0]['rectype'],
                    Category="LOGGERBOARD",
                    # might need to loop over this with multiple slotnumbers
                ),
                SlotNumber=1,  # always 1, or will we ever have multiple boards?
                ComponentDetail=details
            )]
        )]
        return package

    def hardware(self):
        hwr = sisxmlparser.HardwareResponseType(
            Hardware=sisxmlparser.HardwareType(
                Logger=[sisxmlparser.EquipBaseType(
                    SerialNumber=self.serial,
                    ModelName=self.ml[0]['rectype'],
                    Category="LOGGER",
                    IsActualSerialNumber=True,
                    EquipmentEpoch=self.my_epochs,
                )],
                LoggerBoard=self.my_boards,
                LoggerPackage=self.my_package
            ),
            HardwareInstallationGroup=self.my_hwg
        )
        if self.netquake:
            hwr.ResponseDictGroup = get_netquakes_return()
        return hwr

    def namer(self, folder):
        if not folder:
            my_folder = "loggers"
        else:
            my_folder = folder
        fname = f"LOGGER_{self.ml[0]['rectype']}_" \
                f"{self.serial}_" \
                f"v{self.version}.xml"
        fname = fname.replace(" ", "_")
        return f"{my_folder}/{fname}"

    def get_calis(self, component, cali):
        if component not in self.calis:
            self.calis[component] = []
            self.mycalis[component] = []
            self.mycalidates[component] = []

        cdict = convert2serialize(cali)
        d = cdict.pop('CalibrationDate')
        if cdict not in self.mycalis[component]:
            self.calis[component].append(cali)
            self.mycalis[component].append(cdict)
            self.mycalidates[component].append(d)
        else:
            idx = self.mycalis[component].index(cdict)
            if d < self.mycalidates[component][idx]:
                self.calis[component][idx] = cali
                self.mycalis[component][idx] = cdict
                self.mycalidates[component][idx] = d

