import sisxmlparser3_0 as sisxmlparser
from functions import root, component_label, parse_operator, \
    check_none, first_last_date, get_nrl_gain, \
    senchans, l4c_rdg, convert2serialize, g_convert

from logger_setup import my_logger
logger = my_logger()

remaps = {"ARS-14": [10.0, "ARS-14 (ARS-14.NC.stbl)"],
          # "Geospace-HS1": [30.0, "OYO-HS1 (HS1.NC.stbl)"],
          "HS-1-LT VERTICAL": [30.0, "OYO-HS1 (HS1.NC.stbl)"],
          "HS-1-LT HORIZONTAL": [30.0, "OYO-HS1 (HS1.NC.stbl)"],
          "L-4C VERTICAL": [10.0, "L4-C Default (L4.NC.stbl)"],
          "L-4C HORIZONTAL": [10.0, "L4-C Default (L4.NC.stbl)"],
          # "Wilcoxon-731": [10.0, "WIL.NC.stbl"],
          "731A": [10.0, "WILCOXON731A (WIL.NC.stbl)"]
          }

# Q330S+ issues - fir filter in Email
# refer to starred email chain
# replace one site with relevant stuff and have Jacob check before
# ask Prabha about replacing these frequencies -
# test code on virtual machine

def remap_response(sentype):
    if sentype in remaps:
        rdl = sisxmlparser.ResponseDictLinkType(
            Name=remaps[sentype][1],
            SISNamespace="NCSS",
            Type="PolesZeros"
        )
        freq = remaps[sentype][0]
    else:
        freq = 1.0
        rdl = None
    return rdl, freq


class Sensor:
    def __init__(self, serial, ml, channels, version=2, folder=None, hwis=[]):
        self.serial = serial
        self.hwis = hwis
        self.ml = ml
        self.channels = channels
        self.linked = senchans(self.ml, self.channels)
        self.net = self.ml[0]['netstareccha'].split("-")[0]
        self.sentype = self.ml[0]['sentype']
        self.rdg, self.rdl = l4c_rdg(self.sentype)
        self.version = version
        self.inst_dates, self.rem_dates, self.states = self.dates_states()
        self.my_epochs = self.epochs()
        self.calis = {}
        self.mycalis = {}
        self.mycalidates = {}
        self.my_comps = self.components()
        self.my_hardware = self.hardware()
        self.template = root(self.net, hwr=self.my_hardware)
        self.outputname = self.namer(folder)

    def dates_states(self):
        # gets installation dates of sensors
        insts = []
        rems = []
        states = []
        for sen in self.ml:
            if not sen['sendate_inst']:
                for chan in self.channels:

                    if sen['chaid_fk'] == chan['chaid']:
                        sen['sendate_inst'] = chan['chadate_inst']
            mininst, maxrem, status = first_last_date(self.channels)
            if mininst not in insts or maxrem not in rems:
                insts.append(mininst)
                rems.append(maxrem)
                states.append(status)

        return insts, rems, states

    def epochs(self):
        # creates epochs for sensor.
        # If the sensor is not in use, adds a retired epoch from the
        # last offdate, with not ondate
        epochs = []
        inst, rem, status = first_last_date(self.channels)
        epoch = sisxmlparser.EquipmentEpochType(
            OnDate=inst,
            OffDate=rem,
            InventoryStatus=status,
            Operator=parse_operator(self.net),
            Owner=parse_operator(self.net)
        )
        offdates = []
        if epoch not in epochs:
            offdates.append(epoch.OffDate)
            epochs.append(epoch)
        if None not in offdates:
            epochs.append(
                sisxmlparser.EquipmentEpochType(
                    OnDate=max(offdates),
                    OffDate=None,
                    InventoryStatus="RETIRED",
                    Operator=parse_operator(self.net),
                    Owner=parse_operator(self.net)
                )
            )
        return epochs

    @staticmethod
    def get_calib(cdate, response,
                  irange, irdes, orange, ordes, comment=None):
        # creates a calibrationtype object
        cali = sisxmlparser.CalibrationType(
            CalibrationDate=cdate,
            Response=response,
            InputRange=irange,
            InputRangeUnit=irdes,
            OutputRange=orange,
            OutputRangeUnit=ordes,
            Comments=comment,
        )
        return cali

    def components(self):
        comps = {}
        pds = {}
        calidatess = []
        for chan in self.channels:
            comment = None
            if len(self.linked) == 1:
                for sen in self.linked:
                    if len(self.linked[sen]) == 1:
                        chan['seedchan'] = 1
            my_ml = None  # to make sure we don't use a previous one
            for ml in self.ml:
                if str(ml['chaid_fk']) in str(chan['chaid']):
                    if ml['sendate_inst'] == chan['chadate_inst'] and \
                            chan['netstarec'] in ml['netstareccha']:
                        my_ml = ml
            if not my_ml:
                for ml in self.ml:
                    if 'sid' in chan:
                        if str(chan['sid']) == str(ml['senid']):
                            my_ml = ml

            name = component_label(chan['seedchan'])

            if name not in comps:
                comps[name] = sisxmlparser.ComponentType(
                    ComponentName=chan['sencomponent'],
                    Calibration=[]
                )

            if my_ml and check_none(chan['siunit']) \
                    and check_none(chan['sidesc']) \
                    and check_none(chan['sounit']) \
                    and check_none(chan['sodesc']):
                irange = my_ml['rangeg']
                if irange and irange < 0:
                    irange = None
                try:
                    # # TODO: check units here - g & m/s/s
                    # orange = float(irange)*float(my_ml['sens'])
                    # print("now orange = ", orange, "it was", chan['sorange'])
                    # su = my_ml['sens_units'].split("/")
                    # print("su = ")
                    #
                    # # orange = round(orange*2)/2 # rounds output range to nearest 0.5
                    # chan['sorange'] = orange
                    irdes = sisxmlparser.UnitsType(
                                        Name=chan['siunit'],
                                        Description=chan['sidesc']
                                    )
                    ordes = sisxmlparser.UnitsType(
                        Name=chan['sounit'],
                        Description=chan['sodesc'],
                    )
                except Exception as e:
                    # logger.warning(
                    logger.info(
                        f"Looks like I couldn't get irange and orange from "
                        f"rangeg: {my_ml['rangeg']} and sens: {my_ml['sens']}"
                        f"Got this exception: {e}")
                    orange = None
                    ordes = None
                    irdes = None

                if chan['senrespf'] and self.sentype not in remaps:
                    response = sisxmlparser.CalResponseType(
                        RESPFile=sisxmlparser.RESPFileType(
                            ValueOf=chan['senrespf']
                        )
                    )
                    nrlgain = get_nrl_gain(chan['senrespf'])
                    us = my_ml['sens_units'].split("/")
                    mygain = my_ml['sens']
                    # us, ds, mygain = g_convert(us, [""]*len(us), my_ml['sens'])

                    # removed all this...
                    # if mygain != 0:
                    #     facoff = nrlgain / mygain
                    #     if (facoff > 1.05 or facoff < 0.95) \
                    #             and round(facoff, 1) not in [2.0, 0.5]:
                    #         comment = f"The source database had a gain of {my_ml['sens']}, " \
                    #                   f"But the nrl gain of {nrlgain} was used to find total " \
                    #                   f"channel sensitivity"
                    #         logstring = f"gains don't match! their ratio is {facoff}",\
                    #                     f"nrlgain = {nrlgain}",\
                    #                     f"db gain: {my_ml['sens']}",\
                    #                     "gain units & sens type:",\
                    #                     f"{my_ml['sens_units']} {my_ml['sentype']}"
                    #         if round(facoff, 1) == 0.1:
                    #             my_ml['sens'] /= 9.80665
                    #             print("tweaked sens, it was", facoff, "it's now", my_ml['sens'])
                    #         elif round(facoff, 0) == 10.0:
                    #             my_ml['sens'] *= 9.80665
                    #             print("tweaked sens, it was", facoff, "it's now", my_ml['sens'])
                    #         logger.warning(logstring)
                else:
                    if not chan['samprate']:
                        chan['samprate'] = 0.0
                    if self.sentype in remaps:
                        chan['samprate'] = remaps[self.sentype][0]
                        self.rdl = sisxmlparser.ResponseDictLinkType(
                            Name=remaps[self.sentype][1],
                            SISNamespace="NCSS",
                            Type="PolesZeros"
                        )
                    else:
                        chan['samprate'] = 1.0
                    chan['siunit'], chan['sidesc'], chan['ssval'] = g_convert(
                        chan['siunit'], chan['sidesc'], chan['ssval']
                    )
                    response = sisxmlparser.CalResponseType(
                        Gain=sisxmlparser.SISGainType(
                            Value=chan['ssval'],
                            Frequency=chan['samprate'],
                            InputUnits=sisxmlparser.UnitsType(
                                Name=chan['siunit'],
                                Description=chan['sidesc'],
                            ),
                            OutputUnits=sisxmlparser.UnitsType(
                                Name=chan['sounit'],
                                Description=chan['sodesc'],
                            ),
                        ),
                        ResponseDictLink=self.rdl,
                    )
                pd = chan['chadate_inst']
                cali = self.get_calib(
                    pd,
                    # response, irange, irdes, orange, ordes,
                    response, irange, irdes, chan['sorange'], ordes,
                    comment=comment
                )

                if chan['sencomponent'] not in pds:
                    pds[chan['sencomponent']] = [pd]
                elif pd not in pds[chan['sencomponent']]:
                    pds[chan['sencomponent']].append(pd)
                else:
                    logger.info(f"Looks like {pd} is already in {chan['sencomponent']}")

                self.get_calis(name, cali)

            if name in self.calis:
                comps[name].Calibration = self.calis[name]

        complist = []
        for name in comps:
            if comps[name].Calibration != []:
                complist.append(comps[name])

        return complist

    def hardware(self):
        if self.serial == self.ml[0]['unitsn']:
            actual = True
        else:
            actual = False
        if self.hwis:
            hwg = sisxmlparser.HardwareInstallationGroupType(
                HardwareInstallation=self.hwis
            )
        else:
            hwg = None
        hardware = sisxmlparser.HardwareResponseType(
            Hardware=sisxmlparser.HardwareType(
                Sensor=[sisxmlparser.EquipType(
                    SerialNumber=str(self.serial),
                    ModelName=self.sentype,
                    Category="SENSOR",
                    IsActualSerialNumber=actual,
                    EquipmentEpoch=self.my_epochs,
                    Component=self.my_comps,
                )],
            ),
            ResponseDictGroup=self.rdg,
            HardwareInstallationGroup=hwg
        )
        return hardware

    def namer(self, folder):
        if not folder:
            my_folder = "sensors"
        else:
            my_folder = folder

        fname = f"SENSOR_{self.ml[0]['sentype']}_" \
                f"{self.serial}_" \
                f"v{self.version}.xml"
        fname = fname.replace("/", "-")
        fname = fname.replace(" ", "_")
        return f"{my_folder}/{fname}"

    def get_calis(self, component, cali):
        # gets the necessary calibrations - for each component,
        # only have new calibrations, no repeats

        # set to store all dates for each cali
        # if cali for component switches, add when they switch back
        # self.calis[component][idx] will need another entry?

        if component not in self.calis:
            self.calis[component] = []
            self.mycalis[component] = []
            self.mycalidates[component] = []

        cdict = convert2serialize(cali)
        d = cdict.pop('CalibrationDate')

        if d not in self.mycalidates[component]:
            if cdict not in self.mycalis[component]:
                self.calis[component].append(cali)
                self.mycalis[component].append(cdict)
                self.mycalidates[component].append(d)
            else:
                idx = self.mycalis[component].index(cdict)
                if d < self.mycalidates[component][idx]:
                    self.calis[component][idx] = cali
                    self.mycalis[component][idx] = cdict
                    self.mycalidates[component][idx] = d

