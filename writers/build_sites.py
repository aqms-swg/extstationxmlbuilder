import copy

import sisxmlparser3_0 as sisxmlparser
from functions import root, component_label, check_none, parse_operator, l4c_rdg, g_convert
from processing.multi_processor import dataloggerPrimaryMapping
import datetime
from logger_setup import my_logger as myl
logger = myl()


knownfirs = {
    "RT72A-07_50ss": "RT72A-07_50ss",
    "RT130_100ss": "RT130_100ss",
    "RT130_200ss": "RT130_200ss",
    "RT130_500ss": "RT130_500ss",
    "Q330Splus.1.100.below100": "Q330Splus.1.100.below100",
    "Q330Splus.1.100.all": "Q330Splus.1.100.below100",
    "HRD_200ss": "HRD24_1_200_0_001", # or HRD24_1_200_OFF
    "HRD_100ss": "HRD24_1_100_0_001", # or HRD24_1_100_OFF
    "HRD_500ss": "HRD24_1_500_0_001", # or HRD24_1_500_OFF
    "HRD_50ss": "HRD24_1_50_0_001", # or HRD24_1_50_OFF
    "TAURUS_100ss": "TAURUS_0_4_100_HI_0_001", # or TAURUS_0_4_100_HI_OFF
    # # or TAURUS_0_4_100_LO_0_001 or TAURUS_0_4_100_LO_OFF
    "TAURUS_200ss": "TAURUS_0_4_200_HI_0_001",  # or TAURUS_0_4_100_HI_OFF
    # # or TAURUS_0_4_200_LO_0_001 or TAURUS_0_4_200_LO_OFF
    "K2_100_CAUSAL": "K2_100_CAUSAL",
    "K2_100_ACAUSAL": "K2_1_100_NonC",
    "K2_200_CAUSAL": "K2_1_200_C",
    "K2_200_ACAUSAL": "K2_1_200_NonC",
    "ETNA2_100_CAUSAL": "ETNA2_1_100_C",
    "KML_ROCK_100": "KMI_ROCK_100_C",
    "KML_ROCK_200": "KMI_ROCK_200_C"
}

remaps = {"ARS-14": [10.0, "ARS-14 (ARS-14.NC.stbl)"],
          # "Geospace-HS1": [30.0, "OYO-HS1 (HS1.NC.stbl)"],
          "HS-1-LT VERTICAL": [30.0, "OYO-HS1 (HS1.NC.stbl)"],
          "HS-1-LT HORIZONTAL": [30.0, "OYO-HS1 (HS1.NC.stbl)"],
          "L-4C VERTICAL": [10.0, "L4-C Default (L4.NC.stbl)"],
          "L-4C HORIZONTAL": [10.0, "L4-C Default (L4.NC.stbl)"],
          # "Wilcoxon-731": [10.0, "WIL.NC.stbl"],
          "731A": [10.0, "WILCOXON731A (WIL.NC.stbl)"],
          }


def fir_namespace(fir):
    if fir in knownfirs:
        try:
            parsed = sisxmlparser.parse(f"templates/FIR/{knownfirs[fir]}.xml")
            rdg = parsed.HardwareResponse.ResponseDictGroup
            for rd in rdg.ResponseDict:
                if 'FIR' in dir(rd):
                    return rd.FIR.SISNamespace
        except:
            return "NRL"


def pos(degrees, angletype):
    # return sisxml formated position in degrees
    if angletype == "lat":
        return sisxmlparser.LatitudeType(
            ValueOf=float(degrees),
            unit="DEGREES",
        )
    elif angletype == "lon":
        return sisxmlparser.LongitudeType(
            ValueOf=float(degrees),
            unit="DEGREES",
        )


def is_float(element):
    try:
        float(element)
        return True
    except ValueError:
        return False


class Station:
    def __init__(self, code, stations, channels, sensors, loggers, amps=[],
                 version=4, folder=None):
        self.code = code
        self.version = version
        self.comments = []
        self.linked = {}
        # self.link_equipment(channels, loggers, sensors)
        self.loggers = loggers
        self.remdate = self.last_end_date(sensors, loggers, channels)
        self.channels = self.channel_builder(channels)
        self.stations = self.station_builder2(stations, channels)
        self.template = root(code.split(".")[0], stations=self.stations)
        self.outputname = self.namer(folder)

    def last_end_date(self, sensors, dataloggers, channels):
        lastdate = sensors[0]['sendate_rem']
        for sensor in sensors:
            ed = sensor['sendate_rem']
            if type(ed) == datetime.datetime:
                if type(lastdate) != datetime.datetime or ed > lastdate:
                    lastdate = ed
            elif not ed:
                return None
        for dl in dataloggers:
            ed = dl['recdate_rem']
            if type(ed) == datetime.datetime:
                if type(lastdate) != datetime.datetime or ed > lastdate:
                    lastdate = ed
            elif not ed:
                return None
        for chan in channels:
            ed = chan['chadate_rem']
            if type(lastdate) != datetime.datetime:
                lastdate = ed
            elif not ed:
                return None
            elif ed > lastdate:
                lastdate = ed
        return lastdate

    def channel_builder(self, channels):
        added = {}
        chans = []
        # inst_chans = {}

        for chan in channels:
            comp = component_label(chan['seedchan'])
            c2 = f"{comp}_{chan['dlchan']}"

            if check_none(chan['lcode']):
                c2 += f"_{chan['lcode']}"

            # print(comp, chan['dlchan'], c2)

            if c2 not in added:
                added[c2] = []
            # sen = chan['sid']
            # if sen not in inst_chans:
            #     inst_chans[sen] = [comp]
            # else:
            #     inst_chans[sen].append(comp)

            d = chan['dip']
            if (type(d) == str and is_float(d)) or type(d) == int or type(d) == float:
                d = float(d)
            else:
                d = 0.0
            dip = sisxmlparser.DipType(ValueOf=d, unit="DEGREES")

            az = chan['azim']
            if (type(az) == str and is_float(az)) or type(az) == int:
                az = float(az)
            if type(az) == float:
                if az >= 360.0:
                    az -= 360.0
                if az <= -360.0:
                    az += 360.0
            else:
                az = 0.0
            azim = sisxmlparser.AzimuthType(ValueOf=az, unit="DEGREES")

            if 'samprate' in chan and check_none(chan['samprate']):
                samprate = sisxmlparser.FloatType(
                    ValueOf=float(chan['samprate']),
                )
            else:
                samprate = None
            lcode = ''
            if check_none(chan['lcode']):
                lcode = check_none(chan['lcode'])
            if 'chadate_rem' in chan:
                ed = chan['chadate_rem']
            elif self.remdate:
                ed = self.remdate
            else:
                ed = None
            sd = chan['chadate_inst']
            if sd == self.remdate:
                pass
            else:
                new_channel = sisxmlparser.SISChannelType(
                    code=chan['seedchan'],
                    locationCode=lcode,
                    startDate=sd,
                    endDate=ed,
                    Latitude=pos(chan['chalat'], "lat"),
                    Longitude=pos(chan['chalon'], "lon"),
                    Dip=dip,
                    Azimuth=azim,
                    SampleRate=samprate,
                    StationChannelNumber=chan['stachan'],
                    Elevation=sisxmlparser.DistanceType(
                        ValueOf=float(chan['elevation']),
                        unit="METERS"
                    ),
                    Depth=sisxmlparser.DistanceType( # need to get this from...
                        ValueOf=0.0,
                        unit="METERS"
                    ),
                    Response=self.response2(chan), # need to get gain in here...
                    SensorSiteDescription=sisxmlparser.SensorSiteDescriptionType(
                        StationHousing=chan['cosmoscode']
                    )
                )
                if sd != ed:
                    if not lcode:
                        lc = ""
                    else:
                        lc = lcode
                    olap_comp = f"{comp}_{chan['seedchan']}"
                    if check_none(lc):
                        olap_comp += f"_{lc}"
                    added, skip = self.overlaps(added, [olap_comp, sd, ed])
                    if skip:
                        print("skipping due to overlap!", added, olap_comp, sd, ed)
                    if not skip:
                        chans.append(new_channel)
                    else:
                        print("skipping", chan['seedchan'], chan['lcode'], sd)
        return chans

    @staticmethod
    def overlaps(used, new): ###
        import time
        if not new[2]:
            new[2] = datetime.datetime(3000, 1, 1)

        if new[0] not in used:
            skip = False
            used[new[0]] = [[new[1], new[2]]]
        else:
            skip = False
            for se in used[new[0]]:

                if se[0] <= new[1] < se[1]:
                    skip = True
                if se[0] < new[2] <= se[1]:
                    skip = True
            if not skip:
                used[new[0]].append([new[1], new[2]])
        return used, skip

    @staticmethod
    def sr(sequence, elink=None, rdetail=None, respf=None, rdl=None):
        sr = sisxmlparser.SubResponseType()
        sr.sequenceNumber=sequence
        if elink:
            sr.EquipmentLink=elink
        if rdetail:
            sr.ResonseDetail=rdetail
        if respf:
            sr.RESPFile=respf
        if rdl:
            sr.ResponseDictLink=rdl
        if not elink and not rdetail and not respf and not rdl:
            sr = None
        else:
            sequence += 1
        return sr, sequence

    def response2(self, chan):
        subresponses = []
        chid = chan['chaid']
        dpm = {k.lower(): v for k, v in dataloggerPrimaryMapping.items()}
        logid = None
        if check_none(chan['lmodel']):
            # if chan['lmodel'].lower() in dpm:
            #     logid = dpm[chan['lmodel'].lower()]
            if 'recsn' in chan:
                logid = chan['recsn']
            elif 'recid_fk' in chan:
                logid = chan['recid_fk']
            else:
                logid = chan['netstarec'].split("-")[2]
            for dl in self.loggers:
                if str(dl['recid']) == str(logid):
                    logid = dl['recsn']
        i = 1
        n = 1
        ampchans = {}
        if check_none(chid) and check_none(chan['unitsn']):
            if check_none(chid):
                elink = sisxmlparser.EquipmentLinkType(
                            SerialNumber=chan['unitsn'],
                            ModelName=chan['smodel'],
                            Category="SENSOR",
                            ComponentName=chan['sencomponent']
                        )
                sr, i = self.sr(i, elink=elink)
                subresponses.append(sr)
                if 'ampmod' in chan and check_none(chan['ampmod']) \
                        and check_none(chan['ampsn']):
                    if chan['sencomponent'] not in ampchans:
                        ampchans[chan['sencomponent']] = n
                        ampcomponent = n
                        n += 1
                    else:
                        ampcomponent = ampchans[chan['sencomponent']]
                    if type(chan['ampmod']) == list:
                        chan['ampmod'] = chan['ampmod'][1]
                    elink = sisxmlparser.EquipmentLinkType(
                                SerialNumber=chan['ampsn'],
                                ModelName=chan['ampmod'],
                                Category=chan['amptype'],
                                ComponentName=f"COMPONENT{ampcomponent}"
                            )
                    sr, i = self.sr(i, elink=elink)
                    subresponses.append(sr)
                if 'lmodel' in chan and check_none(chan['lmodel']):
                    elink = sisxmlparser.EquipmentLinkType(
                                SerialNumber=f"{logid}-B1",  # netstarec.split("-")[2]
                                ModelName=chan['lmodel'],  # looks like this isn't
                                Category="LOGGERBOARD",
                                ComponentName=chan['logcomponent']
                            )
                    sr, i = self.sr(i, elink=elink)
                    subresponses.append(sr)
                if check_none(chan['logrespf']):
                    lrespf = sisxmlparser.RESPFileType(
                        ValueOf=chan['logrespf'],
                        stageFrom=4,
                    )
                    sr, i = self.sr(i, respf=lrespf)
                    subresponses.append(sr)


                # only using fir filter if no log response file when this v was elif
                elif check_none(chan['firfilter']) and chan['firfilter'] in knownfirs:
                    rdl = sisxmlparser.ResponseDictLinkType2(
                                    Name=knownfirs[chan['firfilter']],
                                    SISNamespace=fir_namespace(chan['firfilter']),
                                    Type="FilterSequence"
                                )
                    sr, i = self.sr(i, rdl=rdl)
                    subresponses.append(sr)

            sfreq = 1.0
            if 'smodel' in chan and chan['smodel'] in remaps:
                sfreq = remaps[chan['smodel']][0]
            # chan['siunit'], chan['sidesc'], chan['totalsens'] = g_convert(
            #     chan['siunit'], chan['sidesc'], chan['totalsens']
            # ) # leaving as G-mks?
            if not check_none(chan['lounit']):
                chan['lounit'] = "counts"
                chan['lodesc'] = "Digital counts"

            response = sisxmlparser.SISResponseType(
                InstrumentSensitivity=sisxmlparser.SensitivityType(
                    Value=chan['totalsens'],
                    Frequency=sfreq,
                    InputUnits=sisxmlparser.UnitsType(
                        Name=chan['siunit'],
                        Description=chan['sidesc'],
                    ),
                    OutputUnits=sisxmlparser.UnitsType(
                        Name=chan['lounit'],
                        Description=chan['lodesc'],
                    )
                ),
                SubResponse=subresponses
            )
            return response
# TODO: CADB, 1745, 1767, 1



    def comment_builder(self, coms, cdates):
        # my_coms = []
        my_logs = []
        added_dates = {}
        if len(coms) == 0:
            return my_logs
        elif all(com == "" for com in coms):
            return my_logs
        else:
            for uc in coms:
                if uc.strip() not in ["", "."]:
                    idx = coms.index(uc)
                    startdate = cdates[idx][0]
                    # stringrep = f"{uc} {cdates[idx][0]}"
                    # if stringrep not in added:
                    # mc = sisxmlparser.CommentType(
                    #     Value=uc,
                    #     BeginEffectiveTime=cdates[idx][0],
                    #     # EndEffectiveTime=cdates[idx][1],
                    # )
                    # added.append(stringrep)
                    # my_coms.append(mc)

                    ml = sisxmlparser.LogType(
                        LogDate=startdate,
                        Subject=f"{self.code} {startdate.strftime('%Y/%m/%d')} log",
                        LogText=uc,
                        OffDate=cdates[idx][1],
                    )

                    if startdate not in added_dates:
                        added_dates[startdate] = [uc]
                    else:
                        for com in added_dates[startdate]:
                            if uc not in com and com not in uc:
                                my_logs.append(ml)
                            elif uc in com:
                                pass
                            elif com in uc:
                                for log in my_logs:
                                    if log.LogText == com and log.LogDate == startdate:
                                        my_logs.remove(log)
                                my_logs.append(ml)


                        # my_logs.append(ml)
            # mcsorted = sorted(my_coms, key=lambda x: x.BeginEffectiveTime)
            mlsorted = sorted(my_logs, key=lambda x: x.LogDate)
            # return mcsorted
            return mlsorted


    def station_builder2(self, stations, channels):
        my_stations = []
        idates = []
        rdates = []
        comments = []
        for sta in stations:
            if 'comment' in sta:
                comments.append(sta['comment'])
            else:
                comments.append("")
            idates.append(sta['stadate_inst'])
            rdates.append(sta['stadate_rem'])
        for chan in channels:
            idates.append(chan['chadate_inst'])
            rdates.append(chan['chadate_rem'])
        try:
            minst = min(idates)
        except:
            minst = datetime.datetime(1966, 1, 1, 0, 0, 0)

        try:
            mrem = max(rdates)
        except:
            mrem = None

        uniquecoms = []
        ucds = []
        for com in comments:
            idx = comments.index(com)

            if com not in uniquecoms:
                uniquecoms.append(com)
                ucds.append([idates[idx], rdates[idx]])
            # elif com == uniquecoms[-1]:
            #     ucds[-1][1] = rdates[idx]
            elif com != uniquecoms[-1]:
                uniquecoms.append(com)
                ucds.append([idates[idx], rdates[idx]])

        my_coms = self.comment_builder(uniquecoms, ucds)

        raz = None
        for sta in stations:
            if not raz:
                if 'refnorth' in sta:
                    if check_none(sta['refnorth']):
                        raz = float(sta['refnorth'])
                        if raz >= 360.0:
                            raz -= 360.0
        if not raz:
            raz = 0.0
        refaz = sisxmlparser.AzimuthType(
            ValueOf=raz,
            unit="DEGREES",
        )
        ele = None
        for sta in stations:
            if not ele:
                if 'elevation' in sta:
                    if check_none(sta['elevation']):
                        ele = float(sta['elevation'])
        if not ele:
            ele = 0.0

        station = sisxmlparser.SISStationType(
            code=stations[0]['stacode'],
            startDate=minst,
            endDate=self.remdate,
            Latitude=pos(stations[0]['stalat'], "lat"),
            Longitude=pos(stations[0]['stalon'], "lon"),
            Elevation=sisxmlparser.DistanceType(
                ValueOf=ele,
                unit="METERS"
            ),
            ReferenceAzimuth=refaz,
            Site=sisxmlparser.SiteType(
                Name=stations[0]['staname'],
                Town=stations[0]['city'],
                Region=stations[0]['state'],
                # removing this because of Jason's
                # issues with region meaning different things for different networks
                # sta['state'],
                Country="USA",
                Description=stations[0]['short_name']
            ),
            Operator=[sisxmlparser.OperatorType(
                Agency=parse_operator(stations[0]['netcode']),
            )],
            CreationDate=minst,
            TerminationDate=mrem,
            TotalNumberChannels=len(self.channels),  # use len(chans) or similar?
            SelectedNumberChannels=len(self.channels),
            Channel=self.channels,
            GeoSite=sisxmlparser.GeoSiteType(
                Place=sisxmlparser.PlaceType(
                    Name=f"{stations[0]['netcode']}:{stations[0]['stacode']}",
                    Latitude=pos(stations[0]['stalat'], "lat"),
                    Longitude=pos(stations[0]['stalon'], "lon"),
                ),
                SiteNetCode=stations[0]['netcode'],
                SiteLookupCode=stations[0]['stacode'],
                SiteTypeTag=["Seismic Station"],
            ),
        )
        if my_coms:
            station.GeoSite.SiteLog = []
            for log in my_coms:
                station.GeoSite.SiteLog.append(log)
            # station.Comment = my_coms
        my_stations.append(station)
        return my_stations


    def station_builder(self, stations):
        my_stations = []
        for sta in stations:
            refaz = None
            if 'refnorth' in sta:
                if check_none(sta['refnorth']):
                    raz = float(sta['refnorth'])
                    if raz >= 360.0:
                        raz -= 360.0
                else:
                    raz = 0.0
                refaz = sisxmlparser.AzimuthType(
                    ValueOf=raz,
                    unit="DEGREES",
                )
            else:
                logger.info("there is no refnorth!")
            station = sisxmlparser.SISStationType(
                code=sta['stacode'],
                startDate=sta['stadate_inst'],
                endDate=self.remdate,
                Latitude=pos(sta['stalat'], "lat"),
                Longitude=pos(sta['stalon'], "lon"),
                Elevation=sisxmlparser.DistanceType(
                    ValueOf=float(sta['elevation']),
                    unit="METERS"
                ),
                ReferenceAzimuth=refaz,
                Site=sisxmlparser.SiteType(
                    Name=sta['staname'],
                    Town=sta['city'],
                    Region=sta['state'],
                    # removing this because of Jason's
                    # issues with region meaning different things for different networks
                    # sta['state'],
                    Country="USA",
                    Description=sta['short_name']
                ),
                Operator=[sisxmlparser.OperatorType(
                    Agency=parse_operator(sta['netcode']),
                )],
                CreationDate=sta['stadate_inst'],
                TotalNumberChannels=len(self.channels),  # use len(chans) or similar?
                SelectedNumberChannels=len(self.channels),
                Channel=self.channels,
                GeoSite=sisxmlparser.GeoSiteType(
                    Place=sisxmlparser.PlaceType(
                        Name=f"{sta['netcode']}:{sta['stacode']}",
                        Latitude=pos(sta['stalat'], "lat"),
                        Longitude=pos(sta['stalon'], "lon"),
                    ),
                    SiteNetCode=sta['netcode'],
                    SiteLookupCode=sta['stacode'],
                    SiteTypeTag=["Seismic Station"],
                ),
            )
            # if 'comment' in sta:
            #     # if sta['comment'].strip() not in ["", None, "none", "-"]:
            #     #     station.Comment = [sisxmlparser.CommentType(
            #     #         Value=sta['comment']
            #     #     )]
            #     if sta['comment'].strip() not in ["", None, "none", "-"]:
            #         station.GeoSite.SiteLog = sisxmlparser.LogType(
            #
            #         )
            my_stations.append(station)
        return my_stations

    def namer(self, folder):
        if not folder:
            my_folder = "stations"
        else:
            my_folder = folder
        fname = f"STATION_{self.code}_v{self.version}.xml".replace(" ", "_")
        return f"{my_folder}/{fname}"

# set up meeting with Jacob at 11 pacific, 2 eastern on wednesday
