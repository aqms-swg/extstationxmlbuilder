from functions import openpickle, write_template, check_none, convert_types, \
    picklewriter, overlap_serial
from writers.build_sensors import Sensor
from writers.build_loggers import Logger
from writers.build_sites import Station
from writers.build_amplifier import Amplifier
from processing.multi_processor import multi_loggers as mlf, multiSNs
from processing.processor import input_processor
from datetime import datetime
import copy
import shelve
import time
from logger_setup import my_logger as myl
my_logger = myl(launcher=True)

# the purpose of this file/function is to group together the relevant pieces
# for sites and equipment, from NOD and the FS


class Coalesce:
    def __init__(self, fs="objs/FS_dic.obj", nod="objs/NOD_dic.obj", justfs=False):
        if fs:
            self.fs = openpickle(fs)
        else:
            self.fs = {}
        if not justfs:
            self.nod = openpickle(nod)
        else:
            self.nod = {}
        self.combo = self.initial_combine()
        if justfs:
            self.justfs_combine(nod)

    def initial_combine(self):
        db_info = {
            "stations": self.combine_component("stations"),
            "sensors": self.combine_component("sensors"),
            "recorders": self.combine_component("recorders"),
            "channels": self.combine_component("channels"),
            "amplifiers": self.combine_component("amplifiers")
        }
        return db_info

    def justfs_combine(self, nod):
        # for cat in self.combo:
        #     print(f"Got this many {cat}:", len(self.combo[cat]))
        opn = openpickle(nod)
        nss = []
        for sta in self.combo['stations']:
            ns = f"{sta['netcode']}-{sta['stacode']}"
            if ns not in nss:
                nss.append(ns)
        for sta in opn['stations']:
            ns = f"{sta['netcode']}-{sta['stacode']}"
            if ns in nss:
                self.combo['stations'].append(sta)
        for sen in opn['sensors']:
            nsrc = sen['netstareccha'].split("-")
            ns = f"{nsrc[0]}-{nsrc[1]}"
            if ns in nss:
                self.combo['sensors'].append(sen)
        for rec in opn['recorders']:
            ns = rec['netsta']
            if ns in nss:
                self.combo['recorders'].append(rec)
        for chan in opn['channels']:
            nsr = chan['netstarec'].split("-")
            ns = f"{nsr[0]}-{nsr[1]}"
            if ns in nss:
                self.combo['channels'].append(chan)
        # for amp in opn['amplifiers']: # none of the sites seem to have amplifiers
        #     ns = amp['netsta']
        #     if ns in nss:
        #         self.combo['amplifiers'].append(sta)

        # for cat in self.combo: # to see how many got added
        #     print(f"Now, got this many {cat}:", len(self.combo[cat]))


    def combine_component(self, component):
        if self.fs:
            items = copy.deepcopy(self.fs[component])
            if component in self.nod:
                for item in self.nod[component]:
                    items.append(item)
        else:
            items = copy.deepcopy(self.nod[component])
        return items

    def get_relchan_dates(self, sen):
        nsrc = sen['netstareccha'].split("-")
        sc = copy.deepcopy(sen)
        fixed = False
        maybes = []
        for chan in self.combo['channels']:
            c = chan
            nsr = chan['netstarec'].split("-")
            if nsrc[0] == nsr[0] and nsrc[1] == nsr[1] and \
                    str(sen['chaid_fk']) == str(chan['chaid']):
                maybes.append(chan)
                if check_none(chan['chadate_inst']):
                    sc['sendate_inst'] = chan['chadate_inst']
                    sc['sendate_rem'] = chan['chadate_rem']
                    fixed = True
                    break
            # c = chan
            # if chan['netstarec'] in sen['netstareccha'] \
            #         and sen['chaid_fk'] == chan['chaid'] \
            #         and check_none(chan['chadate_inst']):
            #     sc['sendate_inst'] = chan['chadate_inst']
            #     sc['sendate_rem'] = chan['chadate_rem']
            #     fixed = True
            #     break
        if not fixed:
            if len(maybes) != 0:
                my_logger.warning(f"Couldn't find a sendate_inst for {sen} ||| {maybes}")
            else:
                my_logger.info(f"No channels match {sen}")

        return sc


    def sen_dict(self):
        # groups the sensors with matching model + unitsn together
        # may need to do this again after correcting the model names
        sen_dict = {}
        for sen in self.combo["sensors"]:

            if not check_none(sen['sendate_inst']):
                os = copy.deepcopy(sen)
                sen = self.get_relchan_dates(os)
            # if sen['sendate_inst'] != sen['sendate_rem']:
            # staname = sen['netstareccha'].split("-")[1]
            usn = sen['unitsn']
            if not check_none(usn):
                usn = sen['sensn']
            if not check_none(usn):
                usn = sen['senid']
            model = sen['sentype']
            if check_none(usn) and check_none(model) and check_none(sen['sendate_inst']):
                mykey = f"{model.strip()}_{usn}"
                if mykey not in sen_dict:
                    sen_dict[mykey] = [sen]
                else:
                    sen_dict[mykey].append(sen)

        return sen_dict

    def dl_dict(self):
        # groups the recorders together
        dl_dict = {}
        for dl in self.combo["recorders"]:
            # if dl['recdate_inst'] != dl['recdate_rem'] \
            #         and check_none(dl['recdate_inst']):
                # staname = sen['netstareccha'].split("-")[1]
            usn = dl['recsn']
            if not check_none(usn):
                usn = dl['recid']
            model = convert_types(dl['rectype'], eqtype="dl")
            dl['rectype'] = model
            if check_none(model) and check_none(usn):
                mykey = f"{model}_{usn}"
                if mykey not in dl_dict:
                    dl_dict[mykey] = [dl]
                else:
                    dl_dict[mykey].append(dl)
        return dl_dict

    def sta_dict(self):
        sta_dict = {}
        for sta in self.combo['stations']:
            if sta['stadate_inst'] != sta['stadate_rem']:
                netsta = f"{sta['netcode']}.{sta['stacode']}"
                if netsta not in sta_dict:
                    sta_dict[netsta] = [sta]
                else:
                    sta_dict[netsta].append(sta)
        return sta_dict

    def amp_dict(self):
        amp_dict = {}
        for amp in self.combo['amplifiers']:
            if amp['ondate'] != amp['offdate']:
                mykey = f"{amp['model']}_{amp['serial']}"
                if mykey not in amp_dict:
                    amp_dict[mykey] = [amp]
                else:
                    amp_dict[mykey].append(amp)
        return amp_dict


multi_loggers = ["T2", "0x1583e50", "0x0167BF64", "0x1583E6E", "0x0167BF6E", "0x160C8ED",
                 "0x019E7ECF", "0x160C8EF", "0x019E7F03", "0x160C8F4", "0x01A1EF05",
                 "0x160C8F5", "0x01A1EF09", "0x160c8f6", "0x1001934", "0x16282e8",
                 "0x1011D54", "0x19BCEC7", "0x1011d62", "0x19E7ED5", "0x1011d86",
                 "0x19E7EDC", "0x1011D86", "0x19E7EF3", "0x1011dd0", "0x1A1EEEC",
                 "0x1011DD0", "0x1A1EF16", "0x1011DEF", "0xa5df8c", "0x1210BD0",
                 "0xA6D3BA", "0x13f76cc", "0xA80BB4", "0x13F76CC", "0xA84F4F",
                 "0x14B9EFE", "N13", "GeyserPk-unkn", "N2"]


def get_sensor_channels(cd, write=False, output="objs/grouped_eq.obj"):
    # puts the equipment connected to sensors together for processing
    grouped_eq = {}
    gescs = {}

    cds = copy.deepcopy(cd['sensors'])
    for key in cds:
        for sen in cds[key]:
            # little tweak to fix redundant sensn/unitsn naming for a station

            if sen['senid'] in [14698, 14699, 146700] \
                    and sen['chaid_fk'] in [15621, 15622, 15623]:
                sen['unitsn'] = '101255_10'
                k2 = key + "_10"
                if k2 not in cd['sensors']:
                    cd['sensors'][k2] = []
                    my_logger.warning(f"ADDING {k2} to cd['sensors']!, {k2 in cd['sensors']}")
                cd['sensors'][k2].append(sen)
                if sen in cd['sensors'][key]:
                    cd['sensors'][key].remove(sen)
            if sen['senid'] == 739 and sen['chaid_fk'] == 811:
                sen['unitsn'] = '1299_CR'
                k2 = key + "_10"
                if k2 not in cd['sensors']:
                    cd['sensors'][k2] = []
                    my_logger.warning(f"ADDING {k2} to cd['sensors']!, {k2 in cd['sensors']}")
                cd['sensors'][k2].append(sen)
                if sen in cd['sensors'][key]:
                    cd['sensors'][key].remove(sen)
    # ------------------------

    for key in cd['sensors']:

        chids = []
        checked_recids = []
        my_netstas = []
        valid_sens = []
        for sen in cd['sensors'][key]: # check to see if sendate_inst is present
            # -------------
            if check_none(sen['sendate_inst']):
                valid_sens.append(sen)
        grouped_eq[key] = {
            "sens": valid_sens,
            "chans": [], "dls": [], "amps": []}
        for sen in cd['sensors'][key]:
            netsta = "-".join(sen['netstareccha'].split("-")[:2])
            my_netstas.append(netsta)
            chid = str(sen['chaid_fk']).strip()
            if chid not in chids:
                chids.append(chid)

        for chan in cd['channels']:
            sc = chan['seedchan']
            se = [chan['chadate_inst'], chan['chadate_rem']]
            ns = "-".join(chan['netstarec'].split("-")[:2])
            if str(chan['chaid']).strip() in chids and ns in my_netstas \
                    and check_none(chan['chadate_inst']) \
                    and chan['chadate_inst'] != chan['chadate_rem']:
                if key not in gescs:
                    gescs[key] = {}
                if sc not in gescs[key]:
                    gescs[key][sc] = []
                # if se not in gescs[key][sc]: # this is the problem, I think
                grouped_eq[key]['chans'].append(chan)
                gescs[key][sc].append(se)

        for chan in grouped_eq[key]['chans']:
            recid = str(chan['recid_fk'])
            recsn = chan['netstarec'].split("-")[-1]
            if recid not in checked_recids and check_none(recid):
                checked_recids.append(recid)
                for k2 in cd['recorders']:
                    for dl in cd['recorders'][k2]:

                        if str(dl['recid']) == recid \
                                and dl['netsta'] in my_netstas \
                                and str(dl['recsn']) == recsn:
                            if dl not in grouped_eq[key]['dls']:
                                # dl['rectype'] = convert_types(dl['rectype'], eqtype="dl")
                                grouped_eq[key]['dls'].append(dl)

        print(f"For {key}, found {len(grouped_eq[key]['chans'])} channels"
              f" and {len(grouped_eq[key]['dls'])} loggers!")

    if write:
        picklewriter(output, grouped_eq)

    return grouped_eq


def combiner(write=False, fs="objs/FS_dic.obj", nod="objs/NOD_dic.obj", justfs=False,
             output="objs/fs_nod_grouped.obj"):
    co = Coalesce(fs=fs, nod=nod, justfs=justfs)
    send = co.sen_dict()
    recd = co.dl_dict()
    stad = co.sta_dict()
    ampd = co.amp_dict()
    grouped = {
        "stations": stad,
        "sensors": send,
        "recorders": recd,
        "amplifiers": ampd,
        "channels": co.combo['channels']
    }
    if write:
        picklewriter(output, grouped)

    return grouped


def processor_grouper(cd, allinfo, write=False, outfile="objs/processed.obj"):
    # processes sensors/channels and groups things together for writing
    processed = {"sens": {}, "chans": [], "dls": {}, "sites": {}}
    for key in cd:
        pieces = key.split("_")
        if "-" not in pieces:
            d = cd[key]
            pchans, psens = input_processor(d['chans'], d['sens'], d['dls'])

            if pchans and psens:
                newkey = f"{psens[0]['sentype']}_{psens[0]['unitsn']}"
                print("grouping & processing for sensor: ", key)
                if newkey not in processed['sens']:
                    processed['sens'][newkey] = {
                        "psens": psens, "pchans": pchans, "dls": d['dls']}
                else:
                    # for sen in psens:
                    #     if sen not in processed['sens'][newkey]['psens']:
                    #         processed['sens'][newkey]['psens'].append(sen)
                    processed['sens'][newkey]['psens'].extend(psens)
                    # for chan in pchans:
                    #     if chan not in processed['sens'][newkey]['pchans']:
                    #         processed['sens'][newkey]['pchans'].append(chan)
                    processed['sens'][newkey]['pchans'].extend(pchans)
                    # for dl in d['dls']:
                    #     if dl not in processed['sens'][newkey]['dls']:
                    #         processed['sens'][newkey]['dls'].append(dl)
                    processed['sens'][newkey]['dls'].extend(d['dls'])

                for chan in pchans:
                    if chan not in processed['chans']:
                        processed['chans'].append(chan)

    for key in allinfo['recorders']:
        pieces = key.split("_")
        if "-" not in pieces:
            pchans = []
            rids = []
            rsns = []
            for dl in allinfo['recorders'][key]:
                if dl['recsn'] not in rsns and check_none(dl['recsn']):
                    rsns.append(str(dl['recsn']))
                if dl['recid'] not in rids and check_none(dl['recid']):
                    rids.append(str(dl['recid']))
            for chan in processed['chans']:
                rid = str(chan['recid_fk'])
                rsn = chan['netstarec'].split("-")[-1]
                if rid in rids and rsn in rsns:
                    pchans.append(chan)
            processed['dls'][key] = {
                "dls": allinfo['recorders'][key], "chans": pchans}
            print(f"adding dl info to {key}")

    for key in allinfo['stations']:
        stas = allinfo['stations'][key]
        net = stas[0]['netcode']
        sta = stas[0]['stacode']
        sens = []
        dls = []
        chans = []
        amps = []
        recsns = []
        for k2 in processed['sens']:
            psens = processed['sens'][k2]['psens']
            for psen in psens:
                nsrc = psen['netstareccha']
                snet = nsrc.split("-")[0]
                ssta = nsrc.split("-")[1]
                if ssta == sta and snet == net:
                    sens.append(psen)
        for pchan in processed['chans']:
            nsr = pchan['netstarec'].split("-")
            cnet = nsr[0]
            csta = nsr[1]
            crec = nsr[2]
            if cnet == net and csta == sta:
                chans.append(pchan)
                recsns.append(crec)
                # chans have proper units here
        for k3 in processed['dls']:
            k3dls = processed['dls'][k3]
            for dl in k3dls['dls']:
                nss = dl['netsta'].split("-")
                if nss[0] == net and nss[1] == sta and dl['recsn'] in recsns:
                    dls.append(dl)

        for ampk in allinfo['amplifiers']:
            for amp in allinfo['amplifiers'][ampk]:
                ans = amp['netsta'].split("-")
                if ans[0] == net and ans[1] == sta:
                    amps.append(amp)

        if len(sens) == 0:
            my_logger.warning(f"Got 0 sensors for {key} {stas[0]['src']}")
        if len(chans) > 0:
            processed['sites'][key] = {
                "stas": stas, "sens": sens, "dls": dls,
                "chans": chans, "amps": amps}
            print(f"filling in equipment for {key}")

    print(f"grouped processed info for {len(processed['sens'])} sensors,"
          f" {len(processed['dls'])} loggers and {len(processed['sites'])} sites!")
    if write:
        picklewriter(outfile, processed)
    return processed

# For L4C_1122, found 16 channels and 0 loggers!


def write_amps(upd, sn=None, mod=None, writef=True, rewrite=False, folder="xmls/amplifiers"):
    # writes amplifiers/VCOs (used by siswriter)
    written_amps = {}
    for key in upd['amplifiers']:
        ks = key.split("_")
        if check_none(ks[0]):
            uak = upd['amplifiers'][key]
            serial = uak[0]['serial']
            model = uak[0]['model'].strip()
            write = False
            if not sn and not mod:
                write = True
            elif sn and not mod and serial == str(sn):
                write = True
            elif mod and not sn and mod == model:
                write = True
            elif sn and mod and serial == str(sn) and mod == model:
                write = True
            if write:
                written_amps[f"{model}~{serial}"] = ampw(serial, uak,
                                                         write=writef, rewrite=rewrite,
                                                         folder=folder)
    return written_amps


def write_sensors(processed, sn=None, mod=None, writef=True, rewrite=False, src=None,
                  folder="xmls/sensors"):
    # writes sensors (used by siswriter)
    written_sensors = {}
    for key in processed['sens']:
        d = processed['sens'][key]
        psens = d['psens']
        pchans = d['pchans']
        write = False
        if len(psens) > 0 and len(pchans) > 0:
            serial = psens[0]['unitsn']
            model = psens[0]['sentype']
            if not sn and not mod:
                write = True
            elif sn and not mod and str(sn) in str(serial):
                write = True
            elif mod and not sn and mod == model:
                write = True
            elif sn and mod and str(sn) in str(serial) and mod == model:
                write = True
            if src and write:
                if src.upper() != psens[0]['src']:
                    write = False
            if check_none(serial) and check_none(model):
                pass
            else:
                write = False

            if write:
                my_sensor = senw(serial, psens, pchans, write=writef, rewrite=rewrite, folder=folder)
                written_sensors[f"{model}_{serial}"] = my_sensor
        else:
            my_logger.warning(f"Couldn't write an xml for {key} because "
                              f"it has {len(psens)} processed sensors "
                              f"and {len(pchans)} processed channels")
    return written_sensors


def write_loggers(processed, sn=None, mod=None, writef=True, rewrite=False, src=None,
                  folder="xmls/dataloggers"):
    # writes loggers (used by siswriter)
    written_loggers = {}
    for key in processed['dls']:
        d = processed['dls'][key]
        dls = d['dls']
        pchans = d['chans']

        if len(dls) > 0 and len(pchans) > 0:
            serial = d['dls'][0]['recsn']
            model = d['dls'][0]['rectype']
            write = None
            if not sn and not mod:
                write = True
            elif sn and not mod and str(sn) in str(serial):
                write = True
            elif mod and not sn and mod == model:
                write = True
            elif sn and mod and str(sn) in str(serial) and mod == model:
                write = True
            if src and write:
                if src != dls[0]['src']:
                    write = False
            if write:
                my_dl = dlw(serial, dls, pchans, write=writef, rewrite=rewrite, folder=folder)
                written_loggers[f"{model}_{serial}"] = my_dl
        else:
            pass
            # my_logger.warning(f"Couldn't write an xml for {key} because "
            #                   f"it has {len(dls)} dataloggers "
            #                   f"and {len(pchans)} processed channels")
    return written_loggers


def write_sites(processed, net=None, sta=None, writef=True, rewrite=False, src=None, folder="xmls/stations"):
    # writes sites (used by siswriter)
    written_sites = {}
    for key in processed['sites']:
        ks = key.split(".")
        my_net = ks[0]
        my_sta = ks[1]

        match = False
        if net and sta:
            if my_net == net and my_sta == sta:
                match = True
        elif net and not sta:
            if my_net == net:
                match = True
        elif sta and not net:
            if my_sta == sta:
                match = True
        elif not sta and not net:
            match = True

        # if match and src:
        #     if src == processed['sites'][key]['stas'][0]['src']:
        #         match = True
        #     else:
        #         match = False

        if match:
            pk = processed['sites'][key]
            if len(pk['stas']) > 0 and len(pk['sens']) > 0 and len(pk['chans']) > 0:
                # try:
                my_site = sitew(key, pk['stas'], pk['chans'],
                                pk['sens'], pk['dls'],  pk['amps'],
                                write=writef, rewrite=rewrite, folder=folder)
                written_sites[f"{my_net}_{my_sta}"] = my_site
                # except Exception as e:
                #     my_logger.warning(f"Couldn't write for {key} due to {e}")
    return written_sites


def sitew(code, stations, pchans, psens, pdls, amps,
          version="10", write=True, rewrite=False, folder=None):
    # writes sites
    successfully_wrote = shelve.open("objs/shelves/finished_site_xmls")
    msg = f"trying to write for {code}! I have {len(stations)} stations, " \
          f"{len(psens)} sensors, {len(pdls)} loggers, " \
          f"{len(pchans)} channels! and {len(amps)} amps!"
    my_logger.info(msg)

    for ps in psens:
        sn0 = ps['unitsn']
        ovs = overlap_serial(ps['unitsn'], "SENSORS", ps['sentype'],
                             ps['netstareccha'].split("-")[0])
        if ovs != sn0:
            ps['unitsn'] = ovs
            for pc in pchans:
                if pc['chaid'] == sn0 or ps['chaid_fk'] == pc['chaid']:
                    pc['chaid'] = ovs
                    pc['unitsn'] = ovs
                if pc['sid'] == sn0:
                    pc['sid'] = ovs

    for pdl in pdls:
        sn0 = pdl['recsn']
        ovs = overlap_serial(pdl['recsn'], "LOGGERS", pdl['rectype'],
                             pdl['netsta'].split("-")[0])
        if ovs != sn0:
            pdl['recsn'] = ovs
            pcc2 = []
            for pc in pchans:

                if pc['recid_fk'] == sn0:
                    pc['recid_fk'] = ovs
                if 'recid' in pc:
                    if pc['recid'] == sn0:
                        pc['recid'] = ovs
                        pc['recid_fk'] = ovs
                        pc['netstarec'] = pc['netstarec'].replace(sn0, ovs)
                pcc2.append(pc)
            pchans = pcc2

    if rewrite and code in successfully_wrote:
        successfully_wrote.pop(code)
    if code not in successfully_wrote:
        my_site = Station(code, stations, pchans, psens, pdls,
                          # version=version, folder="xmls/stations")
                          version=version, folder=folder)
        if write:
            print("writing to: ", my_site.outputname)
            write_template(my_site.template, my_site.outputname)
            successfully_wrote[code] = True
            successfully_wrote.close()
        else:
            successfully_wrote.close()
            return my_site
    else:
        print(f"{code} should already be in xmls/stations!")
    successfully_wrote.close()


def senw(serial, sensors, channels, write=True, rewrite=True, folder="xmls/week2/tuesday/uploading_1"):
    # generates sensor xml from processed sensor and channel entries
    successfully_wrote = shelve.open("objs/shelves/finished_sensor_xmls")
    ovserial = overlap_serial(serial, "SENSORS", sensors[0]['sentype'],
                              sensors[0]['netstareccha'].split("-")[0])
    if ovserial != serial:
        for sen in sensors:
            sen['unitsn'] = ovserial
        for chan in channels:
            chan['chaid'] = ovserial
            chan['senid'] = ovserial

            chan['netstarec'].replace(serial, ovserial)
        serial = ovserial
    key = f"{sensors[0]['sentype']}_{serial}"
    if rewrite and key in successfully_wrote:
        successfully_wrote.pop(key)
    if key not in successfully_wrote:
        my_sensor = Sensor(serial, sensors, channels,
                           # version="10", folder="xmls/sensors")
                           version="10", folder=folder)

        if write:
            print("writing to", my_sensor.outputname)
            write_template(my_sensor.template, my_sensor.outputname)
            successfully_wrote[key] = True
        else:
            successfully_wrote.close()
            return my_sensor
    else:
        print("it should already be in xmls/sensors!")
    successfully_wrote.close()


def dlw(serial, dls, channels, version="10", write=True, rewrite=False,
        hwis=False, folder=None, src=None):
    # bare-bones code to generate xml from processed logger and channel entries
    successfully_wrote = shelve.open("objs/shelves/finished_logger_xmls")

    ovserial = overlap_serial(serial, "LOGGERS", dls[0]['rectype'],
                              dls[0]['netsta'].split("-")[0])
    if ovserial != serial:
        for dl in dls:
            dl['recsn'] = ovserial
        for chan in channels:
            chan['recid_fk'] = ovserial
            chan['recid'] = ovserial
        serial = ovserial
    key = f"{dls[0]['rectype']}_{serial}"
    if rewrite and key in successfully_wrote:
        successfully_wrote.pop(key)
    if key not in successfully_wrote:

        # folder = f"xmls/dataloggers"
        if serial in multiSNs and src != "multi_writer":
            multi_writer(sn=serial)
        else:
            mdl = Logger(serial, dls, channels,
                         version=version, folder=folder, hwis=hwis)
            if write:
                print("Writing to", mdl.outputname)
                successfully_wrote[key] = True
                try:
                    write_template(mdl.template, mdl.outputname)
                except Exception as e:
                    my_logger.warning(
                        f"could not write the xml for {key} logger, due to {e}")
            else:
                successfully_wrote.close()
                return mdl
    else:
        print("it should already be in xmls/dataloggers")
    successfully_wrote.close()


def ampw(code, amps, version="10", write=True, rewrite=False, folder=None):
    # writes amps/vcos
    successfully_wrote = shelve.open("objs/shelves/finished_amp_xmls")
    my_amp = None
    if rewrite and code in successfully_wrote:
        successfully_wrote.pop(code)
    if code not in successfully_wrote:
        my_amp = Amplifier(amps, version=version, folder=folder)
        if write:
            write_template(my_amp.template, my_amp.outputname)
            successfully_wrote[code] = True
    successfully_wrote.close()
    return my_amp


def writer(sn=None, mod=None, writesensors=False, writeloggers=False):
    # calls other write functions
    peq = openpickle("objs/processed_eq.obj")
    if writesensors:
        for key in peq['psens']:
            d = peq['psens'][key]
            serial = d['psens'][0]['unitsn']
            model = d['psens'][0]['sentype']
            write = False
            if not sn and not mod:
                write = True
            elif sn and not mod and serial == sn:
                write = True
            elif mod and not sn and mod == model:
                write = True
            elif sn and mod and serial == sn and mod == model:
                write = True

            if write:
                senw(serial, d['psens'], d['pchans'], write=True)
    if writeloggers:
        n = 0
        f = 0
        g = 0
        for key in peq['dls']:
            d = peq['dls'][key]
            if len(d['chans']) > 0 and len(d['dls']) > 0:

                n += 1
                serial = d['dls'][0]['recsn']

                model = d['dls'][0]['rectype']
                dlw(serial, d['dls'], d['chans'])
            else:
                if len(d['chans']) == 0:
                    f += 1
                if len(d['dls']) == 0:
                    g += 1


def multi_writer(sn=None, mod=None, version="11multi"):
    # writes multi-site dataloggers - not necessary any more? TBD
    mdls = openpickle("objs/msdls.obj")
    if sn and sn in mdls:
        dlw(sn, mdls[sn]['dls'], mdls[sn]['chans'],
            version=version, rewrite=True, hwis=True, src="multi_writer")
    else:
        write = True
        for key in mdls:
            model = mdls[key]['dls'][0]['rectype']
            if mod and mod != model:
                write = False
            if write:
                dlw(key.split("_")[0], mdls[key]['dls'], mdls[key]['chans'],
                    version=version, rewrite=True, hwis=True, src="multi_writer")

        # if not sn and not mod:
        #     write = True
        # elif sn and not mod and
        # dlw(key.split("_")[0], mdls[key]['dls'], mdls[key]['chans'],
        #     version="11multi", rewrite=True, hwis=True)


def write_msl():
    mlf()
    multi_writer()


def epoch_checker(prcd):
    startdates = {}
    corresponding_enddates = {}
    for site in prcd['sites']:
        for chan in prcd['sites'][site]['chans']:
            ns = ".".join(chan['netstarec'].split("-")[:2])
            ls = chan['netstarec'][-1]
            sc = f"{ns}_{chan['seedchan']}"
            if check_none(chan['lcode']):
                sc += f"_{chan['lcode']}"
            sd = chan['chadate_inst']
            ed = chan['chadate_rem']
            if not ed:
                ed = datetime(3000, 1, 1, 0, 0, 0)

            if sc not in startdates:
                startdates[sc] = []
                corresponding_enddates[sc] = {}
            startdates[sc].append([sd, ed, ls])

        for item in startdates:
            ssd = sorted(startdates[item])
            if len(ssd) > 1:
                for i in range(len(ssd) - 1):
                    if ssd[i][1] > ssd[i + 1][0]:
                        print(f"OVERLAP for {item}, "
                              f"enddate {ssd[i][1]} ({ssd[i][2]}) > "
                              f"startdate {ssd[i + 1][0]} ({ssd[i + 1][2]})")


if __name__ == "__main__":
    print("hi!")
    ## V combines fredsheet and NOD
    # combined = combiner(write=True)
    ## V groups channels and loggers with their sensor
    # get_sensor_channels(combined, write=True)
    # V processes sensors and channels
    # processed = processor_grouper(openpickle("objs/grouped_eq.obj"),
    #                               openpickle("objs/fs_nod_grouped.obj"),
    #                               write=True)

    # write_sensors(openpickle("objs/processed.obj"), rewrite=True)
    # write_loggers(openpickle("objs/processed.obj"), rewrite=True)
    # write_sites(openpickle("objs/processed.obj"), rewrite=True)
    # write_loggers(openpickle("objs/processed.obj"), rewrite=True, mod="Q330S+")
    # write_sites(openpickle("objs/processed.obj"), rewrite=True, sta="1216")
    # write_amps(openpickle("objs/fs_nod_grouped.obj"))

    # epoch_checker(openpickle("objs/processed.obj"))
    # write_msl()


"""
1. Find the email from Jacob that explains what needs to change
    a. Velocity sensors should have no input/output range
    b. HS-1 sensors should be able to be HS-1 VERTICAL or HS-1 HORIZONTAL, like L-4C's
    c. Wilcoxon 731A sensors should have only 3 components
        WIL_CSU1/CMW1/CGP1/CCH1 has X, Y, Z and VERTICAL, COMPONENT1, COMPONENT2
2. Email Prabha and ask to delete of all the stations that use equipment used at NC.CCOB
3. Delete all the equipment
4. Upload equipment WITH CHANGES! WHATEVER THEY MAY BE!
5. Email Jacob about those
6. Re-upload stations that use them

Okay, 
"""

