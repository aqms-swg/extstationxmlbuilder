from parsers.create_nod_dic import Scrape
from parsers.parse_FredSheet import fs_to_dict, fs_nod_format
from parsers.parse_NOD import Tweaks
import shelve

# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def build_nod_dic(user, password, output="objs/NOD_raw_dic.obj",
                  dbname="nod_db", host="localhost"):
    # parsers NOD sql database, writes a dictionary and saves it as a pickle file
    scraped_db = Scrape(user=user, password=password, dbname=dbname, host=host)
    scraped_db.pickle_engine(outputfile=output)


def fix_nod_dates(infile="objs/NOD_raw_dic.obj", outfile="objs/NOD_dic.obj"):
    from functions import openpickle
    nod_raw = openpickle(infile)
    tweaker = Tweaks(infile)
    trimmed = tweaker.trimmer()
    # print(len(trimmed['stations']))
    tweaker.datefixer(trimmed, outfile=outfile)


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print("hi!")
    # print_hi('PyCharm')
    # fs_to_dict()
    # fs_nod_format()
    fix_nod_dates()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/

# okay, steps
"""
1. Get fs/nod in dicts
2. Now I have all channels, sensors, loggers, amplifiers and stations
3. Here, I can translate the equipment names and fromat the dates
4. Get sensors, loggers, amplifiers and stations grouped together with the relevant pieces
    I'm going to want to say "write this station" or "write this piece of equipment - serial, model"
    
5. Will also need the multi-site loggers as a separate thing

so NOD is already pretty much as I want it. 
I do need to have the script to make it from the sql

"""