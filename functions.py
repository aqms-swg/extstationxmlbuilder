import pickle
import os
from datetime import datetime, date
import sisxmlparser3_0 as sisxmlparser
from logger_setup import my_logger as myl
my_logger = myl()


def parsedate_fs(datestring):
    # converts a date like "10/01/1993 16:35" into a datetime object
    if "3000" in datestring:
        return None
    else:
        return datetime.strptime(datestring, '%m/%d/%Y %H:%M')


networks = {
    # networks that will be uploaded for
    "NC": {
        "starttime": parsedate_fs("01/01/1926 07:00"),  # "1926-01-01T07:00:00+00:00", #1967-01-01 00:00:00
        "description": "USGS Northern California Regional Network",
        # "totalstations": 338,
    },
    "NP": {
        "starttime": parsedate_fs("01/01/1931 07:00"),  # "1931-01-01 00:00:00",
        "description": "United States National Strong-Motion Network",
        # "totalstations": 32,  # need to get a NP station sample...
    },
    "CI": {
        "starttime":  parsedate_fs("01/01/1926 00:00"),
        "description": "Southern California Seismic Network",
        # "totalstations": 2000, # just guessing
    },
    "WR": {
        "starttime": parsedate_fs("05/12/1963 00:00"),
        "description": "California Department of Water Resources",
    }
}


def openpickle(filename):
    # opens a pickled file and returns the contents
    with open(filename, "rb") as f:
        pfile = pickle.load(f)
    return pfile


def check_none(isitnone):
    # checks if a value is empty in FS/NOD nomenclatures
    if isitnone in [None, 'none', 'None', '-', '--', '', ' ', "UNKN", "null", "NONE"]:
        return None
    else:
        return isitnone


def parsedate(datestring):
    # converts a date object (Year-Month-Day) to a datetime object
    if type(datestring) == date:
        dtc = datetime.combine(datestring, datetime.min.time())

    elif type(datestring) == datetime:
        dtc = datestring
    else:
        return None
    year = dtc.year
    if year > 2021:
        dtc = dtc.replace(year=dtc.year - 100)
    return dtc


def root(network, hwr=None, stations=[]): # need to get network from ml
    # builds the root of a template
    selectedstations = None
    if len(stations) != 0:
        selectedstations = len(stations)
    template = sisxmlparser.SISRootType(
        Source="ISTI NOD Parser",
        Sender="ISTI NOD Parser",
        Created=datetime.utcnow(),
        Network=[sisxmlparser.SISNetworkType(
            code=network,  # get this from netsta
            startDate=networks[network]['starttime'],  # have a dict that shows for NC and NP
            Description=networks[network]['description'],  # again from dict
            # TotalNumberStations=networks[network]['totalstations'],
            SelectedNumberStations=selectedstations,
            Station=stations,
        )],
        HardwareResponse=hwr,
    )
    return template


def component_label(compname):
    # returns SIS-accepted component labels
    if type(compname) == str and len(compname) > 0:
        if compname[-1] in ["X", "Y", "Z"]:
            return compname[-1]
        elif compname[-1] == "E":
            return "X"
        elif compname[-1] == "N":
            return "Y"
        elif compname in ["HN1", "HH1"]:
            return "VERTICAL"
        elif compname[-1] == "D":
            return "COMPONENT1"
        else:
            if compname[-1] in ['0', 'O']:
                return "COMPONENT1"
            else:
                return f"COMPONENT{compname[-1]}"
        # O just O not COMPONENTO
    else:
        return None


def parse_operator(operator):
    # gets proper operator names from those found in the FredSheet and NOD
    if operator.strip() in ["GSM", "NC", "LLL"]:
        return "NCSN"
    elif operator.strip() in ["NP"]:
        return "NSMP"
    elif operator.strip() in ["WR"]:
        return "CDWR"
    elif operator.strip() in ["CI"]:
        return "SCSN-CA"
    else:
        return operator.strip()


def sensitivity(sensunits, inputunits=None):
    # gets proper input/output units and descriptions from unit labels
    if not sensunits:
        return [None, None], [None, None]
    elif "/" in sensunits:
        sul0 = sensunits.split("/")
        sul = [sul0[0], "/".join(sul0[1:]).strip()]
    elif "p" in sensunits:
        sul = sensunits.split("p")
    elif "P" in sensunits:
        sul = sensunits.split("P")
    elif sensunits == "U":
        # sul = ["count", "m/s"]
        sul = ["number", "number"]
        if inputunits:
            sul[0] = inputunits
            sul[1] = inputunits
    else:
        my_logger.warning(f"I don't know what units go with {sensunits}")
        return [None, None], [None, None]
    descriptions = []
    for unit in sul:
        if unit.lower() == "v":
            descriptions.append("Voltage in Volts")
        elif unit.lower() == "mv":
            descriptions.append("Voltage in MilliVolts")
        elif unit.lower() == "number":
            descriptions.append("Number of occurrences")
        elif unit.lower() == "cm":
            descriptions.append("Displacement in Centimeters")
        elif unit.lower() in ["c", "counts"]:
            sul[sul.index(unit)] = "count"
            descriptions.append("Digital counts")
        elif unit.lower() == "m/s":
            descriptions.append("Velocity in meters per second")
        elif unit.lower() in ["m/s/s", "m/s**2"]:
            sul[sul.index(unit)] = "m/s**2"
            descriptions.append("Acceleration in meters per second squared")
        elif unit.lower() == "g":
            sul[sul.index(unit)] = "G-mks"
            descriptions.append("Acceleration in Gravitational Acceleration 9.80665 m/s^2")
        elif unit.lower() == "microstrain":
            sul[sul.index(unit)] = "microstrain"
            descriptions.append("Strain in Micrometer/micrometer")
        elif unit.lower() in ["millibar", "mbar", "mb", "millibars"]:
            sul[sul.index(unit)] = "millibar"
            descriptions.append("Pressure in Millibars")
        elif unit.lower() == "rad/s":
            descriptions.append("Angular Frequency in Radians per second")
        else:
            my_logger.warning(f"didn't append anything for {unit}")
            descriptions.append("")

    return sul, descriptions




def first_last_date(channels):
    if not channels:
        return None, None, None
    insts = []
    rems = []
    for chan in channels:
        insts.append(chan['chadate_inst'])
        rems.append(chan['chadate_rem'])
    if None in insts:
        mininst = datetime(1966, 1, 1, 0, 0, 0)
    else:
        mininst = min(insts)
    if None in rems:
        maxrem = None
    else:
        maxrem = max(rems)
    # if maxrem and maxrem != "Active":
    #     status = "RETIRED"
    # else:
    status = "FUNCTIONAL"
    return mininst, maxrem, status


def get_nrl_gain(nrlurl, logger=None):
    import requests
    r = requests.get(nrlurl)
    content = r.content
    decoded = bytes.decode(content).split("\n")
    for line in decoded:
        if "Sensitivity" in line:
            sens = line.split()[-1]
    try:
        fsens = float(sens)
        return fsens
    except Exception as e:
        # logger.warning(f"couldn't convert {sens} to a float. {e}")
        return None


# def g_convert_old(units, descriptions, value):
#     # converts gs to m/s**2 in units (and related values)
#     for unit in units:
#         idx = units.index(unit)
#         print("idx = ", idx)
#         denom = int(idx == 0) - int(len(units) == 2)
#         print(denom)
#         if unit == "G-mks":
#             units[idx] = "m/s**2"
#             descriptions[idx] = "Acceleration in Meters per Second per Second"
#             value *= 9.80665**(-1.0**int(idx == 0)) # converts to 1/g
#             # if the unit is on the denominator
#         if unit == "g":
#             units[idx] = "m/s**2"
#             descriptions[idx] = "Acceleration in Meters per Second per Second"
#             value *= 9.80665 ** (-1.0 ** int(idx == 0))
#     return units, descriptions, value

def g_convert(unit, description, value):
    # converts gs to m/s**2 in units (and related values)
    if unit == "G-mks":
        unit = "m/s**2"
        description = "Acceleration in Meters per Second per Second"
        value *= 9.80665 # converts to 1/g
        # if the unit is on the denominator
    if unit == "g":
        unit = "m/s**2"
        description = "Acceleration in Meters per Second per Second"
        value *= 9.80665
    return unit, description, value


def senchans(sensors, channels):
    # links sensors with channels
    scs = {}
    for sensor in sensors:
        scs[sensor['senid']] = []
        for chan in channels:
            if chan['chaid'] == sensor['chaid_fk']:
                scs[sensor['senid']].append(chan['chaid'])
    return scs


def convert2serialize(obj):
    if isinstance(obj, dict):
        return { k: convert2serialize(v) for k, v in obj.items() }
    elif hasattr(obj, "_ast"):
        return convert2serialize(obj._ast())
    elif not isinstance(obj, str) and hasattr(obj, "__iter__"):
        return [ convert2serialize(v) for v in obj ]
    elif hasattr(obj, "__dict__"):
        return {
            k: convert2serialize(v)
            for k, v in obj.__dict__.items()
            if not callable(v) and not k.startswith('_')
        }
    else:
        return obj


def l4c_rdg(sentype):
    rdg = None
    rdl = None
    if "L-4C" in sentype:
        parsed = sisxmlparser.parse("templates/L-4C_VERTICAL.xml")
        rdg = parsed.HardwareResponse.ResponseDictGroup
        rdl = parsed.HardwareResponse.Hardware.Sensor[0].Component[0].Calibration[0].Response.ResponseDictLink
    return rdg, rdl


def write_template(filled_template, outputfile):
    # creates an xml file from a template (sisxmlparser object)
    ofs = outputfile.split("/")[:-1]  # folder path pieces
    mydir = "/".join(ofs)
    if len(ofs) > 0:
        if not os.path.exists(mydir):
            os.makedirs(mydir)
    my_logger.info(f"writing to {outputfile}")
    with open(outputfile, "w") as f:
        filled_template.exportxml(f)


insens = {
    "731": "731A",
    "ARS-14": "ARS-14",
    "STD": "C&GS STANDARD",
    "CMG": "CMG-3ESPC",
    "DCA-300": "DCA-300",
    "DSA102": "DSA-102",
    "ES-DECK": "EPISENSOR ES-DECK",
    "ES-U2": "EPISENSOR ES-U2",
    "ES-U": "EPISENSOR ES-U",
    "ES-T": "EPISENSOR ES-T",
    "FBA": "FBA GENERIC",
    "GEOSIG": "GEOSIG-AC-63",
    "204": "MODEL 204",
    "131A": "RT131A-02/1",
    "130-02I": "RT131A-02/3/INT",  # No 130 sensors in SIS
    "147": "RT147-01",
    "6000": "S-6000",
    "SH-1": "SH-1",
    "SSA": "SSA2",
    "TITAN": "TITAN",
    "120P": "TRILLIUM 120P",
    "COMPACT": "TRILLIUM COMPACT",
    "TSA100": "TSA-100S",
    "VELOCITY": "UNITY VELOCITY SENSOR"

}
equalsens = {
    "DM": "C&GS DISPLACEMENT METER",
    "L22": "L-22D 3D",
    "L22-3D": "L-22D 3D",
    "PIEZO": "PIEZOMETER",
}
indls = {
    "72A-07": "72A-07",
    "130": "130-01",
    "DM": "C&GS DISPLACEMENT METER",
    "STD": "C&GS STANDARD",
    "DCA-300": "DCA-300",
    "NATIONAL": "EARTHWORM NI",
    "ETNA2": "ETNA-2",
    "GRANITE": "GRANITE",
    "GSR-IA18": "GSR-IA18-NETQUAKE",
    "HRD": "HRD-24",
    "K2": "K2",
    "LYNX": "LYNX",
    "WHIT": "MT. WHITNEY",
    "DST": "NCSN-DST",
    "Q330S+": "Q330S+",
    "4128": "Q4120",
    "SCOPE": "C&GS SEISMOSCOPE",
    "ALARM": "SEISMIC ALARM",
    "SSA2": "SSA-2",
    "TAURUS": "TAURUS",
    "TITAN": "TITAN SMA",
    "TRIDENT": "TRIDENT",
    "TUSTIN": "CUSP-TUSTIN",
}
equaldls = {}


def convert_types(eqmodel, orientation="VERTICAL",
                  eqtype="sen", depth=None, chaname=None, lchans=1):
    # formats equipment names for SIS
    # print("What type of equipment is", eqmodel, eqtype)
    eqmodel = eqmodel.strip().upper()
    if eqmodel[0:3] == "ES-":
        eqmodel = "EPISENSOR " + eqmodel
    if eqtype == "sen":
        if eqmodel in [  # should this go first? when should es-u2 be used?
            "731A",
            "ARS-14",
            "C&GS STANDARD",
            "C&GS DISPLACEMENT METER",
            "CMG-3ESPC",
            "CMG-40T",
            "DCA-300",
            "DCA-333",
            "DILATOMETER",
            "DSA-102",
            "DSA-302M",
            "EPISENSOR ES-DECK",
            "EPISENSOR GENERIC",
            "EPISENSOR ES-T",
            "EPISENSOR ES-U",
            "EPISENSOR ES-U2",
            "ETNA",
            "FBA-1",
            "FBA-3",
            "FBA-11",
            "FBA-13",
            "FBA-13DH",
            "FBA-23",
            "FBA-23DH",
            "FBA-DECK",
            "FBA GENERIC",
            "GEOSIG-AC-63",
            "L-22D 3D",
            "MBB-1",
            "MBB-2",
            "MO-2",
            "MODEL 204",
            "PIEZOMETER",
            "PRESSURE",
            "R2",
            "RFT-250",
            "RFT-350",
            "RT147-01",
            "RT131A-02/1",
            "RT131A-02/3/INT",
            "S-6000",
            "SA-101",
            "SA-102",
            "SH-1",
            "SMA",
            "SMA-1",
            "SS-202",
            "SSA2",
            "STS-2",
            "TITAN",
            "TRILLIUM 120P",
            "TRILLIUM COMPACT",
            "TSA-100S",
            "UNITY VELOCITY SENSOR"
        ]:
            return eqmodel

        elif "L4" in eqmodel.replace("-", ""):
            return f"L-4C {orientation.upper()}"
        elif "STS-1" in eqmodel:
            if chaname:
                return f"STS-1 {orientation.upper()}"
        elif "GEOSPACE" in eqmodel:
            return f"HS-1-LT {orientation.upper()}"
        else:
            for k in equalsens:
                if k == eqmodel:
                    return equalsens[k]
            for k in insens:
                if k in eqmodel:
                    return insens[k]
        if "EPI" in eqmodel:
            if depth:
                if float(depth) == 0.0:
                    return "EPISENSOR ES-T"
                else:
                    return "EPISENSOR ES-DH"
            else:
                return "EPISENSOR GENERIC"
        print("Couldn't find a sensor type for", eqmodel)

    elif eqtype == "dl":
        if eqmodel in [
            "130-01",
            "130-ANSS",
            "130-SMHR",
            "72A-07",
            "AR-240",
            "BASALT-4X",
            "BASALT-8X",
            "BASALT-INTERNAL EPI",
            "C&GS DISPLACEMENT METER",
            "C&GS SEISMOSCOPE",
            "C&GS STANDARD",
            "CENTAUR",
            "CRA-1",
            "CUSP-TUSTIN",
            "DCA-300",
            "DCA-333",
            "DSA-1",
            "DSA-3",
            "EARTHWORM NI",
            "ETNA",
            "ETNA-2",
            "GEOS",
            "GRANITE",
            "GSR-IA18-NETQUAKE",
            "HRD-24",
            "K2",
            "LYNX",
            "MO-2",
            "MT. WHITNEY",
            "NCSN-DST",
            "OBSIDIAN",
            "OBSIDIAN-4X",
            "OBSIDIAN-8X",
            "OBSIDIAN-12X",
            "OBSIDIAN-24X",
            "Q330",
            "Q330S+",
            "Q4120",
            "RFT-250",
            "RFT-350",
            "SEISMIC ALARM",
            "SMA-1",
            "SSA-1",
            "SSA-2",
            "TAURUS",
            "TITAN SMA",
            "TRIDENT",
            "TSA-SMA"
        ]:
            return eqmodel
        if "BASALT" in eqmodel:
            if lchans >= 4:
                return "BASALT-8X"
            else:
                return "BASALT-4X"

        else:
            for key in indls:
                if key in eqmodel:
                    return indls[key]
            if check_none(eqmodel):
                print("Couldn't find a logger type for", eqmodel)


#         if "STD" in eqmodel:
#             return "C&GS STANDARD"
#         elif eqmodel == "DM":
#             return "C&GS DISPLACEMENT METER"
#         elif "SCOPE" in eqmodel:
#             return "C&GS SEISMOSCOPE"
#         elif eqmodel in [
#             "DCA-300", "DCA-333", "DSA-1", "DSA-3",
#         ]:
#             return eqmodel
#
#
#
#     elif eqmodel == "EXTEN":
#         return "EXTENSOMETER"
#
#
#
#
#
#     elif eqmodel == "EPI":
#         # return "EPISENSOR ES-DECK"
#         return "EPISENSOR GENERIC"
#
#
#
#
#     elif "GRANITE" in eqmodel:
#         return "GRANITE"
#     elif "WHIT" in eqmodel:
#         return "MT. WHITNEY"
#
#     elif "DM" in eqmodel:
#         return "C&GS DISPLACEMENT METER"
#
#     elif "4128" in eqmodel:
#         return "Q4120"
#     elif "ALARM" in eqmodel:
#         return "SEISMIC ALARM"
#     elif "TITAN" in eqmodel:
#         if eqtype == "sen":
#             return "TITAN"
#         else:
#             return "TITAN SMA"
#
#     elif "J3X" in eqmodel:
#         return "J3X"
#
#     elif "SV-1" in eqmodel:
#         return "SV-1"
#
#
#
#     elif eqmodel.upper().strip() in [ # equipment types that exist in SIS
#
#
#          "L-4C VERTICAL", "L-4C HORIZONTAL",
#
#
#
#         "SMA",
#         "DSA102",  "Q4128",
#
#         "SA-101", "SA-102"
#         ]:
#         # lots of these are loggers.......
#         return eqmodel.upper().strip()
#
#     else:
#         sen = get_sensor(eqmodel, depth=depth, orientation=orientation, chaname=chaname)
#         if sen:
#             return sen
#         log = get_logger(eqmodel)
#         if log:
#             return log
#         else:
#             my_logger.info(f"UNKNOWN SENSOR TYPE NOT IN SIS: {eqmodel}")
#             # return sensortype.upper()
#             return None
#
#
# def get_sensor(sensorname, depth="0", chaname=None, orientation="VERTICAL"):
#     # used for getting proper sensor names from the FredSheet
#     sl = sensorname.lower()
#     smod = None
#     dep = float(depth)
#     if "episensor" in sensorname.lower():
#         if dep == 0.0:
#             smod = "EPISENSOR ES-T"
#         else:
#             smod = "EPISENSOR ES-DH"
#     elif "l4c" in sensorname.replace("-", "").lower():
#         smod = f"L-4C {orientation.upper()}"  # should it always be vertical?
#     elif "rt-130" in sl:
#         if "02" not in sl:
#             smod = "130-01"
#         else:
#             smod = "RT131A-02/1"
#     elif "trillium-compact" in sl:
#         smod = "TRILLIUM COMPACT"
#     elif "trillium" in sl and "120p" in sl:
#         smod = "TRILLIUM 120P"
#     elif "geosig" and "ac" and "63" in sl:
#         smod = "GEOSIG-AC-63"
#     elif "fba" and "23" in sl:
#         smod = "FBA-23"
#     elif "fba" and "11" in sl:
#         smod = "FBA-11"
#
#
#     elif "guralp" in sl:
#         if "cmg40t-1" in sl:
#             smod = "CMG-40T"
#         elif "espc" in sl:
#             smod = "CMG-3ESPC"
#     elif "sts-2" in sl:
#         smod = "STS-2"
#     elif "titan" in sl:
#         # smod = "TITAN SMA"
#         smod = "TITAN INTERNAL"
#     elif "sma" in sl:
#         smod = "SMA"
#     elif "ars-14" in sl:
#         smod = "ARS-14"
#     elif "ranger" in sl:
#         smod = "RANGER-SS-1"
#     elif "sts-1" in sl:
#         # if chaname:
#         #     if "Z" in chaname: # can this use orientation instead?
#         #         smod = "STS-1 VERTICAL"
#         #     else:
#         #         smod = "STS-1 HORIZONTAL"
#         if orientation == "VERTICAL":
#             smod = "STS-1 VERTICAL"
#         else:
#             smod = "STS-1 HORIZONTAL"
#     elif "geospace" in sl:
#         if orientation == "VERTICAL":
#             smod = "HS-1-LT VERTICAL"
#         else:
#             smod = "HS-1-LT HORIZONTAL"
#     elif "l" in sl and "4" in sl and "c" in sl:
#         if orientation == "VERTICAL":
#             smod = "L-4C VERTICAL"
#         elif orientation == "HORIZONTAL":
#             smod = "L-4C HORIZONTAL"
#     else:
#         # my_logger.info(f"UNKNOWN SENSOR TYPE NOT IN SIS: {sensorname}")
#         # smod = sensorname.strip()
#         return None
#     return smod.upper()
#
#
# def get_logger(loggername, chans=[], lchans=None):
#     # for getting proper logger names from fredsheet
#     ll = loggername.lower()
#     if not lchans:
#         lchans = len(chans)
#     lmod = None
#     if "basalt" in ll:
#         if lchans >= 4:
#             lmod = "BASALT-8X"
#         else:
#             lmod = "BASALT-4X"
#     elif "k2" in ll:
#         lmod = "K2"
#     elif "reftek" in ll:
#         if "130-01" in ll:
#             lmod = "130-01"
#         elif "130-anss" in ll:
#             lmod = "130-ANSS"
#         elif "130-smhr" in ll:
#             lmod = "130-SMHR"
#     elif "tustin-2" in ll:
#         lmod = "CUSP-TUSTIN-2"
#     elif "tustin" in ll:
#         lmod = "CUSP-TUSTIN"
#     elif "etna" in ll:
#         if "2" in ll:
#             lmod = "ETNA-2"
#         else:
#             lmod = "ETNA"
#     elif "gsr" and "ia18" in ll:
#         lmod = "GSR-IA18-NETQUAKE"  # or with a + appended? 2 models - ask
#     elif "obsidian" in ll:
#         if lchans >= 4:
#             lmod = "OBSIDIAN-8X"
#         else:
#             lmod = "OBSIDIAN-4X"
#     elif "trident" in ll:
#         lmod = "TRIDENT"  # or trident 30s?
#         # we have trident and trident-multi inputs
#     elif "hrd" in ll and "24" in ll:
#         lmod = "HRD-24"
#     elif "national" in ll:
#         lmod = "EARTHWORM NI"
#     elif "330s+" in ll:
#         lmod = "Q330S+"
#     elif "330" in ll:
#         lmod = "Q330"
#     elif "q980" in ll:
#         lmod = "Q980"
#     elif "taurus" in ll:
#         lmod = "TAURUS"
#     elif ll.strip() in ["none", "-"]:
#         lmod = None
#     elif "ssa2" in ll:
#         lmod = "SSA-2"
#     elif "dst" in ll:
#         lmod = "NCSN-DST"
#     elif "centaur" in ll:
#         lmod = "CENTAUR"
#     elif "lynx" in ll:
#         lmod = "LYNX"
#     else:
#         my_logger.info(f"UNKNOWN LOGGER TYPE NOT expected FS list: {ll}")
#         lmod = loggername
#     if type(lmod) == str:
#         return lmod.upper()
#     else:
#         return None


def overlap_serial(sn, eqtype, eqmod, net):
    ld = os.listdir("objs")
    if "overlaps.obj" in ld:
        overlaps = openpickle("objs/overlaps.obj")
    else:
        overlaps = {"SENSORS": [], "LOGGERS": []}
    if eqmod in overlaps[eqtype]:
        if sn in overlaps[eqtype][eqmod]:
            return f"{sn}_{net}"
    return sn


def picklewriter(writefile, to_write):
    if os.path.exists(writefile):
        os.remove(writefile)
    my_logger.info(f"writing to {writefile}")
    print(f"writing to {writefile}")
    fh = open(writefile, "wb")
    pickle.dump(to_write, fh)
    fh.close()


def qu(model, category):
    from sis_api.sis_api import query
    sis = query.Connect(db='sistest', config='credentials/sis_credentials.ini')
    matches = sis.request('equipment',
                          {'category': category, 'page[size]': 200, 'modelname': model},
                          format_out='pandas')
    return matches


def compare_api_results(model, category):
    # checks to see if anything matching the model/category exists in SIS
    # under a different operator
    sisapiobj = qu(model, category)
    n = 0
    insis = {}
    insisproj = {}
    for l in sisapiobj['attributes.equipepochs']:
        for ee in l:
            insis[sisapiobj['attributes.serialnumber'][n]] = ee['operatorcode']
            insisproj[sisapiobj['attributes.serialnumber'][n]] = ee['project']
        n += 1

    op = openpickle("objs/processed.obj")
    overlaps = {}
    if category == "SENSOR":
        myobj = op['sens']
    elif category == "LOGGER":
        myobj = op['dls']
    for item in myobj:
        isp = item.split("_")
        mod = isp[0]
        serial = isp[1]
        if mod == model:
            if serial in insis:
                if insis[serial] not in ["NSMP", "NCSN"]:
                    # print(f"{serial}, {insis[serial]}")
                    overlaps[serial] = insis[serial]
                elif insisproj[serial] not in [""]:
                    overlaps[serial] = insis[serial]
    return overlaps