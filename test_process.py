from functions import openpickle
from processing.processor import input_processor
import sys

def test_process(netsta):
    nss = netsta.split("-")
    vg = openpickle("objs/fs_nod_grouped.obj")
    # vg = openpickle("objs/fs_grouped.obj")
    gpd = openpickle("objs/grouped_eq.obj")
    # gpd = openpickle("objs/fs_grouped.obj")

    allsens = []
    allchans = []
    alldls = []
    for senkey in gpd:
        sens = gpd[senkey]['sens']
        chans = gpd[senkey]['chans']
        dls = gpd[senkey]['dls']
        for sen in sens:
            if nss == sen['netstareccha'].split("-")[:2]:
                allsens.append(sen)
        for chan in chans:
            if nss == chan['netstarec'].split("-")[:2]:
                allchans.append(chan)
        for dl in dls:
            if dl['netsta'] == netsta:
                if dl not in alldls:
                    alldls.append(dl)

    for dlsn in vg['recorders']:
        for dl in vg['recorders'][dlsn]:
            if netsta == dl['netsta']:
                if dl not in alldls:
                    alldls.append(dl)

    if len(allsens) > 0:
        print("Started with this many chans & sens & dls", len(allchans), len(allsens), len(alldls))
        nsrcdis = []
        pnsrcdis = []
        for chan in allchans:
            nsrcdi = (chan['netstarec'], chan['chadate_inst'])
            nsrcdis.append(nsrcdi)
        pchans, psens = input_processor(allchans, allsens, alldls)
        print("Got this many processed:", len(pchans), len(psens))
        for chan in pchans:
            pnsrcdi = (chan['netstarec'], chan['chadate_inst'])
            pnsrcdis.append(pnsrcdi)
        for entry in nsrcdis:
            if entry not in pnsrcdis:
                for chan in allchans:
                    if chan['netstarec'] == entry[0] and chan['chadate_inst'] == entry[1]:
                        print(chan['netstarec'], chan['chadate_inst'])


if __name__ == "__main__":
    test_process(sys.argv[1])
