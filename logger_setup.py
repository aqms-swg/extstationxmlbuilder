import logging
from logging import FileHandler, Formatter
from datetime import datetime


def my_logger(level=logging.WARNING, launcher=True):
# def my_logger(level=logging.DEBUG, launcher=True):
    # sets up the logger. Set launcher=True once per run, ideally through writexxml.py
    log_format = (
        "%(asctime)s [%(levelname)s]: %(message)s in %(pathname)s:%(lineno)d")
    logfile = datetime.now().strftime('./logs/%Y_%m_%d_loader.log')
    mylogger = logging.getLogger("ext_xml.building")
    if launcher:
        mylogger.setLevel(level)
        logger_fh = FileHandler(logfile)
        logger_fh.setLevel(level)
        logger_fh.setFormatter(Formatter(log_format))
        mylogger.addHandler(logger_fh)
    return mylogger
