from coalescer import combiner, get_sensor_channels, processor_grouper, \
    write_sensors, write_loggers, write_sites, write_amps, Coalesce, \
    multi_writer
from processing.multi_processor import multi_loggers
from processing.processor import skipsites
from parsers.create_nod_dic import Scrape
from parsers.parse_FredSheet import fs_to_dict, fs_nod_format
from parsers.parse_NOD import build_nod_dic, fix_nod_dates
from functions import openpickle, check_none, picklewriter, write_template, \
    compare_api_results
import argparse
import shelve
from datetime import datetime


# TODO: write all and prep need to properly handle multi-loggers
#  write all and write dls should skip them
#  make sure normal logger work as they should with modified build_loggers code

"""
A. prepare NOD:
1. turn db into dict with build_nod_dic
2. tweak it with fix_nod_dates (should rename)

B. prepare FS:
1. Turn into dict with fs_to_dict
2. Format like NOD with fs_nod_format

C. Group together pieces
1. combiner to combine FS + NOD
2. get_sensor_channels to put some peices together, to prepare for processing
3. processor_grouper to translate all the names, get NRL links, and put everything
in a nice pickled dict

D. Write! 
1. write_sensors and write_loggers (and write_amplifiers - need to add that!)
2. write_sites

NC: 2557 - 40961  = 38404 lines
NP: 41998 - 43519 = 1521 lines
"""


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-prep", nargs=1, type=str,
                        help="prepares a database for processing!"
                             "Possible arguments: nodsql, nod, fscsv, fs, "
                             "allnod, allfs, all")
    parser.add_argument("-process", nargs=1, type=str,
                        help="processes databases, after they have been "
                             "prepared. Possible arguments: all, combinedb, "
                             "groupeq, sis")
    parser.add_argument("-write", nargs=1, type=str,
                        help="writes extended stationxmls! Possible arguments:"
                             "site, sensor, logger, amp")
    parser.add_argument("-cw", action='store_true',
                        help="Writes the xmls for the CI and WR networks")
    parser.add_argument("--sn", nargs=1, type=str,
                        help="specifies the serial number for the xml "
                             "you want to build")
    parser.add_argument("--mod", nargs=1, type=str,
                        help="specifies the model for the xml you want to build")
    parser.add_argument("--src", nargs=1, type=str,
                        help="specifies the source of the xmls you want to build,"
                             "nod or fs")
    parser.add_argument("--user", nargs=1, type=str,
                        help="your NOD username")
    parser.add_argument("--pw", nargs=1, type=str,
                        help="your NOD password")
    parser.add_argument("--dbname", nargs=1, type=str,
                        help="the name of your NOD database - default nod_db")
    parser.add_argument("--reset", action='store_true',
                        help="Starts writing sites/equipment from scratch, "
                             "assuming you haven't written anything yet."
                             "By default, does not write sites/equipment that "
                             "have been written already")
    parser.add_argument("--host", nargs=1, type=str,
                        help="the host of your NOD database - default localhost")
    parser.add_argument("--start", nargs=1, type=int,
                        help="Start building sites with the nth newest")
    parser.add_argument("--stop", nargs=1, type=int,
                        help="Stop building sites with the nth newest")
    args = parser.parse_args()
    if args.prep or args.process or args.write or args.cw:
        return args
    else:
        parser.error("What do you want me to do? Prepare (-prep), "
                     "process (-process) or write (-write)?")


def nod_dic(args):
    if not args.user:
        raise Exception("You need a username to access your NOD sql database!")
    else:
        usr = args.user[0]
    if not args.pw:
        raise Exception("You need a password to access your NOD sql database!")
    else:
        pw = args.pw[0]
    if not args.dbname:
        print("You didn't include a database name for NOD! "
              "I'm assuming that it's nod_db")
        dbn = "nod_db"
    else:
        dbn = args.dbname[0]
    if not args.host:
        print("You didn't include a host for NOD! "
              "I'm assuming that it's localhost")
        host = "localhost"
    else:
        host = args.host[0]
    build_nod_dic(user=usr, password=pw, dbname=dbn, host=host)


def nod_tweaker():
    fix_nod_dates(infile="objs/NOD_raw_dic.obj", outfile="objs/NOD_dic.obj")


def fstd():
    fs_to_dict(source="objs/ncsn20211105.csv",
               validnetworks=["NC", "NP"], outputfile="objs/FS_raw_dic.obj")


def fsnf():
    fs_nod_format(fsrawd="objs/FS_raw_dic.obj", output="objs/FS_dic.obj")


def fscw():
    # step 0: collect the pieces
    fs_to_dict(validnetworks=["CI", "WR"], outputfile="objs/FS_CW_raw.obj")
    fs_nod_format(fsrawd="objs/FS_CW_raw.obj", output="objs/FS_CW.obj")

    # step 1: process
    co = Coalesce(fs="objs/FS_CW.obj", nod="objs/FS_CW.obj")
    send = co.sen_dict()
    recd = co.dl_dict()
    stad = co.sta_dict()
    ampd = co.amp_dict()
    grouped = {
        "stations": stad,
        "sensors": send,
        "recorders": recd,
        "amplifiers": ampd,
        "channels": co.combo['channels']
    }
    # gsc = get_sensor_channels(grouped, write=False)
    # cwprocessed = processor_grouper(gsc, grouped, write=True, outfile="objs/ciwr_processed.obj")
    #
    # ## step 2: write
    # # cwp = openpickle("objs/ciwr_processed.obj")
    # cwp = cwprocessed
    # sens = write_sensors(cwp, rewrite=True, writef=False)
    # dls = write_loggers(cwp, rewrite=True, writef=False)
    # sites = write_sites(cwp, rewrite=True, writef=False)
    # amps = write_amps(grouped, rewrite=True, writef=False)
    #
    # ciwr_pieces = {
    #     "sites": sites,
    #     "sens": sens,
    #     "dls": dls,
    #     "amps": amps
    # }
    # print("whoa, got everything, writing to my pickle file")
    # picklewriter("objs/ciwr_pieces.obj", ciwr_pieces)

    cp = openpickle("objs/ciwr_pieces.obj")
    for site in cp['sites']:
        eq = {
            "sens": [],
            "logs": [],
            "amps": [],
            "vcos": []
        }
        cst = cp['sites'][site].template
        for chan in cst.Network[0].Station[0].Channel:
            for sr in chan.Response.SubResponse:
                if hasattr(sr, 'EquipmentLink'):
                    sn = sr.EquipmentLink.SerialNumber
                    mod = sr.EquipmentLink.ModelName
                    cat = sr.EquipmentLink.Category
                    myl = None
                    if cat == "SENSOR":
                        myl = eq['sens']
                    elif cat == "LOGGERBOARD":
                        myl = eq['logs']
                    elif cat == "AMPLIFIER":
                        myl = eq['amps']
                    elif cat == "VCO":
                        myl = eq['vcos']
                    if myl is None:
                        print("what do I do with", cat)
                    else:
                        myl.append({"sn": sn, "mod": mod})
        for sen in cp['sens']:
            myhwr = cp['sens'][sen].template.HardwareResponse
            mysen = myhwr.Hardware.Sensor[0]
            sn = mysen.SerialNumber
            mod = mysen.ModelName
            smd = {"sn": sn, "mod": mod}
            if smd in eq['sens']:
                if not cst.HardwareResponse:
                    cst.HardwareResponse = myhwr
                else:
                    cst.HardwareResponse.Hardware.Sensor.append(mysen)
        for log in cp['dls']:
            myhwr = cp['dls'][log].template.HardwareResponse
            mydl = myhwr.Hardware.Logger[0]
            sn = mydl.SerialNumber
            mod = mydl.ModelName
            smd = {"sn": sn, "mod": mod}
            if smd in eq['sens']:
                if not cst.HardwareResponse.Logger:
                    cst.HardwareResponse.Logger = myhwr.Logger
                else:
                    cst.HardwareResponse.Hardware.Logger.append(mydl)

        for amp in cp['amps']:
            myhwr = cp['amps'][amp].template.HardwareResponse
            eqt = None
            myeq = None
            if hasattr(myhwr.Hardware, "Amplifier"):
                myeq = myhwr.Hardware.Amplifier[0]
                eqt = "amp"
            elif hasattr(myhwr.Hardware, "VCO"):
                myeq = myhwr.Hardware.VCO[0]
                eqt = "vco"
            else:
                print(dir(myhwr.Hardware))
            if eqt and myeq:
                sn = myeq.SerialNumber
                mod = myeq.ModelName
                smd = {"sn": sn, "mod": mod}
                if smd in eq['amps']:
                    print("adding!")

                        # needs to consider if it's amp or vco here?
                    if eqt == "amp":
                        if not hasattr(cst.HardwareResponse.Hardware, "Amplifier"):
                            cst.HardwareResponse.Hardware.Amplifier = [myeq]
                        else:
                            cst.HardwareResponse.Hardware.Amplifier.append(myeq)
                    elif eqt == "vco":
                        if not hasattr(cst.HardwareResponse.Hardware, "VCO"):
                            cst.HardwareResponse.Hardware.VCO = [myeq]
                        else:
                            cst.HardwareResponse.Hardware.VCO.append(myeq)

        write_template(cst, f"xmls/ciwr/full/{site}.xml")


def build_all_site_eq(site, processed, reset=False, eqfolder="xmls",
                      sitefolder="xmls",
                      grouped="objs/fs_nod_grouped.obj"):
    built_eq = shelve.open("objs/built")

    for eqt in ['sens', 'dls', 'amps']:
    #     if (eqt not in built_eq) or reset:
    #         built_eq[eqt] = []
        built_eq[eqt] = []

    # op = openpickle("objs/processed.obj")
    op = processed

    writtensites = []
    for sta in site['stas']:
        tag = f"{sta['netcode']}.{sta['stacode']}"
        if tag in skipsites:
            print("skipping {tag}, it's on the do-not-write list")
        elif tag not in writtensites:
            write_sites(op, rewrite=True, sta=sta['stacode'],
                        folder=sitefolder)  # net not included?
            writtensites.append(tag)

    writtenlogs = []
    for dl in site['dls']:
        tag = f"{dl['recsn']}_{dl['rectype']}"
        if tag not in writtenlogs and tag not in built_eq['dls']:
            writtenlogs.append(tag)
            dls = built_eq['dls']
            dls.append(tag)
            built_eq['dls'].append(dl)
            for net in ["NC", "NP"]:
                if f"_{net}" in dl['recsn']:
                    dl['recsn'].strip(f"_{net}")
            write_loggers(op, rewrite=True, sn=dl['recsn'], mod=dl['rectype'],
                          folder=eqfolder)

    writtensens = []
    for sen in site['sens']:
        tag = f"{sen['unitsn']}_{sen['sentype']}"
        if tag not in writtensens and tag not in built_eq['sens']:
            writtensens.append(tag)
            sens = built_eq['sens']
            sens.append(tag)
            built_eq['sens'].append(sen)
            for net in ["NC", "NP"]:
                if f"_{net}" in sen['unitsn']:
                    sen['unitsn'].strip(f"_{net}")
            write_sensors(op, rewrite=True, sn=sen['unitsn'], mod=sen['sentype'],
                          folder=eqfolder)

    writtenamps = []
    for amp in site['amps']:
        tag = amp['serial']
        if tag not in writtenamps and tag not in built_eq['amps']:
            writtenamps.append(tag)
            amps = built_eq['amps']
            amps.append(tag)
            built_eq['amps'].append(amp)
            write_amps(openpickle(grouped), sn=tag, rewrite=True, folder=eqfolder)

    built_eq.close()


def build_all_by_name(sn, op=openpickle("objs/processed.obj"),
                      grouped=openpickle("objs/fs_nod_grouped.obj")):
    if sn in op['sites']:
        build_all_site_eq(op['sites'][sn], op, reset=True)


def newest(startnew, endnew, processed, reset=False):
    lid = {}
    op = processed
    for site in op['sites']:
        last_inst = []
        last_rem = []
        for sta in op['sites'][site]['stas']:
            sdi = sta['stadate_inst']
            if not sta['stadate_rem']:
                sdr = datetime(3000, 1, 1, 0, 0, 0)
            else:
                sdr = sta['stadate_rem']

            last_inst.append(sdi)
            last_rem.append(sdr)
        lid[site] = [max(last_rem), min(last_inst)]

    n = 0
    for site in sorted(lid, key=lambda k: lid[k][1], reverse=True):
        if startnew <= n < endnew:
            print(f"Writing for {site}, which was first installed at {lid[site][1]}")
            # week = 5
            # day = "fri2"
            build_all_site_eq(op['sites'][site], reset=reset,
                              # eqfolder=f"xmls/week{week}/{day}/uploading_1",
                              eqfolder=f"xmls/mass_equipment",
                              # sitefolder=f"xmls/week{week}/{day}/uploading_2"
                              sitefolder=f"xmls/mass_sites"
                              )
        n += 1


def launcher(args):
    if args.prep:
        if "all" in args.prep:
            build_nod_dic(user="tim", password="Sql_password1")
            nod_tweaker()
            fstd()
            fsnf()
        elif "nosql" in args.prep:
            nod_dic(args)
        elif "nod" in args.prep:
            # nod_dic(args)
            build_nod_dic(user="tim", password="Sql_password1")
            nod_tweaker()
        elif "fs" in args.prep:
            fstd()
            fsnf()

    if args.process:
        if "all" in args.process:
            # commented lines are for faster usage, with step skipping
            fs_nod_combo = combiner(write=True)
            # fs_nod_combo = openpickle("objs/fs_nod_grouped.obj")
            get_sensor_channels(fs_nod_combo, write=True)
            # compare_equipment() # leave this commented, needs to talk with SIS(test)
            # checks to see what equipment already exists in sistest
            # multi_loggers() # builds multi-site loggers - warning: slow!
            processor_grouper(openpickle("objs/grouped_eq.obj"),
                              fs_nod_combo,
                              write=True)
            # processor_grouper(openpickle("objs/grouped_eq.obj"),
            #                   openpickle("objs/fs_nod_grouped.obj"),
            #                   write=True)
        if "fs" in args.process:
            just_fs_combo = combiner(write=True, justfs=True, output="objs/fs_combined.obj")

            get_sensor_channels(just_fs_combo, write=True,
                                output="objs/fs_grouped.obj")
            multi_loggers() # builds multi-site loggers - warning: slow!

            processor_grouper(openpickle("objs/fs_grouped.obj"),
                              openpickle("objs/fs_combined.obj"),
                              write=True, outfile="objs/fs_processed.obj")
        if "msl" in args.process:
            multi_loggers()

        if "nod" in args.process:
            just_nod = combiner(write=False, fs=None)
            get_sensor_channels(just_nod, write=True,
                                output="objs/nod_grouped.obj")
            processor_grouper(openpickle("objs/nod_grouped.obj"), just_nod,
                              write=True, outfile="objs/nod_processed.obj")
            # NOTE: by default, -process fs and -process nod ovewrite each other
            # change outfile to give them separate outputs if you need both
            # and call write with openpickle(<my custom processed file>)

    if args.write:
        print("args = ", args.sn, "sn" in args, args)

        sn = None
        mod = None
        if args.src:
            src = args.src[0]
        else:
            src = "both"
        if src == "fs":
            processed = openpickle("objs/fs_processed.obj")
            grouped = openpickle("objs/fs_combined.obj")
        else:
            src = None
            processed = openpickle("objs/processed.obj")
            grouped = openpickle("objs/fs_nod_grouped.obj")
        if check_none(args.sn):
            sn = args.sn[0]
        if check_none(args.mod):
            mod = args.mod[0]
        if check_none(args.src):
            src = args.src[0]
        if "site" in args.write:
            write_sites(processed, rewrite=True, sta=sn,
                        src=src)
        if "sensor" in args.write:
            write_sensors(processed, rewrite=True,
                          sn=sn, mod=mod, src=src)
        if "logger" in args.write:
            write_loggers(processed, rewrite=True,
                          sn=sn, mod=mod, src=src)
        if "amp" in args.write:
            write_amps(openpickle("objs/fs_nod_grouped.obj"), writef=True, rewrite=True,
                       sn=sn, mod=mod)
        if "msl" in args.write:
            multi_writer(sn=sn, mod=mod)
        if "all" in args.write:
            build_all_by_name(sn, processed, grouped=grouped)
        if "newest" in args.write:
            newest(args.start[0], args.stop[0], processed, reset=args.reset)

    if args.cw:
        fscw()


def main():
    args = parse_args()
    launcher(args)


def compare_equipment(processed="objs/processed.obj"):
    op = openpickle(processed)
    overlaps = {"SENSORS": {}, "LOGGERS": {}}
    for key in op['sens']:
        myk = key.split("_")[0]
        if myk not in overlaps['SENSORS']:
            try:
                ois = compare_api_results(myk, "SENSOR")
                overlaps["SENSORS"][myk] = ois
            except Exception as e:
                print(e)
                overlaps['SENSORS'][myk] = []
    for key in op['dls']:
        myk = key.split("_")[0]
        if myk not in overlaps['LOGGERS']:
            try:
                ois = compare_api_results(myk, "LOGGER")
                overlaps["LOGGERS"][myk] = ois
            except Exception as e:
                print(e)
                overlaps['LOGGERS'][myk] = []

    picklewriter("objs/overlaps.obj", overlaps)




if __name__ == "__main__":
    main()
    # compare_equipment()

# TODO: put script to write 100 sites (allow to flag for source from FS)
#  load ACTIVE SITES first
# TODO: add to siswriter functions, make nicely usable, add to readme
"""

You need to delete the site contents first. 
On the site summary page > Site epochs > Click on site name 
> delete site epoch (if there are many) , or else delete contents. 
When the site is down to one site epoch with no channels or equipment 
then on the summary page you will see the 'Delete site' link. 
Sorry for this long procedure. This is somewhat intentionally cumbersome 
so a user does not inadvertently delete a site that has stuff in it

Prabha

"""

# TODO: make sure that write newest works <<<<<<<<<<<<<<<
# TODO: readme should properly explain it