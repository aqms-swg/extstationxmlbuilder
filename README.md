An updated and tidier version of extstationxmlbuilder

### INSTALLATION
Anaconda is the best tool for preparing a virtual environment to run the code. 
You can create it as follows:
> conda env create -f environment.yml
> 
> conda activate sisxml

* objs directory contains FredSheet and pickled NOD dict
  * shelves directory contains files used for expidating processing with the shelve module
* parsers directory contains functions for:
  * creating pickled dictionary from NOD sql
  * creating pickled dictionary from FS csv
  * reformatting information from FS to match NOD format for stations, sensors, loggers, amplifiers
  * minor edits for items in NOD
* processing directory contains:
  * code for finding NRL links for equipment response
  * code for processing input information into SIS-friendly nomenclature
* templates directory contains templates
* writers directory contains code for writing pieces of ext-xmls
* xmls directory contains the default output folders

siswriter.py is the control box for the code. Begin by preparing the data:

NOTE: in the buld_nod_dic function in parse_NOD.py , called by nod_dic() in siswriter.py ,
you need to direct the function to the NOD database. 
If you do not want to included NOD, you can comment out the call of

> python siswriter.py -prep all
> 
> python siswriter.py -process all

Now you're reading to write some xmls!

## Bulk site building
#### NOTE:
By default, processing just the FredSheet or just NOD creates unique processed files,
objs/fs_processed.obj or objs/nod_processed.obj . These need to be referred to in 
line 409 of siswriter.py.

Building many sites can be done in several ways. For example, you could specifiy the source:

> python siswriter.py -write site --src FS

This will write xmls for all of the stations from the Fredsheet

> python siswriter.py -write newest --start 300 --stop 400

This will write from the 300th newest site to the 400th newest site, *including the related equipment*. Prabha, Ellen and the SIS team have determined that 100 sites is the maximum number to upload at any one time. It will also open a shelve file, not building any equipment multiple times. For example, if the 315th most recent site and the 330th most recent site both use sensor L-4C VERTICAL 1333, that sensor will only be built once.
The equipment is sorted by start date.
The equipment will be saved to xmls/mass_equipment, and the sites to xmls/mass_sites.
Equipment must be loaded to SIS(test) before sites.

> python siswriter.py -write all --sn NC.KRK 

This will build a chosen site and all of the equipment it uses. Before uploading the site into SIS(test), the equipment must also be loaded. Refer to the bottom of this page for loading directions.

> python siswriter.py -write sensor --sn 4311
> python siswriter.py -write logger --mod GSR-IA18-NETQUAKE

Without the --src flag, equipment or sites from the Fredsheet and NOD will both be written.

Multi-site loggers need some special love

> python siswriter.py -process msl
> python siswriter.py -write msl

The -prep and -process commands store data in pickle files and/or shelves in the objs directory. The -write commands write to the xmls directory. 

Once you have built some xmls, you can upload them to SIS(test)! You can do it one by one with the web ui, but it's faster to get a bash script to do so here: https://wiki.anss-sis.scsn.org/SIStrac/attachment/wiki/SIS/Implementation/Dataloading/Batchload/upload_xml_v4.sh

Then you can load the contents of a directory like so:

>bash upload_xml_v4.sh -d ./xmls/amplifiers/